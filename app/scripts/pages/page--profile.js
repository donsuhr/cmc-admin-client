import $ from 'jquery';
import config from 'config';
import { auth } from '../../components/preconfigured-auth';
import profileDisplay from '../../components/profile-display';

profileDisplay.create({
    $el: $('.profile-form-container'),
    auth,
    config,
});

