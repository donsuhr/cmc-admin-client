import $ from 'jquery';
import config from 'config';
import { auth, profile } from '../../components/preconfigured-auth';
import changePassword from '../../components/change-password';

changePassword.init({
    $el: $('.change-password-form-container'),
    auth,
    profile,
    config,
    mode: 'reset',
});
