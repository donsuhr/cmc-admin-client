import $ from 'jquery';
import queryString from 'query-string';
import api from '../../components/contact-us';
import { init as groupNavInit } from '../../components/group-nav';

const $el = $('#ContactUsApp');
const parsed = queryString.parse(window.location.search);
const group = $el.data('group') || parsed.group || 'all';
const site = $el.data('site') || parsed.site;
const label = `${$el.data('label')} - ${site} - ${group}`;
const apiFolder = $el.data('apifolder');

api.init({
    el: '#ContactUsApp',
    group,
    site,
    label,
    apiFolder,
});

groupNavInit($('.sectionNav'), apiFolder);
