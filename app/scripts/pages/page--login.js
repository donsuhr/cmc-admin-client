import $ from 'jquery';
import PubSub from 'pubsub-js';
import { loginForm } from 'cmc-auth';
import queryString from 'query-string';
import roleNotFoundTemplate from 'components/loginForm/role-not-found.hbs';
import { auth } from '../../components/preconfigured-auth';

function showMainApp() {
    if (auth.hasRole('admin')) {
        const parsed = queryString.parse(window.location.search);
        const url = parsed.r ? encodeURI(decodeURIComponent(parsed.r)) : '/';
        window.location.href = `${url}${window.location.hash}`;
    } else {
        $('.main-app').html(roleNotFoundTemplate());
        $('.main-app').one('click', '.refresh-btn', (event) => {
            event.preventDefault();
            $('.main-app').find('.refresh-btn').remove();
            auth.refresh(auth.getToken());
        });
    }
}

function onCmcAuthMessage(msg, data) {
    $('.login-form-container').css('display', '');
    // eslint-disable-next-line default-case
    switch (msg) {
        case 'cmc-auth.authenticated':
            showMainApp();
            break;
        case 'cmc-auth.renew-success':
            $('.main-app').removeClass('loading');
            showMainApp();
            break;
    }
}

PubSub.subscribe('cmc-auth', onCmcAuthMessage);

if (auth.isAuthenticated()) {
    showMainApp();
} else {
    // not visible so doesn't try automatically
    loginForm.loadSSO();
}
