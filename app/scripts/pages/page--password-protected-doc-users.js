import $ from 'jquery';
import api from '../../components/password-protected-doc-user';

const $el = $('#RequestDemoApp');

api.init({
    el: '#RequestDemoApp',
    label: $el.data('label'),
    apiFolder: $el.data('apifolder'),
});
