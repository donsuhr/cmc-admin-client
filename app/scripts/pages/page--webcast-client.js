import $ from 'jquery';
import api from '../../components/webcast-client';

const $el = $('#WebcastSeriesApp');

api.init({
    el: '#WebcastSeriesApp',
    group: $el.data('group'),
    site: $el.data('site'),
    label: $el.data('label'),
    apiFolder: $el.data('apifolder'),
    query: $el.data('query'),
});
