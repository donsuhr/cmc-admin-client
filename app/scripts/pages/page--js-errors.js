import React from 'react';
import ReactDOM from 'react-dom';
import App from '../../components/client-side-js-error-list/index';

const el = document.getElementById('MountPoint');

const dataAttributes = [...el.attributes]
    .map(x => ({ name: x.name, value: x.value }))
    .filter(x => x.name.indexOf('data') !== -1)
    .reduce((accum, current) => {
        accum[current.name.replace('data-', '')] = current.value;
        return accum;
    }, {});

ReactDOM.render(React.createElement(App, dataAttributes), el);
