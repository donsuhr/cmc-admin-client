import $ from 'jquery';
import api from '../../components/ci-session-evaluation';

const $el = $('#RequestDemoApp');

api.init({
    el: '#RequestDemoApp',
    group: $el.data('group'),
    site: $el.data('site'),
    label: $el.data('label'),
    query: $el.data('query'),
    apiFolder: $el.data('apifolder'),
});
