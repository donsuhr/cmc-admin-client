import $ from 'jquery';
import api from '../../components/register-show-download';

const $el = $('#RequestDemoApp');

api.init({
    el: '#RequestDemoApp',
    label: $el.data('label'),
    apiFolder: $el.data('apifolder'),
});

