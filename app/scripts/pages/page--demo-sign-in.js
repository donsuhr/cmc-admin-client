import $ from 'jquery';
import queryString from 'query-string';
import config from 'config';
import api from '../../components/demo-sign-in';
import { checkStatus, parseJSON } from '../../components/fetchJsonHelpers';
import { auth } from '../../components/preconfigured-auth';

const $el = $('#RequestDemoApp');

const parsed = queryString.parse(window.location.search);
const group = $el.data('group') || parsed.group || 'all';
const site = $el.data('site') || parsed.site;
const label = `${$el.data('label')} - ${site} - ${group}`;
const apiFolder = $el.data('apifolder');
api.init({
    el: '#RequestDemoApp',
    group,
    site,
    label,
    apiFolder,
});

$('.sectionNav').text('loading...');
fetch(`${config.domains.api}/${apiFolder}/groupOptions`, {
    method: 'GET',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        authorization: `Bearer ${auth.getToken()}`,
    },
})
    .then(checkStatus)
    .then(parseJSON)
    .then((result) => {
        const template = x =>
            `<li><a href="${
                window.location.pathname
            }?group=${x}">${x}</a></li>`;
        let html = result.data.map(x => template(x));
        html = template('all') + html.join('');
        $('.sectionNav').html(html);
    });
