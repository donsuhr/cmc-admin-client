import $ from 'jquery';
import api from '../../components/password-protected-doc';

const $el = $('#s3DocsApp');

api.init({
    el: '#s3DocsApp',
    group: $el.data('group'),
    site: $el.data('site'),
    label: $el.data('label'),
    ACL: $el.data('acl'),
    apiFolder: $el.data('apifolder'),
});
