import $ from 'jquery';
import api from '../../components/why-campusmanagement';

const $el = $('#ContactUsApp');

api.init({
    el: '#ContactUsApp',
    group: $el.data('group'),
    site: $el.data('site'),
    label: $el.data('label'),
    apiFolder: $el.data('apifolder'),
});
