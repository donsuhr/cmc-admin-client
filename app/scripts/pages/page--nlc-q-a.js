import $ from 'jquery';
import api from '../../components/nlc-q-a';

const $el = $('#RequestDemoApp');

api.init({
    el: '#RequestDemoApp',
    group: $el.data('group'),
    site: $el.data('site'),
    label: $el.data('label'),
    apiFolder: $el.data('apifolder'),
});

