import $ from 'jquery';
import api from '../../components/s3-document-properties';

const $el = $('#s3DocsApp');

api.init({
    el: '#s3DocsApp',
    group: $el.data('group'),
    site: $el.data('site'),
    ACL: $el.data('ACL'),
    label: $el.data('label'),
    apiFolder: $el.data('apifolder'),
});
