import $ from 'jquery';
import api from '../../components/product-center-register';

const $el = $('#RequestDemoApp');

api.init({
    el: '#RequestDemoApp',
    group: $el.data('group'),
    site: $el.data('site'),
    label: $el.data('label'),
    apiFolder: $el.data('apifolder'),
});

