import App from '../../components/campusInsight';

const el = document.getElementById('CiAppMountPoint');

const dataAttributes = [...el.attributes]
    .map(x => ({ name: x.name, value: x.value }))
    .filter(x => x.name.indexOf('data') !== -1)
    .reduce((accum, current) => {
        accum[current.name.replace('data-', '')] = current.value;
        return accum;
    }, {});

App.config(dataAttributes, el);
