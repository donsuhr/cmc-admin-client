import $ from 'jquery';
import config from 'config';
import { auth } from '../components/preconfigured-auth';
import loginForm from '../components/loginForm';

const isProtectedPath = window.location.pathname.match(
    /^(?!(\/styles|\/images|\/scripts|\/forgot-password|\/login|\/sso-iframe\.html)).*$/
);

if (isProtectedPath && !auth.isAuthenticated()) {
    const r = encodeURIComponent(
        `${window.location.protocol}//${window.location.host}${
            window.location.pathname
        }`
    );
    window.location.href = `${window.location.protocol}//${
        window.location.host
    }/login?r=${r}`;
} else {
    document.body.classList.remove('page-loading');
}

$(document).ajaxSend((event, request, settings) => {
    if (settings.url.indexOf(config.domains.api) !== -1) {
        const token = auth.getToken();
        if (token) {
            request.setRequestHeader('authorization', `Bearer ${token}`);
        }
    }
});

loginForm.init();

$('.reset-password').on('click', () => {
    auth.updatePassword('lschloss@campusmgmt.com');
});
