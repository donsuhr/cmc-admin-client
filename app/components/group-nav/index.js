import config from 'config';
import { checkStatus, parseJSON } from '../fetchJsonHelpers';
import { auth } from '../preconfigured-auth';

function init($el, apiFolder) {
    $el.text('loading...');
    fetch(`${config.domains.api}/${apiFolder}/groupOptions`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            authorization: `Bearer ${auth.getToken()}`,
        },
    })
        .then(checkStatus)
        .then(parseJSON)
        .then((result) => {
            const template = x =>
                `<li><a href="${
                    window.location.pathname
                }?group=${x}">${x}</a></li>`;
            let html = result.data.map(x => template(x));
            html = template('all') + html.join('');
            $el.html(html);
        });
}

export { init };
