import Backbone from 'backbone';
import config from 'config';

export default Backbone.Collection.extend({
    initialize(options) {
        this.model = options.model;
        this.apiFolder = options.apiFolder;
        this.site = options.site;
        this.group = options.group;
        this.query = options.query;
    },
    url() {
        let query = this.site || this.group ? '?' : '';

        if (this.site) {
            query += query === '' || query === '?' ? '' : '&';
            query += `site=${this.site}`;
        }
        if (this.group) {
            query += query === '' || query === '?' ? '' : '&';
            query += `group=${this.group}`;
        }
        if (this.query) {
            query += query === '' || query === '?' ? '' : '&';
            query += `${this.query}`;
        }
        return `${config.domains.api}/${this.apiFolder}/${query}`;
    },
    parse(response) {
        return response.data;
    },
});
