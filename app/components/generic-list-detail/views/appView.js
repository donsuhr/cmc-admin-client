import Backbone from 'backbone';
import config from 'config';
import LoadingAnimation from '../../loading-view';
import EmailTxModel from '../emailTxModel';
import ListCollection from '../listCollection';
import ListView from './listView';
import ItemModel from '../itemModel';
import DetailsView, { RENDER_MODE_VIEW, RENDER_MODE_EDIT } from './itemDetailView';

export default Backbone.View.extend({
    initialize(options) {
        this.label = options.label;
        this.router = options.router;
        this.viewTemplate = options.appViewTemplate;
        this.listViewClassId = options.listViewClassId;
        this.pending = true;
        this.options = options;
        this.loading = new LoadingAnimation({ $container: this.$el });

        const ListCollectionClass = options.ListCollection || ListCollection;
        this.ItemModelClass = options.ItemModel || ItemModel;
        this.ItemModelClass.apiFolder = options.apiFolder;
        this.listCollection = new ListCollectionClass({
            model: this.ItemModelClass,
            apiFolder: options.apiFolder,
            site: options.site,
            group: options.group,
            query: options.query,
        });
        this.ItemModelClass.prototype.apiFolder = options.apiFolder;

        const ListViewClass = options.ListView || ListView;
        this.listView = new ListViewClass({
            model: this.listCollection,
            itemViewTemplate: options.itemViewTemplate,
        });

        const DetailsViewClass = options.DetailsView || DetailsView;
        this.detailsView = new DetailsViewClass({
            viewTemplate: options.detailsViewTemplate,
            editTemplate: options.detailsEditTemplate,
            EmailTxModel: options.EmailTxModel || EmailTxModel,
            emailLogListContainerTemplate: options.emailLogListContainerTemplate,
            apiFolder: options.apiFolder,
            label: options.label,
        });

        this.listenTo(this.listCollection, 'sync reset', this.onListCollectionSync);
        this.listenTo(this.listCollection, 'error', this.onListCollectionSyncError);
        this.listenTo(this.listCollection, 'request', this.onSyncStart);
        this.listenTo(this.router, 'route', this.onRoute);
        this.listenTo(this.detailsView, 'backClicked', this.onBackClicked);
        this.listenTo(this.detailsView, 'saveStart', this.onSyncStart);
        this.listenTo(this.detailsView, 'saveStop', this.onSyncEnd);
        this.listenTo(Backbone, 'itemSaved', this.onItemSaved);

        this.listCollection.fetch();
    },

    events: {
        edit: 'onEdit',
        'click .list-detail-app__add-button': 'onAddClicked',
    },

    render(event) {
        const dlUrl = `${config.domains.api}/${this.options.apiFolder}/excel/${this.options.site}/${this.options.group}`;
        this.$el.html(this.viewTemplate({ label: this.label, dlUrl }));
        this.listView.setElement(this.$el.find(this.listViewClassId));
        this.listView.render();
        if (typeof this.pageYOffset !== 'undefined') {
            window.scrollTo(0, this.pageYOffset);
        }
    },

    renderDetails(model) {
        this.detailsView.updateModel(model);
        this.$el.html(this.detailsView.render().el);
    },

    onListCollectionSync(event) {
        this.onSyncEnd();
        this.listView.render();
    },

    onListCollectionSyncError(collection, resp, options) {
        // eslint-disable-next-line no-console
        console.log('Error:', resp, resp.stack);
        this.onSyncEnd();
    },

    onSyncStart() {
        this.pending = true;
        this.loading.show();
    },

    onSyncEnd() {
        this.loading.hide();
        this.pending = false;
    },

    onEdit(event, model) {
        const idField = this.listCollection.model.prototype.idAttribute;
        this.onRoute({
            action: 'display',
            id: model.get(idField),
        });
        this.router.updateDisplay(model.get(idField));
    },

    onAddClicked(event) {
        event.preventDefault();
        const newModel = new this.ItemModelClass(null, this.options);
        this.detailsView.setRenderMode(RENDER_MODE_EDIT);
        this.renderDetails(newModel);
    },

    onBackClicked() {
        this.router.updateDisplay();
        this.onRoute({ action: 'show-home' });
    },

    onPendingDisplay() {
        this.onRoute({
            action: 'display',
            id: this.pendingId,
        });
    },

    onItemSaved() {
        this.listCollection.fetch();
    },

    onRoute(event) {
        switch (event.action) {
            case 'show-home':
            default:
                this.render();
                if (this.pending) {
                    this.loading.show();
                }
                break;
            case 'display':
                this.pageYOffset = window.pageYOffset;
                if (this.pending) {
                    this.pendingId = event.id;
                    this.listenToOnce(this.listCollection, 'sync reset', this.onPendingDisplay);
                } else {
                    const idField = this.listCollection.model.prototype.idAttribute;
                    const model = this.listCollection.findWhere({ [idField]: event.id });
                    this.detailsView.setRenderMode(RENDER_MODE_VIEW);
                    this.renderDetails(model);
                }
                break;
        }
    },

});
