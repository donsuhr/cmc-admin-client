import $ from 'jquery';
import assign from 'lodash/assign';
import 'parsleyjs';
import 'jquery-ui/ui/widgets/dialog';
import formSerialize from 'form-serialize';
import Backbone from 'backbone';
import EmailView from './emailListView';
import confirmDeleteTemplate from '../templates/confirm-delete-dialog.hbs';
import { auth } from '../../preconfigured-auth';

const RENDER_MODE_VIEW = 'view';
const RENDER_MODE_EDIT = 'edit';
export { RENDER_MODE_VIEW, RENDER_MODE_EDIT };

export default Backbone.View.extend({
    initialize(options) {
        this.EmailTxModel = options.EmailTxModel;
        this.apiFolder = options.apiFolder;
        this.viewTemplate = options.viewTemplate;
        this.editTemplate = options.editTemplate;
        this.emailLogListContainerTemplate = options.emailLogListContainerTemplate;
        this.label = options.label;
    },

    tagName: 'div',

    events: {
        'click .item-details__back-button': 'onBackClicked',
        'click .item-details__load-emails-button': 'onLoadEmailsClicked',
        'click .item-details__edit-button': 'onEditClicked',
        'click .item-details__save-button': 'onSaveClicked',
        'click .item-details__cancel-button': 'onCancelClicked',
        'click .list-details__delete-button': 'onDeleteClicked',
    },

    updateModel(model) {
        this.pageYOffset = 0;
        if (this.model) {
            this.model.off(null, null, this);
        }
        this.model = model;
        delete this.emailView;
    },

    render() {
        const vm = assign({}, this.model.toJSON(), { label: this.label });
        this.$el.html(this.currentTemplate(vm));
        if (this.model.emailTx) {
            this.renderEmailTransactions();
        }
        if (typeof this.pageYOffset !== 'undefined') {
            window.scrollTo(0, this.pageYOffset);
        }
        this.delegateEvents();
        return this;
    },

    setRenderMode(mode) {
        const modes = {
            [RENDER_MODE_EDIT]: this.editTemplate,
            [RENDER_MODE_VIEW]: this.viewTemplate,
        };
        this.currentTemplate = modes[mode] || modes[RENDER_MODE_VIEW];
    },

    renderEmailTransactions() {
        const parent = this.$el.find('.item-details__email-log');
        if (!this.emailView) {
            let model = this.model.emailTx;
            if (!model) {
                model = new this.EmailTxModel({
                    id: this.model.get(this.model.idAttribute),
                    apiFolder: this.apiFolder,
                });
                this.model.emailTx = model;
            }
            this.emailView = new EmailView({
                el: parent,
                model,
                template: this.emailLogListContainerTemplate,
            });
            model.fetch();
        }
        if (!$.contains(document, this.emailView.el)) {
            this.emailView.setElement(parent);
        }
        this.emailView.render();
    },

    onDeleteClicked(event) {
        const dialogContent = confirmDeleteTemplate();
        this.confirmDeleteDialog = $(dialogContent).dialog({
            appendTo: this.$el,
            resizable: false,
            height: 'auto',
            width: 400,
            modal: true,
            buttons: {
                Delete: () => {
                    this.onConfirmDelete();
                    this.confirmDeleteDialog.dialog('close');
                },
                Cancel: () => {
                    this.confirmDeleteDialog.dialog('close');
                },
            },
        });
    },

    onConfirmDelete() {
        this.model.destroy({
            success: (model, response) => {
                this.trigger('saveStop');
                this.trigger('backClicked', { action: 'show-home' });
            },
            error: () => {
                this.trigger('saveStop');
                this.$el.find('.item-details__status').text('Error Deleting Item');
            },
        });
    },

    onBackClicked() {
        this.trigger('backClicked', { action: 'show-home' });
    },

    onLoadEmailsClicked(event) {
        event.preventDefault();
        this.pageYOffset = window.pageYOffset;
        this.renderEmailTransactions();
    },

    onEditClicked(event) {
        event.preventDefault();
        this.setRenderMode(RENDER_MODE_EDIT);
        this.render();
    },

    onCancelClicked(event) {
        event.preventDefault();
        this.setRenderMode(RENDER_MODE_VIEW);
        this.render();
    },

    onSaveClicked(event) {
        event.preventDefault();
        const $form = this.$el.find('form');
        const parsleyApi = $form.parsley();
        const valid = parsleyApi.validate();
        if (valid) {
            this.trigger('saveStart');
            const obj = formSerialize($form.get(0), { hash: true, empty: true, disabled: true });
            $form.find('[type="checkbox"][value="true"]').each((index, el) => {
                obj[el.name] = $(el).is(':checked');
            });
            this.model.set(obj);
            this.$el.find('.item-details__status').text('Saving...');
            this.model
                .patch(auth)
                .then((result) => {
                    this.setRenderMode(RENDER_MODE_VIEW);
                    this.render();
                })
                .catch((error) => {
                    // eslint-disable-next-line no-console
                    console.log('request failed', error);
                    this.$el.find('.item-details__status').text('Error Saving Item');
                })
                .then(() => {
                    this.trigger('saveStop');
                });
        }
    },

});
