import mapValues from 'lodash/mapValues';
import ldMap from 'lodash/map';
import sortBy from 'lodash/sortBy';
import Backbone from 'backbone';
import emailLogListTemplate from '../templates/email-log-list.hbs';

export default Backbone.View.extend({
    tagName: 'ul',

    className: 'email-log-list',

    initialize(options) {
        this.loading = false;
        this.template = options.template;
        this.listenTo(this.model, 'sync reset', this.onSyncEnd);
        this.listenTo(this.model, 'request', this.onSyncStart);
    },

    events: {
        'click .email-log-list-group__refresh-button': 'onRefreshClicked',
    },

    render() {
        if (this.loading) {
            this.$el.html('Loading...');
        } else {
            this.$el.html(this.template(this.model.toJSON()));
            const map = list =>
                mapValues(list.groupBy('Message-Id'), (x) => {
                    const sorted = sortBy(x, s => s.get('createdAt'));
                    return ldMap(sorted, m => m.toJSON());
                });

            const adminVM = map(this.model.get('adminEmail'));
            const janusVM = map(this.model.get('janusEmail'));
            const cxVM = map(this.model.get('cxEmail'));
            this.$el.find('.email-log-list--admin').append(emailLogListTemplate(adminVM));
            this.$el.find('.email-log-list--janus').append(emailLogListTemplate(janusVM));
            this.$el.find('.email-log-list--cx').append(emailLogListTemplate(cxVM));
            this.delegateEvents();
        }
        if (typeof this.pageYOffset !== 'undefined') {
            window.scrollTo(0, this.pageYOffset);
        }
        return this;
    },

    onRefreshClicked(event) {
        event.preventDefault();
        this.pageYOffset = window.pageYOffset;
        this.model.fetch();
    },

    onSyncEnd() {
        this.loading = false;
        this.render();
    },

    onSyncStart(event) {
        this.loading = true;
        this.render();
    },

});
