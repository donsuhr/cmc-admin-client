import Backbone from 'backbone';

export default Backbone.View.extend({
    initialize(options) {
        this.template = options.template;
    },

    tagName: 'tr',

    className() {
        return (this.model.get('matching') === false) ? 'no-matching-file' : '';
    },

    events: {
        'click .list-detail-app__edit-button': 'onEditClicked',
    },

    render() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },

    onEditClicked(event) {
        this.$el.trigger('edit', this.model);
    },

});
