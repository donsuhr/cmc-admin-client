import Backbone from 'backbone';
import ItemView from './listItemView';

export default Backbone.View.extend({
    initialize(options) {
        this.listenTo(this.model, 'sync reset', this.render);
        this.options = options;
    },

    render(event) {
        this.$el.empty();
        this.model.each((item) => {
            const view = new ItemView({
                model: item,
                template: this.options.itemViewTemplate,
            });
            this.$el.append(view.render().el);
        });
        return this;
    },
});
