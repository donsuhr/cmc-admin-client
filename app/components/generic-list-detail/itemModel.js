import Backbone from 'backbone';
import keys from 'lodash/keys';
import config from 'config';
import { checkStatus, parseJSON } from '../fetchJsonHelpers';

export default Backbone.Model.extend({
    idAttribute: '_id',

    url() {
        return `${config.domains.api}/${this.apiFolder}/${this.get(this.idAttribute)}`;
    },
    onlyPatchChanged: false,
    initialize() {
        this.listenTo(this, 'change', this.onPropChange);
        this.dirty = [];
    },

    onPropChange(event) {
        this.dirty = this.dirty.concat(keys(event.changed));
    },

    patch(auth) {
        let body;
        if (this.onlyPatchChanged) {
            body = this.dirty.reduce((acc, cur) => {
                acc[cur] = this.get(cur);
                return acc;
            }, {});
            body = JSON.stringify(body);
        } else {
            body = JSON.stringify(this.toJSON());
        }
        let method = 'PATCH';
        let url = this.url();
        const id = this.get(this.idAttribute);
        if (!id) {
            method = 'POST';
            url = `${config.domains.api}/${this.apiFolder}/`;
        }
        return fetch(url, {
            method,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                authorization: `Bearer ${auth.getToken()}`,
            },
            body,
        })
            .then(checkStatus)
            .then(parseJSON)
            .then((response) => {
                this.clear({ silent: true }).set(response, { silent: true });
                this.dirty = [];
                Backbone.trigger('itemSaved', this);
                return response;
            });
    },

});
