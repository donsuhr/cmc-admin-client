import Backbone from 'backbone';
import config from 'config';

export default Backbone.Model.extend({
    initialize(options) {
        this.apiFolder = options.apiFolder;
    },

    defaults: {
        adminEmail: new Backbone.Collection(),
        janusEmail: new Backbone.Collection(),
        cxEmail: new Backbone.Collection(),
    },

    url() {
        return `${config.domains.api}/${this.apiFolder}/${this.id}/email-log`;
    },

    parse(response) {
        response = response.data;
        if (response) {
            response.adminEmail = new Backbone.Collection(response.adminEmail);
            response.janusEmail = new Backbone.Collection(response.janusEmail);
            response.cxEmail = new Backbone.Collection(response.cxEmail);
        }
        return response;
    },

});
