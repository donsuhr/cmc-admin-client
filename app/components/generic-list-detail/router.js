import Backbone from 'backbone';

export default Backbone.Router.extend({
    routes: {
        '': 'showHome',
        ':id': 'display',
    },

    showHome() {
        this.trigger('route', {
            action: 'show-home',
        });
    },

    display(id) {
        this.trigger('route', {
            action: 'display',
            id,
        });
    },

    updateDisplay(id) {
        const url = id ? `/${id}` : '';
        this.navigate(url, { trigger: false });
    },

});
