import $ from 'jquery';
import { loginForm } from 'cmc-auth';
import PubSub from 'pubsub-js';
import { auth, profile } from '../preconfigured-auth';

const container = $('.login-form-container');

// hide until settled to stop loading showing in top bar
container.css('display', 'none');

function onCmcAuthMessage(msg, data) {
    switch (msg) { // eslint-disable-line default-case
        case 'cmc-auth.authenticated':
            container.css('display', '');
            break;
    }
}

export default {
    init() {
        loginForm.init({
            $el: container,
            auth,
            profile,
        });
        PubSub.subscribe('cmc-auth', onCmcAuthMessage);
        if (auth.isAuthenticated()) {
            container.css('display', '');
        }
    },
};
