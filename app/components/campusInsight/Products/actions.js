import merge from 'lodash/merge';
import omitBy from 'lodash/omitBy';
import isNil from 'lodash/isNil';
import config from 'config';
import { fetchItems, updateItem, addItem, deleteItem } from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/products/`;

function normalizeResponse(response) {
    const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = nullRemoved.map(x => merge({}, {
        title: '',
    }, x));
    return response;
}

export const REQUEST_PRODUCTS = 'REQUEST_PRODUCTS';

export function requestProducts() {
    return {
        type: REQUEST_PRODUCTS,
    };
}

export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';

export function receiveProducts(items) {
    return {
        type: RECEIVE_PRODUCTS,
        items: normalizeResponse(items),
    };
}

export const RECEIVE_PRODUCT = 'RECEIVE_PRODUCT';

export function receiveProduct(item) {
    return {
        type: RECEIVE_PRODUCT,
        item: normalizeResponse({ data: [item] })[0],
    };
}

export const RECEIVE_PRODUCT_TIMEOUT = 'RECEIVE_PRODUCT_TIMEOUT';

export function receiveProductTimeout(item) {
    return {
        type: RECEIVE_PRODUCT_TIMEOUT,
        item,
    };
}

export const REQUEST_PRODUCT_UPDATE = 'REQUEST_PRODUCT_UPDATE';

function requestProductUpdate(id) {
    return {
        type: REQUEST_PRODUCT_UPDATE,
        id,
    };
}

export const PRODUCT_UPDATE_ERROR = 'PRODUCT_UPDATE_ERROR';

function requestProductError(error, id) {
    return {
        type: PRODUCT_UPDATE_ERROR,
        error,
        id,
    };
}

export const REQUEST_PRODUCT_ADD = 'REQUEST_PRODUCT_ADD';

function requestProductAdd() {
    return {
        type: REQUEST_PRODUCT_ADD,
    };
}

export const PRODUCT_ADD_ERROR = 'PRODUCT_ADD_ERROR';

function addProductError(error, data) {
    return {
        type: PRODUCT_ADD_ERROR,
        error,
        data,
    };
}

export const REQUEST_PRODUCT_DELETE = 'REQUEST_PRODUCT_DELETE';

function requestProductDelete(id) {
    return {
        type: REQUEST_PRODUCT_DELETE,
        id,
    };
}

export const RECEIVE_PRODUCT_DELETE = 'RECEIVE_PRODUCT_DELETE';

function receiveProductDelete(id) {
    return {
        type: RECEIVE_PRODUCT_DELETE,
        id,
    };
}

export const REQUEST_PRODUCT_DELETE_ERROR = 'REQUEST_PRODUCT_DELETE_ERROR';

function requestProductDeleteError(id) {
    return {
        type: REQUEST_PRODUCT_DELETE_ERROR,
        id,
    };
}

export function addProduct(item) {
    return (dispatch, getState) => {
        const { domAttributes } = getState();
        if (domAttributes && Object.hasOwnProperty.call(domAttributes, 'year')) {
            item.year = domAttributes.year;
        }
        return addItem({
            item,
            dispatch,
            endpoint,
            startAction: requestProductAdd,
            endAction: receiveProduct,
            endActionTimeout: receiveProductTimeout,
            errorAction: addProductError,
        });
    };
}

export function updateProduct(item, change) {
    const newItem = {
        ...item,
        ...change,
    };

    return dispatch => updateItem({
        item: newItem,
        change,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestProductUpdate,
        endAction: receiveProduct,
        endActionTimeout: receiveProductTimeout,
        errorAction: requestProductError,
    });
}

export function fetchProducts() {
    return (dispatch, getState) => {
        const year = Object.hasOwnProperty.call(getState().domAttributes, 'year') ? `${getState().domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestProducts,
            endAction: receiveProducts,
            errorAction: requestProductError,
        });
    };
}

export function deleteProduct(item) {
    return dispatch => deleteItem({
        item,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestProductDelete,
        endAction: receiveProductDelete,
        errorAction: requestProductDeleteError,
    });
}
