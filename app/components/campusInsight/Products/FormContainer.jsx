import React from 'react';
import { connect } from 'react-redux';
import FormWrapper from '../components/Form-wrapper';
import { addProduct, updateProduct, deleteProduct } from './actions';
import Form from './Form';
import { getProductById } from '../redux/reducers';

const FormContainer = ({ params, year }) => (
    <FormWrapper
        returnTo="/products"
        addAction={addProduct}
        updateAction={updateProduct}
        deleteAction={deleteProduct}
        getById={getProductById}
        stateKey="products"
        formName="productForm"
        year={year}
        params={params}
        formComponent={Form}
    />
);

FormContainer.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    params: React.PropTypes.object,
    year: React.PropTypes.string,
};

function mapStateToProps(state, ownProps) {
    return {
        year: state.domAttributes.year,
    };
}

const FormContainerConnected = connect(
    mapStateToProps
)(FormContainer);

export default FormContainerConnected;
