import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import Grid from '../components/Grid';
import { getUnicodeStatus } from '../util/grid-util';

import { fetchProducts, updateProduct } from './actions';
import { getById } from './reducers';

/* eslint-disable react/prop-types */
const gridColumns = gridContainerProps => {
    const ret = [
        {
            key: 'title',
            name: 'Product',
            editable: true,
            sortable: true,
            resizable: true,
            width: 250,
        },
        {
            key: 'status',
            name: '↹',
            width: 35,
            formatter: ({ value }) => (<div>{getUnicodeStatus(value).status}</div>),
            cellClass: 'presenter-grid__cell--saving',
        },
        {
            key: '_id',
            name: '',
            width: 35,
            formatter: props => (
                <div>
                    <Link to={`${props.dependentValues.gridProps.location.pathname}/edit/${props.value}`}>
                        Edit
                    </Link>
                </div>
            ),
            cellClass: 'presenter-grid__cell--actions',
            getRowMetaData() {
                return { gridProps: gridContainerProps };
            },
        },
    ];
    if (!gridContainerProps.year) {
        const index = ret.length - 2;
        return [
            ...ret.slice(0, index),
            {
                key: 'year',
                name: 'Year',
                editable: true,
                sortable: true,
                resizable: true,
                width: 50,
            },
            ...ret.slice(index),
        ];
    }
    return ret;
};

const GridContainer = props => (
    <Grid
        columns={gridColumns(props)}
        {...props}
    />
);

function mapStateToProps(state, ownProps) {
    return {
        dataById: state.products.byId,
        fetching: state.products.fetching,
        getItemById: getById.bind(null, state.products),
        year: state.domAttributes.year,
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        update: bindActionCreators(updateProduct, dispatch),
        fetch: bindActionCreators(fetchProducts, dispatch),
    };
}

const GridConnected = connect(mapStateToProps, mapDispatchToProps)(GridContainer);

export default GridConnected;
