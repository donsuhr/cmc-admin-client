import { combineReducers } from 'redux';
import * as actions from './actions';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.REQUEST_PRODUCT_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.RECEIVE_PRODUCT:
            return {
                ...state,
                savingStatus: 'saved',
                ...action.item,
            };
        case actions.RECEIVE_PRODUCT_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.PRODUCT_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        case actions.RECEIVE_PRODUCTS: {
            return {
                ...defaultState,
                ...state,
                ...currentItem,
                savingStatus: '',
            };
        }
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_PRODUCTS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});
        case actions.REQUEST_PRODUCT_UPDATE:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_PRODUCT:
        case actions.RECEIVE_PRODUCT_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.RECEIVE_PRODUCT_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case actions.PRODUCT_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_PRODUCTS:
        case actions.REQUEST_PRODUCT_ADD:
        case actions.REQUEST_PRODUCT_UPDATE:
        case actions.REQUEST_PRODUCT_DELETE:

            return true;
        case actions.RECEIVE_PRODUCTS:
        case actions.PRODUCT_ADD_ERROR:
        case actions.PRODUCT_UPDATE_ERROR:
        case actions.RECEIVE_PRODUCT:
        case actions.RECEIVE_PRODUCT_DELETE:
        case actions.REQUEST_PRODUCT_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

const presenters = combineReducers({
    fetching,
    byId,
});

export default presenters;

export function getById(state, id) {
    return state.byId[id];
}
