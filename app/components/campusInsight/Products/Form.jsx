import React from 'react';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import { renderField } from '../util/form-util';

const validate = (values) => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Required';
    }
    return errors;
};

const Form = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        item: React.PropTypes.object,
        isEdit: React.PropTypes.bool.isRequired,
        error: React.PropTypes.bool,
        pristine: React.PropTypes.bool,
        submitting: React.PropTypes.bool,
        handleSubmit: React.PropTypes.func.isRequired,
        handleDelete: React.PropTypes.func.isRequired,
        year: React.PropTypes.string,
    },

    componentDidMount() {
        this.firstField.getRenderedComponent().focus();
    },

    render() {
        const {
            error,
            handleSubmit,
            pristine,
            submitting,
            handleDelete,
            isEdit,
            year,
            ...props
        } = this.props;
        return (
            <div className="">
                <form onSubmit={handleSubmit} className="cmc-form">
                    <dl className="item-details-list">
                        <dt><label htmlFor="title">Product</label></dt>
                        <dd>
                            <Field
                                ref={(x) => {
                                    this.firstField = x;
                                }}
                                withRef
                                name="title"
                                label="Product"
                                component="input"
                                type="text"
                                id="title"
                            />
                        </dd>
                        {!year &&
                        <Field
                            name="year"
                            label="Year"
                            component={renderField}
                            type="text"
                        />
                        }
                        {
                            props.item && <div>
                                <dt>Created At</dt>
                                <dd>{moment(props.item.createdAt).format('YYYY-MM-DD hh:mm A')}</dd>
                                <dt>Updated At</dt>
                                <dd>{moment(props.item.updatedAt).format('YYYY-MM-DD hh:mm A')}</dd>
                            </div>
                        }
                    </dl>

                    <button
                        className="item-details__save-button"
                        type="submit"
                        disabled={pristine || submitting}
                    >
                        {isEdit ? 'Save' : 'Create'}
                    </button>
                    <br />
                    {
                        props.item && <button
                            className="item-details__delete-button"
                            onClick={handleDelete}
                            type="button"
                        >
                            Delete
                        </button>
                    }
                    <p>{error && <strong>{error}</strong>}</p>
                </form>
            </div>
        );
    },
});

const ContactForm = reduxForm({
    form: 'productForm',
    validate,
})(Form);

export default ContactForm;
