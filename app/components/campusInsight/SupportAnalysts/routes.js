/* eslint-disable no-shadow */
module.exports = {
    path: 'supportAnalysts',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('../SupportAnalysts/GridContainer').default);
        });
    },

    childRoutes: [
        {
            path: 'add',
            getComponent(nextState, cb) {
                require.ensure([], (require) => {
                    cb(
                        null,
                        require('../SupportAnalysts/FormContainer').default
                    );
                });
            },
        },
        {
            path: 'edit/:id',
            getComponent(nextState, cb) {
                require.ensure([], (require) => {
                    cb(
                        null,
                        require('../SupportAnalysts/FormContainer').default
                    );
                });
            },
        },
    ],
};
