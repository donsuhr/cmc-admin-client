import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import { fetchAnalysts, updateAnalyst } from './actions';
import Grid from '../components/Grid';
import { getUnicodeStatus } from '../util/grid-util';
import { getById } from './reducers';

/* eslint-disable react/prop-types */
const gridColumns = gridContainerProps => {
    const ret =
        [
            {
                key: 'firstName',
                name: 'First',
                editable: true,
                sortable: true,
                resizable: true,
                width: 100,
            },
            {
                key: 'lastName',
                name: 'Last',
                editable: true,
                sortable: true,
                resizable: true,
                width: 100,
            },
            {
                key: 'topic',
                name: 'Topic',
                editable: true,
                sortable: true,
                resizable: true,
                width: 100,
            },
            {
                key: 'savingStatus',
                name: '↹',
                width: 35,
                formatter: ({ value }) => (<div>{getUnicodeStatus(value).status}</div>),
                cellClass: 'presenter-grid__cell--saving',
            },
            {
                key: '_id',
                name: '',
                width: 35,
                formatter: props => (
                    <div>
                        <Link to={`${props.dependentValues.gridProps.location.pathname}/edit/${props.value}`}>
                            Edit
                        </Link>
                    </div>
                ),
                cellClass: 'presenter-grid__cell--actions',
                getRowMetaData() {
                    return { gridProps: gridContainerProps };
                },
            },
        ];
    if (!gridContainerProps.year) {
        const index = ret.length - 2;
        return [
            ...ret.slice(0, index),
            {
                key: 'year',
                name: 'Year',
                editable: true,
                sortable: true,
                resizable: true,
                width: 50,
            },
            ...ret.slice(index),
        ];
    }
    return ret;
};

const GridContainer = props => (
    <Grid
        columns={gridColumns(props)}
        {...props}
    />
);

function mapStateToProps(state, ownProps) {
    return {
        dataById: state.analysts.byId,
        fetching: state.analysts.fetching,
        getItemById: getById.bind(null, state),
        year: state.domAttributes.year,
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        update: bindActionCreators(updateAnalyst, dispatch),
        fetch: bindActionCreators(fetchAnalysts, dispatch),
    };
}

const GridConnected = connect(mapStateToProps, mapDispatchToProps)(GridContainer);

export default GridConnected;
