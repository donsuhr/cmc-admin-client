import { combineReducers } from 'redux';
import * as actions from './actions';

const defaultState = {
    savingStatus: '',
    availability: [],
};

const availability = (state = [], action) => {
    switch (action.type) {
        case actions.RECEIVE_ANALYSTS:
            return action.item.availability.map(x => ({
                ...x,
                pristineValues: { ...x },
                savingStatus: '',
            }));
        case actions.REQUEST_ANALYST_AVAILABILITY_UPDATE:
        case actions.REQUEST_ANALYST_AVAILABILITY_DELETE:
            if (action.row.pristineValues.start === null &&
                action.row.pristineValues.end === null) {
                return [
                    ...state,
                    {
                        start: action.row.start,
                        end: action.row.end,
                        pristineValues: {
                            start: null,
                            end: null,
                        },
                        savingStatus: 'saving',
                    },
                ];
            }
            return state.map((x) => {
                if (x.start === action.row.pristineValues.start &&
                    x.end === action.row.pristineValues.end) {
                    return {
                        ...x,
                        start: action.row.start,
                        end: action.row.end,
                        savingStatus: 'saving',
                    };
                }
                return x;
            });
        case actions.REQUEST_ANALYST_AVAILABILITY_DELETE_ERROR:
            return state.map((x) => {
                if (x.start === action.row.pristineValues.start &&
                    x.end === action.row.pristineValues.end) {
                    return {
                        ...x,
                        savingStatus: `Error:${action.error}`,
                    };
                }
                return x;
            });
        case actions.RECEIVE_ANALYST_AVAILABILITY_UPDATE:
            const findFn = (stateRow, actionRow) => {
                if (actionRow.pristineValues.start === null &&
                    actionRow.pristineValues.end === null) {
                    return stateRow.start === actionRow.start &&
                        stateRow.end === actionRow.end;
                }
                return stateRow.pristineValues.start === actionRow.pristineValues.start &&
                    stateRow.pristineValues.end === actionRow.pristineValues.end;
            };

            return state.map((x) => {
                if (findFn(x, action.row)) {
                    return {
                        ...x,
                        start: action.row.start,
                        end: action.row.end,
                        pristineValues: {
                            start: action.row.start,
                            end: action.row.end,
                        },
                        savingStatus: 'saved',
                    };
                }
                return x;
            });
        case actions.RECEIVE_ANALYST_AVAILABILITY_TIMEOUT:
            return state.map((x) => {
                if (
                    x.start === action.row.start &&
                    x.end === action.row.end
                ) {
                    return {
                        ...x,
                        savingStatus: '',
                    };
                }
                return x;
            });
        case actions.RECEIVE_ANALYST_AVAILABILITY_DELETE:
            const index = state.findIndex(
                x => x.start === action.row.start && x.end === action.row.end
            );
            if (index !== -1) {
                return [
                    ...state.slice(0, index),
                    ...state.slice(index + 1),
                ];
            }
            return state;
        default:
            return state;
    }
};

const item = (state = defaultState, action) => {
    switch (action.type) {
        case actions.REQUEST_ANALYST_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.REQUEST_ANALYST_AVAILABILITY_UPDATE:
        case actions.RECEIVE_ANALYST_AVAILABILITY_UPDATE:
        case actions.RECEIVE_ANALYST_AVAILABILITY_TIMEOUT:
        case actions.REQUEST_ANALYST_AVAILABILITY_DELETE:
        case actions.RECEIVE_ANALYST_AVAILABILITY_DELETE:
        case actions.REQUEST_ANALYST_AVAILABILITY_DELETE_ERROR:
            if (state._id === action.id) {
                return {
                    ...state,
                    availability: availability(state.availability, action),
                };
            }
            return state;
        case actions.RECEIVE_ANALYST:
            return {
                ...state,
                savingStatus: 'saved',
                ...action.item,
            };
        case actions.RECEIVE_ANALYST_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.ANALYST_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        case actions.RECEIVE_ANALYSTS: {
            return {
                ...defaultState,
                ...state,
                ...action.item,
                availability: availability(state.availability, action),
                savingStatus: '',
            };
        }
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_ANALYSTS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], {
                    type: action.type,
                    item: c,
                });
                return p;
            }, {});
        case actions.REQUEST_ANALYST_UPDATE:
        case actions.REQUEST_ANALYST_AVAILABILITY_UPDATE:
        case actions.RECEIVE_ANALYST_AVAILABILITY_UPDATE:
        case actions.REQUEST_ANALYST_AVAILABILITY_DELETE:
        case actions.REQUEST_ANALYST_AVAILABILITY_DELETE_ERROR:
        case actions.RECEIVE_ANALYST_AVAILABILITY_DELETE:
        case actions.RECEIVE_ANALYST_AVAILABILITY_TIMEOUT:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_ANALYST:
        case actions.RECEIVE_ANALYST_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.RECEIVE_ANALYST_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case actions.ANALYST_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_ANALYSTS:
        case actions.REQUEST_ANALYST_ADD:
        case actions.REQUEST_ANALYST_UPDATE:
        case actions.REQUEST_ANALYST_DELETE:
            return true;
        case actions.RECEIVE_ANALYSTS:
        case actions.ANALYST_ADD_ERROR:
        case actions.ANALYST_UPDATE_ERROR:
        case actions.REQUEST_ANALYST_ERROR:
        case actions.RECEIVE_ANALYST:
        case actions.RECEIVE_ANALYST_DELETE:
        case actions.REQUEST_ANALYST_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.RECEIVE_ANALYSTS:
        case actions.REQUEST_ANALYST_ERROR:
            return true;
        default:
            return state;
    }
};


const analysts = combineReducers({
    hasEverLoaded,
    fetching,
    byId,
});

export default analysts;

export function getById(state, id) {
    if (!state.analysts || !state.analysts.byId) {
        return undefined;
    }
    return state.analysts.byId[id];
}

export function getAppointmentFromId(state, availabilityId) {
    let availabilityObj;
    const analystId = Object.keys(state.byId).find(x =>
        // eslint-disable-next-line implicit-arrow-linebreak
        state.byId[x].availability.some((y) => {
            const found = y._id === availabilityId;
            if (found) {
                availabilityObj = y;
            }
            return found;
        }));
    return { availabilityObj, analystObj: state.byId[analystId] };
}
