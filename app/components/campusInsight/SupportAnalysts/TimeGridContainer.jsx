import React from 'react';
import { connect } from 'react-redux';
import { listenToAuth, listenToUserRoot } from '../../firebase-login/auth.actions';
import { listenToScheduleSupport, listenToUsedAvailability } from '../../firebase-login/schedule.support.actions';
import { getScheduleItemIsAvailable } from '../../firebase-login/schedule.support.reducer';

import TimeGrid from './TimeGrid';

const container = React.createClass({
    propTypes: {
        listenToAuth: React.PropTypes.func,
        listenToUserRoot: React.PropTypes.func,
        listenToUsedAvailability: React.PropTypes.func,
        listenToScheduleSupport: React.PropTypes.func,
        canDeleteAny: React.PropTypes.bool,
        user: React.PropTypes.shape({
            isAuth: React.PropTypes.bool,
            isAdmin: React.PropTypes.bool,
            info: React.PropTypes.shape({
                uid: React.PropTypes.string,
            }),
        }),
        deleteAvailabilityRow: React.PropTypes.func,
        updateAvailabilityRow: React.PropTypes.func,
        // eslint-disable-next-line react/forbid-prop-types
        rows: React.PropTypes.array,
        itemId: React.PropTypes.string,
    },
    componentDidMount() {
        this.props.listenToAuth()
            .then((user) => {
                this.props.listenToUserRoot(user.uid);
                this.props.listenToScheduleSupport();
                this.props.listenToUsedAvailability();
            });
    },
    componentWillReceiveProps(nextProps) {
        if (nextProps.user.isAuth && !this.props.user.isAuth) {
            this.props.listenToUserRoot(nextProps.user.info.uid);
            this.props.listenToScheduleSupport();
            this.props.listenToUsedAvailability();
        }
    },
    render() {
        return (
            <div>
                {this.props.canDeleteAny &&
                <p>
                    Use the&nbsp;
                    <a
                        href="https://www.campusinsight.com/admin-3/support-consultations-grid/"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Support Consultations Grid
                    </a>&nbsp;
                    to remove scheduled appointments to delete items below with no delete button.
                </p>
                }
                <TimeGrid
                    canDeleteAny={this.props.canDeleteAny}
                    itemId={this.props.itemId}
                    rows={this.props.rows}
                    updateAvailabilityRow={this.props.updateAvailabilityRow}
                    deleteAvailabilityRow={this.props.deleteAvailabilityRow}
                    year={this.props.year}
                />
            </div>
        );
    },
});

function mapStateToProps(state, ownProps) {
    const canDeleteAny = state.user.isAuth &&
        state.user.isAdmin &&
        state.schedule.hasEverLoaded &&
        state.schedule.hasEverLoadedUsedAvailability;
    const rows = ownProps.rows.map((row) => {
        const canDelete = canDeleteAny && getScheduleItemIsAvailable(state.schedule, row._id);
        return {
            ...row,
            canDelete,
        };
    });
    return {
        schedule: state.schedule,
        user: state.user,
        canDeleteAny,
        itemId: ownProps.itemId,
        rows,
        updateAvailabilityRow: ownProps.updateAvailabilityRow,
        deleteAvailabilityRow: ownProps.deleteAvailabilityRow,
        year: ownProps.year,
    };
}

const connected = connect(
    mapStateToProps,
    {
        listenToScheduleSupport,
        listenToUsedAvailability,
        listenToAuth,
        listenToUserRoot,
    }
)(container);

export default connected;
