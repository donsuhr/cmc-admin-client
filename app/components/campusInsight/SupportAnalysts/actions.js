import config from 'config';
import { fetchItems, updateItem, addItem, deleteItem } from '../util/fetch-util';
import { auth } from '../../preconfigured-auth';
import { checkStatus, parseJSON } from '../../fetchJsonHelpers';

const endpoint = `${config.domains.api}/campusInsight/analysts/`;

export const REQUEST_ANALYSTS = 'REQUEST_ANALYSTS';

export function requestAnalysts() {
    return {
        type: REQUEST_ANALYSTS,
    };
}

export const RECEIVE_ANALYSTS = 'RECEIVE_ANALYSTS';

export function receiveAnalysts(items) {
    return {
        type: RECEIVE_ANALYSTS,
        items: items.data,
    };
}

export const RECEIVE_ANALYST = 'RECEIVE_ANALYST';

export function receiveAnalyst(item) {
    return {
        type: RECEIVE_ANALYST,
        item,
    };
}

export const RECEIVE_ANALYST_TIMEOUT = 'RECEIVE_ANALYST_TIMEOUT';

export function receiveAnalystTimeout(item) {
    return {
        type: RECEIVE_ANALYST_TIMEOUT,
        item,
    };
}

export const REQUEST_ANALYST_UPDATE = 'REQUEST_ANALYST_UPDATE';

function requestAnalystUpdate(id) {
    return {
        type: REQUEST_ANALYST_UPDATE,
        id,
    };
}

export const ANALYST_UPDATE_ERROR = 'ANALYST_UPDATE_ERROR';

function updateAnalystError(error, id) {
    return {
        type: ANALYST_UPDATE_ERROR,
        error,
        id,
    };
}


export const REQUEST_ANALYST_ERROR = 'REQUEST_ANALYST_ERROR';

function requestAnalystError(error, id) {
    return {
        type: REQUEST_ANALYST_ERROR,
        error,
        id,
    };
}

export const REQUEST_ANALYST_ADD = 'REQUEST_ANALYST_ADD';

function requestAnalystAdd() {
    return {
        type: REQUEST_ANALYST_ADD,
    };
}

export const ANALYST_ADD_ERROR = 'ANALYST_ADD_ERROR';

function addAnalystError(error, data) {
    return {
        type: ANALYST_ADD_ERROR,
        error,
        data,
    };
}

export const REQUEST_ANALYST_DELETE = 'REQUEST_ANALYST_DELETE';

function requestAnalystDelete(id) {
    return {
        type: REQUEST_ANALYST_DELETE,
        id,
    };
}

export const RECEIVE_ANALYST_DELETE = 'RECEIVE_ANALYST_DELETE';

function receiveAnalystDelete(id) {
    return {
        type: RECEIVE_ANALYST_DELETE,
        id,
    };
}

export const REQUEST_ANALYST_DELETE_ERROR = 'REQUEST_ANALYST_DELETE_ERROR';

function requestAnalystDeleteError(id) {
    return {
        type: REQUEST_ANALYST_DELETE_ERROR,
        id,
    };
}

export const REQUEST_ANALYST_AVAILABILITY_UPDATE = 'REQUEST_ANALYST_AVAILABILITY_UPDATE';

function requestAnalystAvailabilityUpdate(id, row) {
    return {
        type: REQUEST_ANALYST_AVAILABILITY_UPDATE,
        row,
        id,
    };
}

export const REQUEST_ANALYST_AVAILABILITY_DELETE = 'REQUEST_ANALYST_AVAILABILITY_DELETE';

function requestAnalystAvailabilityDelete(id, row) {
    return {
        type: REQUEST_ANALYST_AVAILABILITY_DELETE,
        row,
        id,
    };
}

export const REQUEST_ANALYST_AVAILABILITY_DELETE_ERROR = 'REQUEST_ANALYST_AVAILABILITY_DELETE_ERROR';

function requestAnalystAvailabilityDeleteError(error, id, row) {
    return {
        type: REQUEST_ANALYST_AVAILABILITY_DELETE_ERROR,
        error,
        row,
        id,
    };
}

export const RECEIVE_ANALYST_AVAILABILITY_DELETE = 'RECEIVE_ANALYST_AVAILABILITY_DELETE';

function receiveAnalystAvailabilityDelete(id, row) {
    return {
        type: RECEIVE_ANALYST_AVAILABILITY_DELETE,
        row,
        id,
    };
}

export const RECEIVE_ANALYST_AVAILABILITY_UPDATE = 'RECEIVE_ANALYST_AVAILABILITY_UPDATE';

function receiveAnalystAvailabilityUpdate(id, row) {
    return {
        type: RECEIVE_ANALYST_AVAILABILITY_UPDATE,
        row,
        id,
    };
}

export const RECEIVE_ANALYST_AVAILABILITY_TIMEOUT = 'RECEIVE_ANALYST_AVAILABILITY_TIMEOUT';

export function receiveAnalystAvailabilityTimeout(id, row) {
    return {
        type: RECEIVE_ANALYST_AVAILABILITY_TIMEOUT,
        row,
        id,
    };
}

export function addAnalyst(item) {
    return (dispatch, getState) => {
        const { domAttributes } = getState();
        if (domAttributes && Object.hasOwnProperty.call(domAttributes, 'year')) {
            item.year = domAttributes.year;
        }
        return addItem({
            item,
            dispatch,
            endpoint,
            startAction: requestAnalystAdd,
            endAction: receiveAnalyst,
            endActionTimeout: receiveAnalystTimeout,
            errorAction: addAnalystError,
        });
    };
}

export function updateAnalyst(item, change) {
    const newItem = {
        ...item,
        ...change,
    };

    delete newItem.availability;

    return dispatch => updateItem({
        item: newItem,
        change,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestAnalystUpdate,
        endAction: receiveAnalyst,
        endActionTimeout: receiveAnalystTimeout,
        errorAction: updateAnalystError,
    });
}

export function fetchAnalysts() {
    return (dispatch, getState) => {
        const year = Object.hasOwnProperty.call(getState().domAttributes, 'year') ? `${getState().domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestAnalysts,
            endAction: receiveAnalysts,
            errorAction: requestAnalystError,
        });
    };
}

export function deleteAnalyst(item) {
    return dispatch => deleteItem({
        item,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestAnalystDelete,
        endAction: receiveAnalystDelete,
        errorAction: requestAnalystDeleteError,
    });
}

export function updateAvailabilityRow(itemId, row) {
    return (dispatch) => {
        dispatch(requestAnalystAvailabilityUpdate(itemId, row));
        const availEndpoint = `${endpoint}${itemId}/availability`;
        return new Promise((resolve, reject) => {
            fetch(availEndpoint, {
                method: 'PATCH',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    authorization: `Bearer ${auth.getToken()}`,
                },
                body: JSON.stringify(row),
            })
                .then(checkStatus)
                .then(parseJSON)
                .then((result) => {
                    dispatch(receiveAnalystAvailabilityUpdate(itemId, row));
                    setTimeout(() => {
                        dispatch(receiveAnalystAvailabilityTimeout(itemId, row));
                    }, 1000);
                    resolve(result);
                })
                .catch((err) => {
                    // eslint-disable-next-line no-console
                    console.log('error', err);
                    // eslint-disable-next-line no-debugger
                    debugger;
                    /*
                     dispatch(errorAction(err, item._id));
                     getSubmissionError(err)
                     .then((submitError) => {
                     reject(submitError);
                     });
                     */
                });
        });
    };
}

export function deleteAvailabilityRow(itemId, row) {
    return (dispatch) => {
        dispatch(requestAnalystAvailabilityDelete(itemId, row));
        const availEndpoint = `${endpoint}${itemId}/availability`;
        return new Promise((resolve, reject) => {
            fetch(availEndpoint, {
                method: 'DELETE',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    authorization: `Bearer ${auth.getToken()}`,
                },
                body: JSON.stringify(row),
            })
                .then(checkStatus)
                .then((result) => {
                    if (result.status !== 204 && result.status !== 200 && result.status !== 202) {
                        throw new Error('Error deleting item');
                    }
                })
                .then((result) => {
                    // eslint-disable-next-line no-underscore-dangle
                    dispatch(receiveAnalystAvailabilityDelete(itemId, row));
                    resolve(result);
                })
                .catch((err) => {
                    dispatch(requestAnalystAvailabilityDeleteError(err, itemId, row));
                    reject(err);
                });
        });
    };
}
