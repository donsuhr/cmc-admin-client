import React from 'react';
import ReactDataGrid from 'react-data-grid';
import sortBy from 'lodash/sortBy';
import { EmptyRowsView, RowRenderer, getUnicodeStatus } from '../util/grid-util';
import DateEditor from '../react-data-grid-editors/DateEditor';
import moment from '../util/load-moment-tz';

const dateSortBy = x => new Date(x.start);

const formatters = {
    easternDate: ({ value }) => (
        <div>{value ? moment(value).tz('America/New_York').format('YYYY-MM-DD hh:mm A') : ''}</div>
    ),
    deleteBtn: ({ dependentValues: { formatterProps, props } }) => {
        if (formatterProps.canDelete) {
            return (<div className="time-grid__remove-button-wrapper">
                <button
                    type="button"
                    className="time-grid__remove-row-btn"
                    onClick={(event) => {
                        event.preventDefault();
                        if (formatterProps.pristineValues.start === null &&
                            formatterProps.pristineValues.end === null) {
                            props.removeUnsavedRow(formatterProps);
                        } else {
                            props.deleteAvailabilityRow(
                                props.itemId,
                                formatterProps
                            );
                        }
                    }}
                >
                    Delete
                </button>
            </div>);
        }
        return (<div>&nbsp;</div>);
    },
    savingStatusIcon: ({ value }) => (
        <div>{getUnicodeStatus(value).status}</div>
    ),
};

formatters.easternDate.propTypes = {
    value: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.instanceOf(Date),
    ]),
};
formatters.deleteBtn.propTypes = {
    removeUnsavedRow: React.PropTypes.func,
    deleteAvailabilityRow: React.PropTypes.func,
    itemId: React.PropTypes.string,
    dependentValues: React.PropTypes.shape({
        formatterProps: React.PropTypes.obj,
        props: React.PropTypes.shape({
            removeUnsavedRow: React.PropTypes.func,
            deleteAvailabilityRow: React.PropTypes.func,
            itemId: React.PropTypes.string,
        }).isRequired,
    }),
};
formatters.savingStatusIcon.propTypes = {
    value: React.PropTypes.string,
};

const gridColumns = props => ([
    {
        key: 'start',
        name: 'Start',
        editable: true,
        resizable: true,
        width: 230,
        formatter: formatters.easternDate,
        editor: DateEditor,
    },
    {
        key: 'end',
        name: 'End',
        editable: true,
        resizable: true,
        width: 230,
        formatter: formatters.easternDate,
        editor: DateEditor,
    },
    {
        key: '',
        name: '',
        width: 35,
        formatter: formatters.deleteBtn,
        getRowMetaData: row => ({ formatterProps: row, props }),
    },
    {
        key: 'savingStatus',
        name: '↹',
        width: 35,
        formatter: formatters.savingStatusIcon,
        cellClass: 'presenter-grid__cell--saving',
    },
]);

const Grid = React.createClass({
    propTypes: {
        deleteAvailabilityRow: React.PropTypes.func,
        updateAvailabilityRow: React.PropTypes.func,
        // eslint-disable-next-line react/forbid-prop-types
        rows: React.PropTypes.array,
        itemId: React.PropTypes.string,
    },
    getInitialState() {
        const rows = sortBy(this.props.rows, dateSortBy);
        return { rows };
    },

    componentWillReceiveProps(nextProps) {
        if (this.props.rows !== nextProps.rows) {
            const rows = sortBy(nextProps.rows, dateSortBy);
            this.setState({ rows });
        }
    },

    onAddRowClicked(event) {
        event.preventDefault();
        const rows = [
            ...this.state.rows,
            {
                start: '',
                end: '',
                pristineValues: {
                    start: null,
                    end: null,
                },
                canDelete: true,
            },
        ];
        this.setState({ rows });
    },

    onAddYearClicked(event) {
        event.preventDefault();

        const rangeSets = {
            2017: [
                {
                    start: moment.tz('2017-04-27T13:00', 'America/New_York'),
                    end: moment.tz('2017-04-27T16:30', 'America/New_York'),
                },
                {
                    start: moment.tz('2017-04-28T09:00', 'America/New_York'),
                    end: moment.tz('2017-04-28T11:30', 'America/New_York'),
                },
                {
                    start: moment.tz('2017-04-28T13:00', 'America/New_York'),
                    end: moment.tz('2017-04-28T15:30', 'America/New_York'),
                },
            ],
            2018: [
                {
                    start: moment.tz('2018-04-18T14:30', 'America/New_York'),
                    end: moment.tz('2018-04-18T17:00', 'America/New_York'),
                },
                {
                    start: moment.tz('2018-04-19T10:00', 'America/New_York'),
                    end: moment.tz('2018-04-19T12:00', 'America/New_York'),
                },
                {
                    start: moment.tz('2018-04-19T13:00', 'America/New_York'),
                    end: moment.tz('2018-04-19T17:00', 'America/New_York'),
                },
                {
                    start: moment.tz('2018-04-20T08:30', 'America/New_York'),
                    end: moment.tz('2018-04-20T12:00', 'America/New_York'),
                },
            ],
            2019: [
                {
                    start: moment.tz('2019-03-27T14:30', 'America/New_York'),
                    end: moment.tz('2019-03-27T16:30', 'America/New_York'),
                },
                {
                    start: moment.tz('2019-03-28T10:00', 'America/New_York'),
                    end: moment.tz('2019-03-28T11:30', 'America/New_York'),
                },
                {
                    start: moment.tz('2019-03-28T13:00', 'America/New_York'),
                    end: moment.tz('2019-03-28T16:30', 'America/New_York'),
                },
                {
                    start: moment.tz('2019-03-29T08:30', 'America/New_York'),
                    end: moment.tz('2019-03-29T11:30', 'America/New_York'),
                },
            ],
            2020: [
                {
                    start: moment.tz('2020-04-29T15:00', 'America/New_York'),
                    end: moment.tz('2020-04-29T17:00', 'America/New_York'),
                },
                {
                    start: moment.tz('2020-04-30T10:30', 'America/New_York'),
                    end: moment.tz('2020-04-30T17:30', 'America/New_York'),
                },
                {
                    start: moment.tz('2020-05-01T09:00', 'America/New_York'),
                    end: moment.tz('2020-05-01T12:00', 'America/New_York'),
                },
            ],
        };
        const newRows = [];
        if (!Object.hasOwnProperty.call(rangeSets, this.props.year)) {
            throw new Error(`Range not set for year ${this.props.year}`);
        }
        const ranges = rangeSets[this.props.year];
        ranges.forEach((x) => {
            for (const m = moment(x.start); m.diff(x.end, 'minutes') <= 0; m.add(30, 'minutes')) {
                const newRow = {
                    start: m.toDate(),
                    end: m.clone().add(30, 'minutes').toDate(),
                    pristineValues: {
                        start: null,
                        end: null,
                    },
                };
                newRows.push(newRow);
                this.props.updateAvailabilityRow(this.props.itemId, newRow);
            }
        });

        const rows = [
            ...this.state.rows,
            ...newRows,
        ];

        this.setState({ rows: sortBy(rows, dateSortBy) });
    },

    handleRowUpdated(updatedRowData) {
        const newRow = {
            ...this.state.rows[updatedRowData.rowIdx],
            ...updatedRowData.updated,
        };
        const rows = [
            ...this.state.rows.slice(0, updatedRowData.rowIdx),
            newRow,
            ...this.state.rows.slice(updatedRowData.rowIdx + 1),
        ];

        const start = moment(newRow.start);
        const end = moment(newRow.end);
        if (!start.isSame(newRow.pristineValues.start) || !end.isSame(newRow.pristineValues.end)) {
            if (start.isValid() && end.isValid() && end.isAfter(start)) {
                this.props.updateAvailabilityRow(this.props.itemId, newRow);
            }
        }

        this.setState({ rows });
    },

    removeUnsavedRow(rowData) {
        const index = this.state.rows.findIndex(
            x => x.start === rowData.start && x.end === rowData.end
        );
        if (index !== -1) {
            const rows = [
                ...this.state.rows.slice(0, index),
                ...this.state.rows.slice(index + 1),
            ];
            this.setState({ rows });
        }
    },

    rowGetter(i) {
        return this.state.rows[i];
    },

    render() {
        return (
            <div>
                <ReactDataGrid
                    columns={gridColumns({
                        deleteAvailabilityRow: this.props.deleteAvailabilityRow,
                        removeUnsavedRow: this.removeUnsavedRow,
                        itemId: this.props.itemId,
                    })}
                    rowGetter={this.rowGetter}
                    rowsCount={this.state.rows.length}
                    rowRenderer={RowRenderer}
                    enableCellSelect
                    minHeight={350}
                    onRowUpdated={this.handleRowUpdated}
                    emptyRowsView={EmptyRowsView}
                    rowHeight={30}
                />
                <button
                    type="button"
                    className="analyst-form__availability-add-btn"
                    onClick={this.onAddRowClicked}
                >
                    Add Row
                </button>
                {this.props.year &&
                    <button
                        type="button"
                        className="analyst-form__availability-add-btn"
                        onClick={this.onAddYearClicked}
                    >
                        Add {this.props.year} Default
                    </button>
                }
            </div>
        );
    },
});

export default Grid;
