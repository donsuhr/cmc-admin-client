import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../util/form-util';
import TimeGrid from './TimeGridContainer';
import FirebaseLogin from '../../firebase-login';

const validate = (values) => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = 'Required';
    }
    if (!values.lastName) {
        errors.lastName = 'Required';
    }
    return errors;
};

const Form = ({
    error,
    handleSubmit,
    pristine,
    submitting,
    handleDelete,
    isEdit,
    updateAvailabilityRow,
    deleteAvailabilityRow,
    year,
    ...props
}) => (
    <div className="">
        {year &&
        <FirebaseLogin />
        }
        {!year &&
            <p>View in year mode to login to firebase to delete availability</p>
        }
        <form onSubmit={handleSubmit} className="cmc-form">
            <dl className="item-details-list">
                <Field
                    name="firstName"
                    label="First Name"
                    component={renderField}
                    type="text"
                />
                <Field
                    name="lastName"
                    label="Last Name"
                    component={renderField}
                    type="text"
                />
                <Field
                    name="topic"
                    label="Topic"
                    component={renderField}
                    type="text"
                />
                {!year &&
                <Field
                    name="year"
                    label="Year"
                    component={renderField}
                    type="text"
                />
                }
                <dt className="analyst-form__availability-dt">
                    {
                        // eslint-disable-next-line jsx-a11y/label-has-for
                    }<label>Availability</label>
                </dt>
                <dd className="analyst-form__availability-dd">
                    {props.item &&
                    <TimeGrid
                        itemId={props.item._id}
                        rows={props.item.availability}
                        updateAvailabilityRow={updateAvailabilityRow}
                        deleteAvailabilityRow={deleteAvailabilityRow}
                        year={year}
                    />
                    }
                </dd>
            </dl>
            <button
                className="item-details__save-button"
                type="submit"
                disabled={pristine || submitting}
            >
                {isEdit ? 'Save' : 'Create'}
            </button>
            <br />
            {
                props.item && <button
                    className="item-details__delete-button"
                    onClick={handleDelete}
                    type="button"
                >
                    Delete
                </button>
            }
            <p>{error && <strong>{error}</strong>}</p>
        </form>
    </div>
);

Form.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    item: React.PropTypes.object,
    isEdit: React.PropTypes.bool.isRequired,
    error: React.PropTypes.bool,
    pristine: React.PropTypes.bool,
    submitting: React.PropTypes.bool,
    handleSubmit: React.PropTypes.func.isRequired,
    handleDelete: React.PropTypes.func.isRequired,
    updateAvailabilityRow: React.PropTypes.func.isRequired,
    deleteAvailabilityRow: React.PropTypes.func.isRequired,
    year: React.PropTypes.string,
};

const ContactForm = reduxForm({
    form: 'presenterForm',
    validate,
})(Form);

export default ContactForm;
