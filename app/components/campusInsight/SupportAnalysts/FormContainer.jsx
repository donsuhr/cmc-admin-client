import React from 'react';
import { connect } from 'react-redux';
import FormWrapper from '../components/Form-wrapper';
import { addAnalyst, updateAnalyst, deleteAnalyst, updateAvailabilityRow, deleteAvailabilityRow } from './actions';
import Form from './Form';
import { getAnalystById } from '../redux/reducers';

const FormContainer = props => (
    <FormWrapper
        returnTo="/supportAnalysts"
        addAction={addAnalyst}
        updateAction={updateAnalyst}
        deleteAction={deleteAnalyst}
        getById={getAnalystById}
        stateKey="presenters"
        formName="presenterForm"
        params={props.params}
        formComponent={Form}
        year={props.year}
        updateAvailabilityRow={props.updateAvailabilityRow}
        deleteAvailabilityRow={props.deleteAvailabilityRow}
    />
);

FormContainer.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    params: React.PropTypes.object,
    updateAvailabilityRow: React.PropTypes.func,
    deleteAvailabilityRow: React.PropTypes.func,
    year: React.PropTypes.string,
};

function mapStateToProps(state, ownProps) {
    return {
        year: state.domAttributes.year,
    };
}

const FormContainerConnected = connect(
    mapStateToProps,
    {
        updateAvailabilityRow,
        deleteAvailabilityRow,
    }
)(FormContainer);

export default FormContainerConnected;
