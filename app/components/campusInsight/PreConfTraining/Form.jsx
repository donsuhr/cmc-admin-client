import React from 'react';
import { Field, reduxForm } from 'redux-form';
import moment from '../util/load-moment-tz';
import {
    renderField,
    renderTextArea,
    renderMultiSelect,
    renderDateTimeLocal,
    normalizeToArray,
} from '../util/form-util';

const validate = (values) => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Required';
    }
    if (moment(values.start) > moment(values.end)) {
        errors.end = 'End should be after start.';
    }
    return errors;
};

const Form = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        isEdit: React.PropTypes.bool.isRequired,
        error: React.PropTypes.bool,
        pristine: React.PropTypes.bool,
        submitting: React.PropTypes.bool,
        handleSubmit: React.PropTypes.func.isRequired,
        handleDelete: React.PropTypes.func.isRequired,
        presenters: React.PropTypes.object,
        locations: React.PropTypes.object,
        products: React.PropTypes.object,
        pctTracks: React.PropTypes.object,
        year: React.PropTypes.string,
    },

    updateSite(val, instance) {
        const startValue = moment(val);
        const endFieldComponent = this.endField.getRenderedComponent();
        let endFieldValue;
        if (typeof endFieldComponent.props.input.value === 'object') {
            endFieldValue = moment(endFieldComponent.props.input.value);
        } else if (typeof endFieldComponent.props.input.value === 'string') {
            endFieldValue = moment(endFieldComponent.props.input.value).tz('America/New_York');
        } else {
            endFieldValue = startValue;
        }
        if (startValue.diff(endFieldValue, 'min') >= 0) {
            const later = startValue.clone().add(45, 'm').toDate();
            endFieldComponent.toggleWarning(true);
            endFieldComponent.updateValue(later);
        }
    },

    render() {
        const {
            error,
            handleSubmit,
            pristine,
            submitting,
            presenters,
            locations,
            products,
            handleDelete,
            pctTracks,
            isEdit,
            year,
            ...props
        } = this.props;
        const presentersOptions = Object.keys(presenters.byId).map(x => ({
            value: presenters.byId[x]._id,
            label: `${presenters.byId[x].lastName}, ${presenters.byId[x].firstName}`,
        }));
        const locationsOptions = Object.keys(locations.byId).map(x => ({
            value: locations.byId[x]._id,
            label: locations.byId[x].title,
            disabled: !locations.byId[x].active,
        }));
        const productsOptions = Object.keys(products.byId).map(x => ({
            value: products.byId[x]._id,
            label: products.byId[x].title,
        }));
        const tracksOptions = Object.keys(pctTracks.byId).map(x => ({
            value: pctTracks.byId[x]._id,
            label: pctTracks.byId[x].title,
        }));
        return (
            <div className="cmc-form--pre-conf-training">
                <form onSubmit={handleSubmit} className="cmc-form">
                    <dl className="item-details-list">
                        <Field name="title" label="Title *" component={renderField} type="text" />
                        <Field name="description" label="Description" component={renderTextArea} />

                        <dt>Status</dt>
                        <dd>
                            <ul>
                                <li>
                                    <Field
                                        name="status"
                                        id="status--open"
                                        component="input"
                                        type="radio"
                                        value="Open"
                                    />
                                    <label htmlFor="status--open"> Open</label>
                                </li>
                                <li>
                                    <Field
                                        name="status"
                                        id="status--few-seats"
                                        component="input"
                                        type="radio"
                                        value="Few Seats"
                                    />
                                    <label htmlFor="status--few-seats"> Few Seats</label>
                                </li>
                                <li>
                                    <Field
                                        name="status"
                                        id="status--full"
                                        component="input"
                                        type="radio"
                                        value="Full"
                                    />
                                    <label htmlFor="status--full"> Full</label>
                                </li>
                                <li>
                                    <Field
                                        name="status"
                                        id="status--closed"
                                        component="input"
                                        type="radio"
                                        value="Closed"
                                    />
                                    <label htmlFor="status--closed"> Closed</label>
                                </li>
                            </ul>
                        </dd>
                        <Field
                            name="product._id"
                            label="Product"
                            component={renderMultiSelect}
                            options={productsOptions}
                            isLoading={presenters.fetching}
                        />
                        <Field
                            name="track._id"
                            label="Track"
                            component={renderMultiSelect}
                            options={tracksOptions}
                            isLoading={presenters.fetching}
                        />
                        <Field
                            multi
                            name="presenter"
                            label="Presenters"
                            normalize={normalizeToArray}
                            component={renderMultiSelect}
                            options={presentersOptions}
                            isLoading={presenters.fetching}
                        />
                        <Field
                            name="location._id"
                            label="Location"
                            component={renderMultiSelect}
                            options={locationsOptions}
                            isLoading={locations.fetching}
                        />
                        <Field
                            name="start"
                            label="Start Time"
                            year={year}
                            component={renderDateTimeLocal}
                            onChangeAction={this.updateSite}
                            item={props.item}
                        />
                        <Field
                            ref={(x) => {
                                this.endField = x;
                            }}
                            withRef
                            name="end"
                            year={year}
                            label="End Time"
                            component={renderDateTimeLocal}
                            item={props.item}
                        />
                        <Field
                            name="learningObjectives"
                            label="Learning Objectives"
                            component={renderTextArea}
                        />
                        <Field
                            name="targetAudience"
                            label="Target Audience"
                            component={renderTextArea}
                        />
                        <Field
                            name="prerequisites"
                            label="Prerequisites"
                            component={renderTextArea}
                        />
                        <Field
                            name="relatedCourses"
                            label="Related Courses"
                            component={renderTextArea}
                        />

                        <dt><label htmlFor="level">Level</label></dt>
                        <dd>
                            <Field name="level" label="Level" component="select" id="level">
                                <option value="" />
                                <option value="Introductory">Introductory</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="Advanced">Advanced</option>
                                <option value="All Levels">All Levels</option>
                            </Field>
                        </dd>
                        <dt><label htmlFor="type">Type</label></dt>
                        <dd>
                            <Field name="type" label="Type" component="select" id="type">
                                <option value="" />
                                <option value="Operations">Operations</option>
                                <option value="Technical">Technical</option>
                                <option value="Configuration">Configuration</option>
                                <option value="Advanced">Advanced</option>
                                <option value="Functional">Functional</option>
                            </Field>
                        </dd>
                        <dt><label htmlFor="price">Price</label></dt>
                        <dd>
                            <Field name="price" component="input" type="number" placeholder="$" />
                        </dd>

                        <Field
                            name="active"
                            label="Active"
                            component={renderField}
                            type="checkbox"
                        />
                        <Field
                            name="flagAsNew"
                            label="New"
                            component={renderField}
                            type="checkbox"
                        />
                        {
                            props.item && <div>
                                <dt>Created At</dt>
                                <dd>{moment(props.item.createdAt).format('YYYY-MM-DD hh:mm A')}</dd>
                                <dt>Updated At</dt>
                                <dd>{moment(props.item.updatedAt).format('YYYY-MM-DD hh:mm A')}</dd>
                            </div>
                        }
                    </dl>
                    <button
                        className="item-details__save-button"
                        type="submit"
                        disabled={pristine || submitting}
                    >
                        {isEdit ? 'Save' : 'Create'}
                    </button>
                    <br />
                    {
                        props.item && <button
                            className="item-details__delete-button"
                            onClick={handleDelete}
                            type="button"
                        >
                            Delete
                        </button>
                    }
                    <p>{error && <strong>{error}</strong>}</p>
                </form>
            </div>
        );
    },
});

const ContactForm = reduxForm({
    form: 'pctForm',
    validate,
})(Form);

export default ContactForm;
