import React from 'react';
import { connect } from 'react-redux';
import isObject from 'lodash/isObject';
import FormWrapper from '../components/Form-wrapper';
import { addPct, updatePct, deletePct } from './actions';
import { fetchPresenters } from '../Presenters/actions';
import { fetchLocations } from '../Locations/actions';
import { fetchProducts } from '../Products/actions';
import { fetchPctTracks } from '../PctTracks/actions';
import Form from './Form';
import { getPCTById } from '../redux/reducers';

const FormContainer = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        params: React.PropTypes.object,
        presenters: React.PropTypes.object,
        locations: React.PropTypes.object,
        products: React.PropTypes.object,
        pctTracks: React.PropTypes.object,
        fetchPresenters: React.PropTypes.func.isRequired,
        fetchLocations: React.PropTypes.func.isRequired,
        fetchProducts: React.PropTypes.func.isRequired,
        fetchPctTracks: React.PropTypes.func.isRequired,
    },

    componentDidMount() {
        if (!this.props.presenters.fetching &&
            Object.keys(this.props.presenters.byId).length === 0) {
            this.props.fetchPresenters();
        }
        if (!this.props.locations.fetching &&
            Object.keys(this.props.locations.byId).length === 0) {
            this.props.fetchLocations();
        }
        if (!this.props.products.fetching &&
            Object.keys(this.props.products.byId).length === 0) {
            this.props.fetchProducts();
        }
        if (!this.props.pctTracks.fetching &&
            Object.keys(this.props.pctTracks.byId).length === 0) {
            this.props.fetchPctTracks();
        }
    },

    preSubmitProcess(data) {
        function convertObjToId(obj) {
            let ret = { ...obj };
            if (isObject(ret)) {
                ret = ret._id === '' ? null : ret._id;
            }
            return ret;
        }

        const ret = { ...data };
        if (ret.location) {
            ret.location = convertObjToId(ret.location);
        }
        if (ret.track) {
            ret.track = convertObjToId(ret.track);
        }
        if (ret.product) {
            ret.product = convertObjToId(ret.product);
        }

        return ret;
    },

    render() {
        return (
            <FormWrapper
                returnTo="/pcts"
                addAction={addPct}
                updateAction={updatePct}
                deleteAction={deletePct}
                getById={getPCTById}
                stateKey="pcts"
                formName="pctForm"
                presenters={this.props.presenters}
                year={this.props.year}
                locations={this.props.locations}
                products={this.props.products}
                pctTracks={this.props.pctTracks}
                params={this.props.params}
                formComponent={Form}
                preSubmitProcess={this.preSubmitProcess}
            />
        );
    },
});

function mapStateToProps(state, ownProps) {
    return {
        presenters: state.presenters,
        locations: state.locations,
        products: state.products,
        pctTracks: state.pctTracks,
        year: state.domAttributes.year,
    };
}

const FormContainerConnected = connect(mapStateToProps, {
    fetchPresenters,
    fetchLocations,
    fetchProducts,
    fetchPctTracks,
})(FormContainer);

export default FormContainerConnected;
