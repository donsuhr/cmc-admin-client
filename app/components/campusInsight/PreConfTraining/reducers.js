import { combineReducers } from 'redux';
import * as actions from './actions';
import { RECEIVE_LOCATION_DELETE } from '../Locations/actions';
import { RECEIVE_PRESENTER, RECEIVE_PRESENTER_DELETE } from '../Presenters/actions';
import { RECEIVE_PCT_TRACK_DELETE } from '../PctTracks/actions';
import { RECEIVE_PRODUCT_DELETE } from '../Products/actions';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.REQUEST_PCT_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.RECEIVE_PCT:
            return {
                ...state,
                ...action.item,
                presenterCount: action.item.presenter.length || 0,
                idCopy: action.item._id,
                savingStatus: 'saved',
            };
        case actions.RECEIVE_PCTS:
            return {
                ...state,
                ...currentItem,
                presenterCount: currentItem.presenter.length || 0,
                idCopy: currentItem._id,
                savingStatus: '',
            };
        case actions.RECEIVE_PCT_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.PCT_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_PCTS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});
        case actions.REQUEST_PCT_UPDATE:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_PCT_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case actions.RECEIVE_PCT:
        case actions.RECEIVE_PCT_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.PCT_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case RECEIVE_LOCATION_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key].location && state[key].location._id === action.id) {
                    state[key].location = undefined;
                }
                acc[key] = state[key];
                return acc;
            }, {});
        case RECEIVE_PCT_TRACK_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key].track && state[key].track._id === action.id) {
                    state[key].track = undefined;
                }
                acc[key] = state[key];
                return acc;
            }, {});
        case RECEIVE_PRODUCT_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key].product && state[key].product._id === action.id) {
                    state[key].product = undefined;
                }
                acc[key] = state[key];
                return acc;
            }, {});
        case RECEIVE_PRESENTER:
            const updatedPresenterPctIdList = action.item.pct.map(x => x._id);
            return Object.keys(state).reduce((acc, c) => {
                const current = state[c];
                const newPresenterIdList = state[c].presenter.slice();
                if (newPresenterIdList.includes(action.item._id)) {
                    if (!updatedPresenterPctIdList.includes(current._id)) {
                        // this pct was removed from the presenter
                        newPresenterIdList.splice(newPresenterIdList.indexOf(action.item._id), 1);
                    }
                }
                action.item.pct.forEach((session) => {
                    if (session._id === current._id) {
                        if (!newPresenterIdList.includes(action.item._id)) {
                            // this session was added to the presenter
                            newPresenterIdList.push(action.item._id);
                        }
                    }
                });
                acc[c] = {
                    ...current,
                    presenter: newPresenterIdList,
                };
                return acc;
            }, {});
        case RECEIVE_PRESENTER_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key].presenter.includes(action.id)) {
                    const index = state[key].presenter.indexOf(action.id);
                    state[key].presenter = [
                        ...state[key].presenter.slice(0, index),
                        ...state[key].presenter.slice(index + 1),
                    ];
                }
                acc[key] = state[key];
                return acc;
            }, {});

        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_PCTS:
        case actions.REQUEST_PCT_ADD:
        case actions.REQUEST_PCT_UPDATE:
        case actions.REQUEST_PCT_DELETE:
            return true;
        case actions.RECEIVE_PCTS:
        case actions.PCT_ADD_ERROR:
        case actions.RECEIVE_PCT:
        case actions.PCT_UPDATE_ERROR:
        case actions.RECEIVE_PCT_DELETE:
        case actions.REQUEST_PCT_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

const pcts = combineReducers({
    fetching,
    byId,
});

export default pcts;

export function getById(state, id) {
    return state.byId[id];
}

export function getPresenterPcts(state, presenterId) {
    if (Object.keys(state.byId).length < 1) {
        return [];
    }
    return Object.keys(state.byId).reduce((acc, pctId) => {
        if (state.byId[pctId].presenter.includes(presenterId)) {
            acc.push(pctId);
        }
        return acc;
    }, []);
}
