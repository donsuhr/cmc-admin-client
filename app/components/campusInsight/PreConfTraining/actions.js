import merge from 'lodash/merge';
import config from 'config';
import {
    fetchItems,
    updateItem,
    addItem,
    deleteItem,
} from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/pcts/`;

function normalizeResponse(response) {
    //   const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = response.data.map(x =>
        merge(
            {},
            {
                title: '',
            },
            x
        ));
    return response;
}

export const REQUEST_PCTS = 'REQUEST_PCTS';

export function requestPcts() {
    return {
        type: REQUEST_PCTS,
    };
}

export const RECEIVE_PCTS = 'RECEIVE_PCTS';

export function receivePcts(items) {
    return {
        type: RECEIVE_PCTS,
        items: normalizeResponse(items),
    };
}

export const RECEIVE_PCT = 'RECEIVE_PCT';

export function receivePct(item) {
    return {
        type: RECEIVE_PCT,
        item: normalizeResponse({ data: [item] })[0],
    };
}

export const RECEIVE_PCT_TIMEOUT = 'RECEIVE_PCT_TIMEOUT';

export function receivePctTimeout(item) {
    return {
        type: RECEIVE_PCT_TIMEOUT,
        item,
    };
}

export const REQUEST_PCT_UPDATE = 'REQUEST_PCT_UPDATE';

function requestPctUpdate(id) {
    return {
        type: REQUEST_PCT_UPDATE,
        id,
    };
}

export const REQUEST_PCT_DELETE = 'REQUEST_PCT_DELETE';

function requestPctDelete(id) {
    return {
        type: REQUEST_PCT_DELETE,
        id,
    };
}

export const RECEIVE_PCT_DELETE = 'RECEIVE_PCT_DELETE';

function receivePctDelete(id) {
    return {
        type: RECEIVE_PCT_DELETE,
        id,
    };
}

export const REQUEST_PCT_DELETE_ERROR = 'REQUEST_PCT_DELETE_ERROR';

function requestPctDeleteError(id) {
    return {
        type: REQUEST_PCT_DELETE_ERROR,
        id,
    };
}

export const PCT_UPDATE_ERROR = 'PCT_UPDATE_ERROR';

function requestPctError(error, id) {
    return {
        type: PCT_UPDATE_ERROR,
        error,
        id,
    };
}

export const REQUEST_PCT_ADD = 'REQUEST_PCT_ADD';

function requestPctAdd() {
    return {
        type: REQUEST_PCT_ADD,
    };
}

export const PCT_ADD_ERROR = 'PCT_ADD_ERROR';

function addPctError(error, data) {
    return {
        type: PCT_ADD_ERROR,
        error,
        data,
    };
}

export function addPct(item) {
    return dispatch =>
        addItem({
            item,
            dispatch,
            endpoint,
            startAction: requestPctAdd,
            endAction: receivePct,
            endActionTimeout: receivePctTimeout,
            errorAction: addPctError,
        });
}

export function updatePct(item, change) {
    return dispatch =>
        updateItem({
            item,
            change,
            dispatch,
            endpoint: `${endpoint}${item._id}`,
            startAction: requestPctUpdate,
            endAction: receivePct,
            endActionTimeout: receivePctTimeout,
            errorAction: requestPctError,
        });
}

export function fetchPcts() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `${state.domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestPcts,
            endAction: receivePcts,
            errorAction: requestPctError,
        });
    };
}

export function deletePct(item) {
    return dispatch =>
        deleteItem({
            item,
            dispatch,
            endpoint: `${endpoint}${item._id}`,
            startAction: requestPctDelete,
            endAction: receivePctDelete,
            errorAction: requestPctDeleteError,
        });
}
