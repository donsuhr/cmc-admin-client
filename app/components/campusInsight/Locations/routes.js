/* eslint-disable no-shadow */
module.exports = {
    path: 'locations',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./GridContainer').default);
        });
    },

    childRoutes: [
        {
            path: 'add',
            getComponent(nextState, cb) {
                require.ensure([], (require) => {
                    cb(null, require('./FormContainer').default);
                });
            },
        },
        {
            path: 'edit/:id',
            getComponent(nextState, cb) {
                require.ensure([], (require) => {
                    cb(null, require('./FormContainer').default);
                });
            },
        },
    ],
};
