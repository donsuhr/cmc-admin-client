import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import { fetchLocations, updateLocation } from './actions';
import CheckBoxEditor from '../react-data-grid-editors/CheckBoxEditor';
import Grid from '../components/Grid';
import { getUnicodeStatus } from '../util/grid-util';
import { getById } from './reducers';

/* eslint-disable react/prop-types */
const gridColumns = gridContainerProps => {
    const ret = [
        {
            key: 'title',
            name: 'Title',
            editable: true,
            sortable: true,
            resizable: true,
            width: 250,
        },
        {
            key: 'active',
            name: 'Act',
            editable: true,
            sortable: true,
            width: 35,
            formatter: ({ value }) => (<div>{value ? '✓' : '✖'}</div>),
            cellClass: 'presenter-grid__cell--active',
            editor: CheckBoxEditor,
        },
        {
            key: 'savingStatus',
            name: '↹',
            width: 35,
            formatter: ({ value }) => (<div>{getUnicodeStatus(value).status}</div>),
            cellClass: 'presenter-grid__cell--saving',
        },
        {
            key: '_id',
            name: '',
            width: 35,
            formatter: props => (
                <div>
                    <Link to={`${props.dependentValues.gridProps.location.pathname}/edit/${props.value}`}>
                        Edit
                    </Link>
                </div>
            ),
            cellClass: 'presenter-grid__cell--actions',
            getRowMetaData() {
                return { gridProps: gridContainerProps };
            },
        },
    ];
    if (!gridContainerProps.year) {
        const index = ret.length - 2;
        return [
            ...ret.slice(0, index),
            {
                key: 'year',
                name: 'Year',
                editable: true,
                sortable: true,
                resizable: true,
                width: 50,
            },
            ...ret.slice(index),
        ];
    }
    return ret;
};
/* eslint-enable react/prop-types */

const GridContainer = props => (
    <Grid
        columns={gridColumns(props)}
        {...props}
    />
);

function mapStateToProps(state, ownProps) {
    return {
        dataById: state.locations.byId,
        fetching: state.locations.fetching,
        getItemById: getById.bind(null, state.locations),
        year: state.domAttributes.year,
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        update: bindActionCreators(updateLocation, dispatch),
        fetch: bindActionCreators(fetchLocations, dispatch),
    };
}

const GridConnected = connect(mapStateToProps, mapDispatchToProps)(GridContainer);

export default GridConnected;
