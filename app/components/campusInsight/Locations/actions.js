import merge from 'lodash/merge';
import omitBy from 'lodash/omitBy';
import isNil from 'lodash/isNil';
import config from 'config';
import { fetchItems, updateItem, addItem, deleteItem } from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/locations/`;

function normalizeResponse(response) {
    const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = nullRemoved.map(x => merge({}, {
        title: '',
    }, x));
    return response;
}

export const REQUEST_LOCATIONS = 'REQUEST_LOCATIONS';

export function requestLocations() {
    return {
        type: REQUEST_LOCATIONS,
    };
}

export const RECEIVE_LOCATIONS = 'RECEIVE_LOCATIONS';

export function receiveLocations(items) {
    return {
        type: RECEIVE_LOCATIONS,
        items: normalizeResponse(items),
    };
}

export const RECEIVE_LOCATION = 'RECEIVE_LOCATION';

export function receiveLocation(item) {
    return {
        type: RECEIVE_LOCATION,
        item: normalizeResponse({ data: [item] })[0],
    };
}

export const RECEIVE_LOCATION_TIMEOUT = 'RECEIVE_LOCATION_TIMEOUT';

export function receiveLocationTimeout(item) {
    return {
        type: RECEIVE_LOCATION_TIMEOUT,
        item,
    };
}

export const REQUEST_LOCATION_UPDATE = 'REQUEST_LOCATION_UPDATE';

function requestLocationUpdate(id) {
    return {
        type: REQUEST_LOCATION_UPDATE,
        id,
    };
}

export const LOCATION_UPDATE_ERROR = 'LOCATION_UPDATE_ERROR';

function requestLocationError(error, id) {
    return {
        type: LOCATION_UPDATE_ERROR,
        error,
        id,
    };
}

export const REQUEST_LOCATION_ADD = 'REQUEST_LOCATION_ADD';

function requestLocationAdd() {
    return {
        type: REQUEST_LOCATION_ADD,
    };
}

export const LOCATION_ADD_ERROR = 'LOCATION_ADD_ERROR';

function addLocationError(error, data) {
    return {
        type: LOCATION_ADD_ERROR,
        error,
        data,
    };
}

export const REQUEST_LOCATION_DELETE = 'REQUEST_LOCATION_DELETE';

function requestLocationDelete(id) {
    return {
        type: REQUEST_LOCATION_DELETE,
        id,
    };
}

export const RECEIVE_LOCATION_DELETE = 'RECEIVE_LOCATION_DELETE';

function receiveLocationDelete(id) {
    return {
        type: RECEIVE_LOCATION_DELETE,
        id,
    };
}

export const REQUEST_LOCATION_DELETE_ERROR = 'REQUEST_LOCATION_DELETE_ERROR';

function requestLocationDeleteError(id) {
    return {
        type: REQUEST_LOCATION_DELETE_ERROR,
        id,
    };
}

export function addLocation(item) {
    return (dispatch, getState) => {
        const { domAttributes } = getState();
        if (domAttributes && Object.hasOwnProperty.call(domAttributes, 'year')) {
            item.year = domAttributes.year;
        }
        return addItem({
            item,
            dispatch,
            endpoint,
            startAction: requestLocationAdd,
            endAction: receiveLocation,
            endActionTimeout: receiveLocationTimeout,
            errorAction: addLocationError,
        });
    };
}

export function updateLocation(item, change) {
    const newItem = {
        ...item,
        ...change,
    };

    return dispatch => updateItem({
        item: newItem,
        change,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestLocationUpdate,
        endAction: receiveLocation,
        endActionTimeout: receiveLocationTimeout,
        errorAction: requestLocationError,
    });
}

export function fetchLocations() {
    return (dispatch, getState) => {
        const year = Object.hasOwnProperty.call(getState().domAttributes, 'year') ? `${getState().domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestLocations,
            endAction: receiveLocations,
            errorAction: requestLocationError,
        });
    };
}

export function deleteLocation(item) {
    return dispatch => deleteItem({
        item,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestLocationDelete,
        endAction: receiveLocationDelete,
        errorAction: requestLocationDeleteError,
    });
}
