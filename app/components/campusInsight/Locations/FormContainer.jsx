import React from 'react';
import { connect } from 'react-redux';

import FormWrapper from '../components/Form-wrapper';
import { addLocation, updateLocation, deleteLocation } from './actions';
import Form from './Form';
import { getLocationById } from '../redux/reducers';

const FormContainer = ({ params, year }) => (
    <FormWrapper
        returnTo="/locations"
        addAction={addLocation}
        updateAction={updateLocation}
        getById={getLocationById}
        stateKey="locations"
        formName="locationForm"
        deleteAction={deleteLocation}
        year={year}
        params={params}
        formComponent={Form}
    />
);

FormContainer.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    params: React.PropTypes.object,
    year: React.PropTypes.string,
};

function mapStateToProps(state, ownProps) {
    return {
        year: state.domAttributes.year,
    };
}

const FormContainerConnected = connect(
    mapStateToProps
)(FormContainer);

export default FormContainerConnected;
