import React from 'react';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';

import { renderField } from '../util/form-util';

const validate = (values) => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Required';
    }
    return errors;
};

const Form = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        item: React.PropTypes.object,
        isEdit: React.PropTypes.bool.isRequired,
        error: React.PropTypes.bool,
        pristine: React.PropTypes.bool,
        submitting: React.PropTypes.bool,
        handleSubmit: React.PropTypes.func.isRequired,
        handleDelete: React.PropTypes.func.isRequired,
        year: React.PropTypes.string,
    },

    componentDidMount() {
        this.inputNode.focus();
    },

    render() {
        const {
            error, pristine, submitting, handleSubmit, handleDelete, isEdit, year, ...props
        } = this.props;

        return (
            <div className="">
                <form onSubmit={handleSubmit} className="cmc-form">
                    <dl className="item-details-list">
                        <Field
                            inputRef={(node) => {
                                this.inputNode = node;
                            }}
                            name="title"
                            label="Title"
                            component={renderField}
                            type="text"
                        />
                        <Field
                            name="active"
                            label="Active"
                            component={renderField}
                            type="checkbox"
                        />
                        {!year &&
                        <Field
                            name="year"
                            label="Year"
                            component={renderField}
                            type="text"
                        />
                        }
                        {
                            props.item && <div>
                                <dt>Created At</dt>
                                <dd>{moment(props.item.createdAt).format('YYYY-MM-DD hh:mm A')}</dd>
                                <dt>Updated At</dt>
                                <dd>{moment(props.item.updatedAt).format('YYYY-MM-DD hh:mm A')}</dd>
                            </div>
                        }
                    </dl>
                    <button
                        className="item-details__save-button"
                        type="submit"
                        disabled={pristine || submitting}
                    >
                        {isEdit ? 'Save' : 'Create'}
                    </button>
                    <br />
                    {
                        props.item && <button
                            className="item-details__delete-button"
                            onClick={handleDelete}
                            type="button"
                        >
                            Delete
                        </button>
                    }
                    <p>{error && <strong>{error}</strong>}</p>
                </form>
            </div>
        );
    },
});

const ReduxForm = reduxForm({
    form: 'locationForm',
    validate,
})(Form);

export default ReduxForm;
