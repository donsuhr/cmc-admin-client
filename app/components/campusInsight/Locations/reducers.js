import { combineReducers } from 'redux';
import * as actions from './actions';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.REQUEST_LOCATION_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.RECEIVE_LOCATION:
            return {
                ...state,
                ...action.item,
                savingStatus: 'saved',
            };
        case actions.RECEIVE_LOCATIONS:
            return {
                ...state,
                ...currentItem,
                savingStatus: '',
            };
        case actions.RECEIVE_LOCATION_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.LOCATION_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_LOCATIONS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});
        case actions.REQUEST_LOCATION_UPDATE:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_LOCATION:
        case actions.RECEIVE_LOCATION_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.RECEIVE_LOCATION_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case actions.LOCATION_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };

        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_LOCATIONS:
        case actions.REQUEST_LOCATION_ADD:
        case actions.REQUEST_LOCATION_UPDATE:
        case actions.REQUEST_LOCATION_DELETE:
            return true;
        case actions.RECEIVE_LOCATIONS:
        case actions.LOCATION_ADD_ERROR:
        case actions.RECEIVE_LOCATION:
        case actions.LOCATION_UPDATE_ERROR:
        case actions.RECEIVE_LOCATION_DELETE:
        case actions.REQUEST_LOCATION_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

const locations = combineReducers({
    fetching,
    byId,
});

export default locations;

export function getById(state, id) {
    return state.byId[id];
}
