import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';

import presenters, * as presenterSelectors from '../Presenters/reducers';
import analysts, * as analystSelectors from '../SupportAnalysts/reducers';
import sessions, * as sessionSelectors from '../Sessions/reducers';
import pcts, * as pctsSelectors from '../PreConfTraining/reducers';
import products, * as productSelectors from '../Products/reducers';
import locations, * as locationSelectors from '../Locations/reducers';
import sessionTracks, * as sessionTrackSelectors from '../SessionTracks/reducers';
import pctTracks, * as pctTrackSelectors from '../PctTracks/reducers';
import schedule from '../../firebase-login/schedule.support.reducer';
import easybook from '../../firebase-login/easybook.reducer';
import userProfiles from '../../firebase-login/userProfiles.reducer';
import user from '../../firebase-login/auth.reducer';
import domAttributes from './domAttributes.reducers';
import reminderEmail from '../ConsultationReminder/reducer';

export default combineReducers({
    sessions,
    presenters,
    analysts,
    pcts,
    locations,
    products,
    sessionTracks,
    pctTracks,
    schedule,
    user,
    form: formReducer,
    routing: routerReducer,
    domAttributes,
    easybook,
    userProfiles,
    reminderEmail,
});

export const getPresenterById = (state, id) =>
    presenterSelectors.getById(state, id);
export const getAnalystById = (state, id) =>
    analystSelectors.getById(state, id);
export const getSessionById = (state, id) =>
    sessionSelectors.getById(state.sessions, id);
export const getPCTById = (state, id) => pctsSelectors.getById(state.pcts, id);
export const getLocationById = (state, id) =>
    locationSelectors.getById(state.locations, id);
export const getProductById = (state, id) =>
    productSelectors.getById(state.products, id);
export const getSessionTrackById = (state, id) =>
    sessionTrackSelectors.getById(state.sessionTracks, id);
export const getPctTrackById = (state, id) =>
    pctTrackSelectors.getById(state.pctTracks, id);

export const getSupportConsultationData = (state) => {
    const usedAvailabilityIds = state.schedule.usedAvailabilityId;
    const userProfilesState = state.userProfiles;
    const easyBookState = state.easybook;
    const getAppointmentFromId = analystSelectors.getAppointmentFromId.bind(
        null,
        state.analysts
    );

    const defaultProfile = {
        userFirstName: '',
        userLastName: '',
        userInstitution: '',
        userEmail: '',
        userPhone: '',
        start: '',
        end: '',
        analystFirstName: '',
        analystLastName: '',
        topic: '',
        mode: '',
        profileLoadState: '',
        analystLoadState: '',
    };
    return Object.keys(usedAvailabilityIds).reduce((acc, availabilityId) => {
        const { userId } = usedAvailabilityIds[availabilityId];
        acc[availabilityId] = {
            ...defaultProfile,
        };
        const profile = acc[availabilityId];

        if (userId === 'easybook') {
            profile.mode = 'easybook';

            const hasEasyBookKey = Object.hasOwnProperty.call(
                easyBookState.byAvailabilityId,
                availabilityId
            );
            if (hasEasyBookKey) {
                const easyBookProfile =
                    easyBookState.byAvailabilityId[availabilityId].val.info;
                if (easyBookProfile === 'closed') {
                    profile.userFirstName = 'closed';
                    profile.userLastName = 'closed';
                    profile.userInstitution = 'closed';
                } else {
                    profile.userFirstName = easyBookProfile.firstName;
                    profile.userLastName = easyBookProfile.lastName;
                    profile.userInstitution = easyBookProfile.institution;
                }
                profile.profileLoadState = 'loaded';
            } else if (easyBookState.fetching && !easyBookState.hasEverLoaded) {
                profile.profileLoadState = 'loading';
            } else {
                profile.profileLoadState = 'not requested';
            }
        } else {
            profile.mode = 'regularbook';

            const hasProfileKey = Object.hasOwnProperty.call(
                state.userProfiles.byId,
                userId
            );

            if (hasProfileKey) {
                const regularProfile = userProfilesState.byId[userId].val;
                if (userProfilesState.byId[userId].loading) {
                    profile.profileLoadState = 'loading';
                } else {
                    profile.userFirstName = regularProfile.firstName;
                    profile.userLastName = regularProfile.lastName;
                    profile.userInstitution = regularProfile.institution;
                    profile.userEmail = regularProfile.email;
                    profile.userPhone = regularProfile.phone;
                    profile.profileLoadState = 'loaded';
                }
            } else {
                profile.profileLoadState = 'not requested';
            }
        }
        const analystsLoaded =
            state.analysts.hasEverLoaded && !state.analysts.fetching;

        if (analystsLoaded) {
            const {
                analystObj: {
                    firstName: analystFirstName,
                    lastName: analystLastName,
                    topic,
                },
                availabilityObj: { start, end },
            } = getAppointmentFromId(availabilityId);
            profile.analystFirstName = analystFirstName;
            profile.analystLastName = analystLastName;

            profile.topic = topic;
            profile.start = start;
            profile.end = end;
        }
        if (!state.analysts.fetching) {
            if (!state.analysts.hasEverLoaded) {
                profile.analystLoadState = 'not requested';
            } else {
                profile.analystLoadState = 'loaded';
            }
        } else {
            profile.analystLoadState = 'loading';
        }

        return acc;
    }, {});
};
