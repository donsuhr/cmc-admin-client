import merge from 'lodash/merge';
import omitBy from 'lodash/omitBy';
import isNil from 'lodash/isNil';
import config from 'config';
import { fetchItems, updateItem, addItem, deleteItem } from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/pctTracks/`;

function normalizeResponse(response) {
    const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = nullRemoved.map(x => merge({}, {
        title: '',
    }, x));
    return response;
}

export const REQUEST_PCT_TRACKS = 'REQUEST_PCT_TRACKS';

export function requestPctTracks() {
    return {
        type: REQUEST_PCT_TRACKS,
    };
}

export const RECEIVE_PCT_TRACKS = 'RECEIVE_PCT_TRACKS';

export function receivePctTracks(items) {
    return {
        type: RECEIVE_PCT_TRACKS,
        items: normalizeResponse(items),
    };
}

export const RECEIVE_PCT_TRACK = 'RECEIVE_PCT_TRACK';

export function receivePctTrack(item) {
    return {
        type: RECEIVE_PCT_TRACK,
        item: normalizeResponse({ data: [item] })[0],
    };
}

export const RECEIVE_PCT_TRACK_TIMEOUT = 'RECEIVE_PCT_TRACK_TIMEOUT';

export function receivePctTrackTimeout(item) {
    return {
        type: RECEIVE_PCT_TRACK_TIMEOUT,
        item,
    };
}

export const REQUEST_PCT_TRACK_UPDATE = 'REQUEST_PCT_TRACK_UPDATE';

function requestPctTrackUpdate(id) {
    return {
        type: REQUEST_PCT_TRACK_UPDATE,
        id,
    };
}

export const PCT_TRACK_UPDATE_ERROR = 'PCT_TRACK_UPDATE_ERROR';

function requestPctTrackError(error, id) {
    return {
        type: PCT_TRACK_UPDATE_ERROR,
        error,
        id,
    };
}

export const REQUEST_PCT_TRACK_ADD = 'REQUEST_PCT_TRACK_ADD';

function requestPctTrackAdd() {
    return {
        type: REQUEST_PCT_TRACK_ADD,
    };
}

export const PCT_TRACK_ADD_ERROR = 'PCT_TRACK_ADD_ERROR';

function addPctTrackError(error, data) {
    return {
        type: PCT_TRACK_ADD_ERROR,
        error,
        data,
    };
}

export const REQUEST_PCT_TRACK_DELETE = 'REQUEST_PCT_TRACK_DELETE';

function requestPctTrackDelete(id) {
    return {
        type: REQUEST_PCT_TRACK_DELETE,
        id,
    };
}

export const RECEIVE_PCT_TRACK_DELETE = 'RECEIVE_PCT_TRACK_DELETE';

function receivePctTrackDelete(id) {
    return {
        type: RECEIVE_PCT_TRACK_DELETE,
        id,
    };
}

export const REQUEST_PCT_TRACK_DELETE_ERROR = 'REQUEST_PCT_TRACK_DELETE_ERROR';

function requestPctTrackDeleteError(id) {
    return {
        type: REQUEST_PCT_TRACK_DELETE_ERROR,
        id,
    };
}

export function addPctTrack(item) {
    return (dispatch, getState) => {
        const { domAttributes } = getState();
        if (domAttributes && Object.hasOwnProperty.call(domAttributes, 'year')) {
            item.year = domAttributes.year;
        }
        return addItem({
            item,
            dispatch,
            endpoint,
            startAction: requestPctTrackAdd,
            endAction: receivePctTrack,
            endActionTimeout: receivePctTrackTimeout,
            errorAction: addPctTrackError,
        });
    };
}

export function updatePctTrack(item, change) {
    const newItem = {
        ...item,
        ...change,
    };

    return dispatch => updateItem({
        item: newItem,
        change,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestPctTrackUpdate,
        endAction: receivePctTrack,
        endActionTimeout: receivePctTrackTimeout,
        errorAction: requestPctTrackError,
    });
}

export function fetchPctTracks() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `${state.domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestPctTracks,
            endAction: receivePctTracks,
            errorAction: requestPctTrackError,
        });
    };
}

export function deletePctTrack(item) {
    return dispatch => deleteItem({
        item,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestPctTrackDelete,
        endAction: receivePctTrackDelete,
        errorAction: requestPctTrackDeleteError,
    });
}
