import React from 'react';
import { connect } from 'react-redux';
import FormWrapper from '../components/Form-wrapper';
import { addPctTrack, updatePctTrack, deletePctTrack } from './actions';
import Form from './Form';
import { getPctTrackById } from '../redux/reducers';

const FormContainer = ({ params, year }) => (
    <FormWrapper
        returnTo="/pctTracks"
        addAction={addPctTrack}
        updateAction={updatePctTrack}
        deleteAction={deletePctTrack}
        getById={getPctTrackById}
        stateKey="pctTracks"
        year={year}
        formName="trackForm"
        params={params}
        formComponent={Form}
    />
);


function mapStateToProps(state, ownProps) {
    return {
        sessions: state.sessions,
        pcts: state.pcts,
        year: state.domAttributes.year,
    };
}

FormContainer.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    params: React.PropTypes.object,
    year: React.PropTypes.string,
};

const FormContainerConnected = connect(
    mapStateToProps
)(FormContainer);

export default FormContainerConnected;
