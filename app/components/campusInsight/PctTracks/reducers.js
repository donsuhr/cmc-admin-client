import { combineReducers } from 'redux';
import * as actions from './actions';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.REQUEST_PCT_TRACK_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.RECEIVE_PCT_TRACK:
            return {
                ...state,
                savingStatus: 'saved',
                ...action.item,
            };
        case actions.RECEIVE_PCT_TRACK_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.PCT_TRACK_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        case actions.RECEIVE_PCT_TRACKS: {
            return {
                ...defaultState,
                ...state,
                ...currentItem,
                savingStatus: '',
            };
        }
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_PCT_TRACKS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});
        case actions.REQUEST_PCT_TRACK_UPDATE:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_PCT_TRACK:
        case actions.RECEIVE_PCT_TRACK_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.RECEIVE_PCT_TRACK_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case actions.PCT_TRACK_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_PCT_TRACKS:
        case actions.REQUEST_PCT_TRACK_ADD:
        case actions.REQUEST_PCT_TRACK_UPDATE:
        case actions.REQUEST_PCT_TRACK_DELETE:
            return true;
        case actions.RECEIVE_PCT_TRACKS:
        case actions.PCT_TRACK_ADD_ERROR:
        case actions.PCT_TRACK_UPDATE_ERROR:
        case actions.RECEIVE_PCT_TRACK:
        case actions.RECEIVE_PCT_TRACK_DELETE:
        case actions.REQUEST_PCT_TRACK_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

const pctTracks = combineReducers({
    fetching,
    byId,
});

export default pctTracks;

export function getById(state, id) {
    return state.byId[id];
}
