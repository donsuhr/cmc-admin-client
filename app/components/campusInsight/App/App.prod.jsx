import React from 'react';
import Nav from './NavContainer';

const Root = props => (
    <div>
        <Nav />
        {props.children}
    </div >
);

Root.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.arrayOf(React.PropTypes.node),
        React.PropTypes.node,
    ]),
};

export default Root;
