import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';

const AppContainer = ({ store, routes, history }) => (
    <Provider store={store}>
        <Router history={history} routes={routes} />
    </Provider>
);

AppContainer.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    history: React.PropTypes.object.isRequired,
    routes: React.PropTypes.object.isRequired,
    store: React.PropTypes.object.isRequired,
};

export default AppContainer;
