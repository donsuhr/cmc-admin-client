import React from 'react';
import { Link } from 'react-router';

const Nav = ({ year }) => (
    <div className="admin-nav admin-nav--campusInsight">
        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/pcts"
        >
            <span className="menu-item-text">Pre Conference Training</span>
        </Link>
        |
        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/pctTracks"
        >
            <span className="menu-item-text">PCT Tracks</span>
        </Link>

        <br />

        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/sessions"
        >
            <span className="menu-item-text">Sessions</span>
        </Link>
        |
        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/sessionTracks"
        >
            <span className="menu-item-text">Session Tracks</span>
        </Link>

        <br />

        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/presenters"
        >
            <span className="menu-item-text">Presenters</span>
        </Link>
        |
        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/locations"
        >
            <span className="menu-item-text">Locations</span>
        </Link>
        |
        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/products"
        >
            <span className="menu-item-text">Products</span>
        </Link>

        <br />

        <Link
            className="menu-item"
            activeClassName="menu-item--active-file"
            to="/supportAnalysts"
        >
            <span className="menu-item-text">Support Analysts</span>
        </Link>
        {
            year === '2019' && [
                ' | ',
                <Link
                    key="1"
                    className="menu-item"
                    activeClassName="menu-item--active-file"
                    to="/consultationReminder"
                >
                    <span className="menu-item-text">Consultation Reminder Email</span>
                </Link>,
            ]
        }

    </div>
);

export default Nav;
