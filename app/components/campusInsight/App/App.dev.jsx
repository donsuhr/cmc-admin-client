import React from 'react';
import Nav from './NavContainer';
import DevTools from '../redux/DevTools'; // eslint-disable-line no-unused-vars

const Root = props => (
    <div>
        <Nav />
        {/* {(props && props.children) ? React.cloneElement(props.children, props) : null} */}
        {props.children}
        <DevTools />
    </div>
);

Root.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.arrayOf(React.PropTypes.node),
        React.PropTypes.node,
    ]),
};

export default Root;
