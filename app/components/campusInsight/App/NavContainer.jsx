import React from 'react';
import { connect } from 'react-redux';
import Nav from './Nav';

const NavContainer = props => <Nav year={props.year} />;

NavContainer.propTypes = {
    year: React.PropTypes.string,
};

function mapStateToProps(state, ownProps) {
    return {
        year: state.domAttributes.year,
    };
}

const NavConnected = connect(mapStateToProps)(NavContainer);

export default NavConnected;
