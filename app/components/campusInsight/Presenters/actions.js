import merge from 'lodash/merge';
import omitBy from 'lodash/omitBy';
import isNil from 'lodash/isNil';
import config from 'config';
import { fetchItems, updateItem, addItem, deleteItem } from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/presenters/`;

function normalizeResponse(response) {
    const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = nullRemoved.map(x => merge({}, {
        firstName: '',
        lastName: '',
        sessions: [],
        pct: [],
    }, x));
    return response;
}

export const REQUEST_PRESENTERS = 'REQUEST_PRESENTERS';

export function requestPresenters() {
    return {
        type: REQUEST_PRESENTERS,
    };
}

export const RECEIVE_PRESENTERS = 'RECEIVE_PRESENTERS';

export function receivePresenters(items) {
    return {
        type: RECEIVE_PRESENTERS,
        items: normalizeResponse(items),
    };
}

export const RECEIVE_PRESENTER = 'RECEIVE_PRESENTER';

export function receivePresenter(item) {
    return {
        type: RECEIVE_PRESENTER,
        item: normalizeResponse({ data: [item] })[0],
    };
}

export const RECEIVE_PRESENTER_TIMEOUT = 'RECEIVE_PRESENTER_TIMEOUT';

export function receivePresenterTimeout(item) {
    return {
        type: RECEIVE_PRESENTER_TIMEOUT,
        item,
    };
}

export const REQUEST_PRESENTER_UPDATE = 'REQUEST_PRESENTER_UPDATE';

function requestPresenterUpdate(id) {
    return {
        type: REQUEST_PRESENTER_UPDATE,
        id,
    };
}

export const PRESENTER_UPDATE_ERROR = 'PRESENTER_UPDATE_ERROR';

function requestPresenterError(error, id) {
    return {
        type: PRESENTER_UPDATE_ERROR,
        error,
        id,
    };
}

export const REQUEST_PRESENTER_ADD = 'REQUEST_PRESENTER_ADD';

function requestPresenterAdd() {
    return {
        type: REQUEST_PRESENTER_ADD,
    };
}

export const PRESENTER_ADD_ERROR = 'PRESENTER_ADD_ERROR';

function addPresenterError(error, data) {
    return {
        type: PRESENTER_ADD_ERROR,
        error,
        data,
    };
}

export const REQUEST_PRESENTER_DELETE = 'REQUEST_PRESENTER_DELETE';

function requestPresenterDelete(id) {
    return {
        type: REQUEST_PRESENTER_DELETE,
        id,
    };
}

export const RECEIVE_PRESENTER_DELETE = 'RECEIVE_PRESENTER_DELETE';

function receivePresenterDelete(id) {
    return {
        type: RECEIVE_PRESENTER_DELETE,
        id,
    };
}

export const REQUEST_PRESENTER_DELETE_ERROR = 'REQUEST_PRESENTER_DELETE_ERROR';

function requestPresenterDeleteError(id) {
    return {
        type: REQUEST_PRESENTER_DELETE_ERROR,
        id,
    };
}

export function addPresenter(item) {
    return (dispatch, getState) => {
        const { domAttributes } = getState();
        if (domAttributes && Object.hasOwnProperty.call(domAttributes, 'year')) {
            item.year = domAttributes.year;
        }
        return addItem({
            item,
            dispatch,
            endpoint,
            startAction: requestPresenterAdd,
            endAction: receivePresenter,
            endActionTimeout: receivePresenterTimeout,
            errorAction: addPresenterError,
        });
    };
}

export function updatePresenter(item, change) {
    const newItem = {
        ...item,
        ...change,
    };

    const dbSessionIds = item.sessionIds || [];
    const dbPctIds = item.pctIds || [];

    newItem.sessionsAdded = change.sessionIds ?
        change.sessionIds.filter(x => !dbSessionIds.includes(x)) : [];
    newItem.sessionsRemoved = change.sessionIds ?
        dbSessionIds.filter(x => !change.sessionIds.includes(x)) : [];
    newItem.pctAdded = change.pctIds ? change.pctIds.filter(x => !dbPctIds.includes(x)) : [];
    newItem.pctRemoved = change.pctIds ? dbPctIds.filter(x => !change.pctIds.includes(x)) : [];
    delete newItem.sessionIds;
    delete newItem.sessions;
    delete newItem.pctIds;
    delete newItem.pct;

    return dispatch => updateItem({
        item: newItem,
        change,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestPresenterUpdate,
        endAction: receivePresenter,
        endActionTimeout: receivePresenterTimeout,
        errorAction: requestPresenterError,
    });
}

export function fetchPresenters() {
    return (dispatch, getState) => {
        const year = Object.hasOwnProperty.call(getState().domAttributes, 'year') ? `${getState().domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestPresenters,
            endAction: receivePresenters,
            errorAction: requestPresenterError,
        });
    };
}

export function deletePresenter(item) {
    return dispatch => deleteItem({
        item,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestPresenterDelete,
        endAction: receivePresenterDelete,
        errorAction: requestPresenterDeleteError,
    });
}
