import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import { fetchPresenters, updatePresenter } from '../Presenters/actions';
import CheckBoxEditor from '../react-data-grid-editors/CheckBoxEditor';
import Grid from '../components/Grid';
import { getUnicodeStatus } from '../util/grid-util';
import { getById, populateCounts } from '../Presenters/reducers';

/* eslint-disable react/prop-types */
const gridColumns = gridContainerProps => {
    const ret = [
        {
            key: '_id',
            name: '',
            width: 35,
            formatter: props => (
                <div>
                    <Link to={`${props.dependentValues.gridProps.location.pathname}/edit/${props.value}`}>
                        Edit
                    </Link>
                </div>
            ),
            cellClass: 'presenter-grid__cell--actions',
            getRowMetaData() {
                return { gridProps: gridContainerProps };
            },
        },
        {
            key: 'firstName',
            name: 'First',
            editable: true,
            sortable: true,
            resizable: true,
            width: 100,
        },
        {
            key: 'lastName',
            name: 'Last',
            editable: true,
            sortable: true,
            resizable: true,
            width: 100,
        },
        {
            key: 'title',
            name: 'Title',
            editable: true,
            sortable: true,
            resizable: true,
            width: 100,
        },
        {
            key: 'institution',
            name: 'Institution',
            editable: true,
            sortable: true,
            resizable: true,
            width: 100,
        },
        {
            key: 'institutionHref',
            name: 'www',
            editable: true,
            sortable: true,
            resizable: true,
            width: 100,
        },
        {
            key: 'bio',
            name: 'Bio',
            resizable: true,
            width: 100,
        },
        {
            key: 'active',
            name: 'Act',
            editable: true,
            sortable: true,
            width: 35,
            formatter: ({ value }) => (<div>{value ? '✓' : '✖'}</div>),
            cellClass: 'presenter-grid__cell--active',
            editor: CheckBoxEditor,
        },
        {
            // key sessionIds will cause
            // Warning: Failed prop type: Invalid prop `value` supplied to `Cell`.
            // https://github.com/adazzle/react-data-grid/issues/706
            key: 'sessionsCount',
            name: 'Sess',
            sortable: true,
            width: 35,
            editable: false,
            resizable: false,
            cellClass: 'presenter-grid__cell--sessions',
        },
        {
            // pctIds will Warning: Failed prop type: Invalid prop `value` supplied to `Cell`.
            // https://github.com/adazzle/react-data-grid/issues/706
            key: 'pctsCount',
            name: 'Pct',
            sortable: true,
            width: 35,
            cellClass: 'presenter-grid__cell--pct',
        },
        {
            key: 'savingStatus',
            name: '↹',
            width: 35,
            formatter: ({ value }) => (<div>{getUnicodeStatus(value).savingStatus}</div>),
            cellClass: 'presenter-grid__cell--saving',
        },
        {
            key: 'idCopy',
            name: '_id',
            width: 45,
            formatter: props => (
                <div>
                    <Link to={`${props.dependentValues.gridProps.location.pathname}/edit/${props.value}`}>
                        Edit
                    </Link>
                </div>
            ),
            cellClass: 'presenter-grid__cell--actions',
            getRowMetaData() {
                return { gridProps: gridContainerProps };
            },
        },
    ];
    if (!gridContainerProps.year) {
        const index = ret.length - 2;
        return [
            ...ret.slice(0, index),
            {
                key: 'year',
                name: 'Year',
                editable: true,
                sortable: true,
                resizable: true,
                width: 50,
            },
            ...ret.slice(index),
        ];
    }
    return ret;
};

const GridContainer = props => (
    <Grid
        columns={gridColumns(props)}
        {...props}
    />
);

function mapStateToProps(state, ownProps) {
    return {
        dataById: populateCounts(state),
        fetching: state.presenters.fetching,
        getItemById: getById.bind(null, state),
        year: state.domAttributes.year,
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        update: bindActionCreators(updatePresenter, dispatch),
        fetch: bindActionCreators(fetchPresenters, dispatch),
    };
}

const GridConnected = connect(mapStateToProps, mapDispatchToProps)(GridContainer);

export default GridConnected;
