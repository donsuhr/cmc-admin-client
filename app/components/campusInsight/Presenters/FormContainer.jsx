import React from 'react';
import { connect } from 'react-redux';

import FormWrapper from '../components/Form-wrapper';
import { addPresenter, updatePresenter, deletePresenter } from './actions';
import Form from './Form';
import { getPresenterById } from '../redux/reducers';
import { fetchSessions } from '../Sessions/actions';
import { fetchPcts } from '../PreConfTraining/actions';

const FormContainer = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        params: React.PropTypes.object,
        sessions: React.PropTypes.object,
        pcts: React.PropTypes.object,
        fetchSessions: React.PropTypes.func.isRequired,
        fetchPcts: React.PropTypes.func.isRequired,
    },

    componentDidMount() {
        if (!this.props.sessions.fetching &&
            Object.keys(this.props.sessions.byId).length === 0) {
            this.props.fetchSessions();
        }
        if (!this.props.pcts.fetching &&
            Object.keys(this.props.pcts.byId).length === 0) {
            this.props.fetchPcts();
        }
    },

    render() {
        return (
            <FormWrapper
                returnTo="/presenters"
                addAction={addPresenter}
                updateAction={updatePresenter}
                deleteAction={deletePresenter}
                getById={getPresenterById}
                stateKey="presenters"
                formName="presenterForm"
                year={this.props.year}
                sessions={this.props.sessions}
                pcts={this.props.pcts}
                params={this.props.params}
                formComponent={Form}
            />
        );
    },
});

function mapStateToProps(state, ownProps) {
    return {
        sessions: state.sessions,
        pcts: state.pcts,
        year: state.domAttributes.year,
    };
}

const FormContainerConnected = connect(
    mapStateToProps,
    { fetchSessions, fetchPcts, deletePresenter }
)(FormContainer);

export default FormContainerConnected;
