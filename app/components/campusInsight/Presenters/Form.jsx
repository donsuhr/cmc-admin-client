import React from 'react';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import { renderField, renderTextArea, renderMultiSelect, normalizeToArray } from '../util/form-util';

const validate = (values) => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = 'Required';
    }
    if (!values.lastName) {
        errors.lastName = 'Required';
    }
    return errors;
};

const Form = ({
    error,
    handleSubmit,
    pristine,
    submitting,
    sessions,
    pcts,
    year,
    handleDelete,
    isEdit,
    ...props
}) => {
    const sessionOptions = Object.keys(sessions.byId).map(x => ({
        value: sessions.byId[x]._id,
        label: sessions.byId[x].title,
    }));
    const pctsOptions = Object.keys(pcts.byId).map(x => ({
        value: pcts.byId[x]._id,
        label: pcts.byId[x].title,
    }));
    return (
        <div className="">
            <form onSubmit={handleSubmit} className="cmc-form">
                <dl className="item-details-list">
                    <Field
                        name="firstName"
                        label="First Name"
                        component={renderField}
                        type="text"
                    />
                    <Field
                        name="lastName"
                        label="Last Name"
                        component={renderField}
                        type="text"
                    />
                    <Field
                        name="title"
                        label="Title"
                        component={renderField}
                        type="text"
                    />
                    <Field
                        name="institution"
                        label="Institution"
                        component={renderField}
                        type="text"
                    />
                    <Field
                        name="institutionHref"
                        label="Institution Web Site"
                        component={renderField}
                        type="text"
                    />
                    <Field
                        name="bio"
                        label="Bio"
                        component={renderTextArea}
                    />
                    <Field
                        name="active"
                        label="Active"
                        component={renderField}
                        type="checkbox"
                    />
                    {!year &&
                    <Field
                        name="year"
                        label="Year"
                        component={renderField}
                        type="text"
                    />
                    }
                    {isEdit ?
                        <div>
                            <Field
                                multi
                                name="sessionIds"
                                label="Sessions"
                                normalize={normalizeToArray}
                                component={renderMultiSelect}
                                options={sessionOptions}
                                isLoading={sessions.fetching}
                            />
                            <Field
                                multi
                                name="pctIds"
                                label="Pre Conf Training"
                                normalize={normalizeToArray}
                                component={renderMultiSelect}
                                options={pctsOptions}
                                isLoading={pcts.fetching}
                            />
                        </div>
                        :
                        <dt>save to add sessions and/or pre conf training</dt>
                    }
                    {
                        props.item && <div>
                            <dt>Created At</dt>
                            <dd>{moment(props.item.createdAt).format('YYYY-MM-DD hh:mm A')}</dd>
                            <dt>Updated At</dt>
                            <dd>{moment(props.item.updatedAt).format('YYYY-MM-DD hh:mm A')}</dd>
                        </div>
                    }
                </dl>
                <button
                    className="item-details__save-button"
                    type="submit"
                    disabled={pristine || submitting}
                >
                    {isEdit ? 'Save' : 'Create'}
                </button>
                <br />
                {
                    props.item && <button
                        className="item-details__delete-button"
                        onClick={handleDelete}
                        type="button"
                    >
                        Delete
                    </button>
                }
                <p>{error && <strong>{error}</strong>}</p>
            </form>
        </div>
    );
};

Form.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    item: React.PropTypes.object,
    isEdit: React.PropTypes.bool.isRequired,
    error: React.PropTypes.bool,
    pristine: React.PropTypes.bool,
    submitting: React.PropTypes.bool,
    handleSubmit: React.PropTypes.func.isRequired,
    handleDelete: React.PropTypes.func.isRequired,
    sessions: React.PropTypes.object,
    pcts: React.PropTypes.object,
    year: React.PropTypes.string,
};

const ContactForm = reduxForm({
    form: 'presenterForm',
    validate,
})(Form);

export default ContactForm;
