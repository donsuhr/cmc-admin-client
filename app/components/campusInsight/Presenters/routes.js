/* eslint-disable no-shadow */
module.exports = {
    path: 'presenters',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('../Presenters/GridContainer').default);
        });
    },

    getChildRoutes(partialNextState, cb) {
        require.ensure([], (require) => {
            cb(null, [
                {
                    path: 'add',
                    getComponent(nextState, cb) {
                        require.ensure([], (require) => {
                            cb(null, require('../Presenters/FormContainer').default);
                        });
                    },
                },
                {
                    path: 'edit/:id',
                    getComponent(nextState, cb) {
                        require.ensure([], (require) => {
                            cb(null, require('../Presenters/FormContainer').default);
                        });
                    },
                },
            ]);
        });
    },
};

