import { combineReducers } from 'redux';
import * as actions from './actions';
import { getPresenterSessions } from '../Sessions/reducers';
import { getPresenterPcts } from '../PreConfTraining/reducers';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.REQUEST_PRESENTER_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.RECEIVE_PRESENTER:
            return {
                ...state,
                savingStatus: 'saved',
                idCopy: action.item._id,
                ...action.item,
            };
        case actions.RECEIVE_PRESENTER_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.PRESENTER_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        case actions.RECEIVE_PRESENTERS: {
            return {
                ...defaultState,
                ...state,
                ...currentItem,
                idCopy: currentItem._id,
                savingStatus: '',
            };
        }
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_PRESENTERS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});
        case actions.REQUEST_PRESENTER_UPDATE:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_PRESENTER:
        case actions.RECEIVE_PRESENTER_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.RECEIVE_PRESENTER_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case actions.PRESENTER_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_PRESENTERS:
        case actions.REQUEST_PRESENTER_ADD:
        case actions.REQUEST_PRESENTER_UPDATE:
        case actions.REQUEST_PRESENTER_DELETE:
            return true;
        case actions.RECEIVE_PRESENTERS:
        case actions.PRESENTER_ADD_ERROR:
        case actions.PRESENTER_UPDATE_ERROR:
        case actions.RECEIVE_PRESENTER:
        case actions.RECEIVE_PRESENTER_DELETE:
        case actions.REQUEST_PRESENTER_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

const presenters = combineReducers({
    fetching,
    byId,
});

export default presenters;

const addCountsToItem = (state, currentItem) => {
    const sessionIds =
        Object.keys(state.sessions.byId).length > 0
            ? getPresenterSessions(state.sessions, currentItem._id)
            : currentItem.sessions.map(x => x._id);
    const pctIds =
        Object.keys(state.pcts.byId).length > 0
            ? getPresenterPcts(state.pcts, currentItem._id)
            : currentItem.pct.map(x => x._id);
    return {
        ...currentItem,
        sessionIds,
        pctIds,
        sessionsCount: sessionIds.length,
        pctsCount: pctIds.length,
    };
};

export function getById(state, id) {
    const currentItem =
        state.presenters && state.presenters.byId && state.presenters.byId[id];
    if (typeof currentItem === 'undefined') {
        return currentItem;
    }
    return addCountsToItem(state, currentItem);
}

export function populateCounts(state) {
    return Object.keys(state.presenters.byId)
        .map(key => addCountsToItem(state, state.presenters.byId[key]))
        .reduce((acc, x) => {
            acc[x._id] = x;
            return acc;
        }, {});
}
