import React from 'react';
import ReactDOM from 'react-dom';
import { hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import 'react-select/dist/react-select.css';
import * as actions from './redux/actions';

import configureStore from './redux/configureStore';
import createRoutes from './routes';
import AppContainer from './App/AppContainer';

import './styles.scss';

let mountNode;
let domAttributes;

const store = configureStore();
const routes = createRoutes();
const history = syncHistoryWithStore(hashHistory, store);

let render = () => {
    ReactDOM.render(
        React.createElement(AppContainer, {
            domAttributes,
            store,
            history,
            routes,
        }),
        mountNode
    );
};

export default {
    config(attrs, el) {
        store.dispatch(actions.initDomAttributes(attrs));
        mountNode = el;
        domAttributes = attrs;
        render();
    },
};

// ========================================================
// Developer Tools Setup
// ========================================================
if (process.env.NODE_ENV) {
    if (window.devToolsExtension) {
        //   window.devToolsExtension.open();
    }
}

// This code is excluded from production bundle
if (process.env.NODE_ENV) {
    if (module.hot) {
        // Development render functions
        const renderApp = render;
        const renderError = (error) => {
            // eslint-disable-next-line import/no-extraneous-dependencies, global-require
            const RedBox = require('redbox-react').default;

            ReactDOM.render(React.createElement(RedBox, { error }), mountNode);
        };

        // Wrap render in try/catch
        render = () => {
            try {
                renderApp();
            } catch (error) {
                renderError(error);
            }
        };

        // Setup hot module replacement
        module.hot.accept('./routes', () => {
            setTimeout(() => {
                ReactDOM.unmountComponentAtNode(mountNode);
                render();
            });
        });
    }
}
