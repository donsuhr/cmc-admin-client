const moment = require('moment-timezone/moment-timezone');
const tzData = require('./custom-moment-tz.json');

moment.tz.load(tzData);
module.exports = moment;
