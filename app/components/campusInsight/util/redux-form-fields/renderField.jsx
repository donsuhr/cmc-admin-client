/* eslint-disable react/forbid-prop-types */

import React from 'react';

const renderField = React.createClass({
    propTypes: {
        input: React.PropTypes.object,
        meta: React.PropTypes.object,
        label: React.PropTypes.string.isRequired,
        type: React.PropTypes.string.isRequired,
        inputRef: React.PropTypes.func,
    },

    render() {
        const { input, label, type, meta: { touched, error }, inputRef } = this.props;
        return (
            <div>
                <dt><label htmlFor={input.name}>{label}</label></dt>
                <dd>
                    <input
                        {...input}
                        ref={inputRef}
                        placeholder={label}
                        type={type}
                        id={input.name}
                    />
                    {touched && error && <span>{error}</span>}
                </dd>
            </div>
        );
    },
});

export default renderField;
