import React from 'react';
import ReactDataGrid from 'react-data-grid';

export const getUnicodeStatus = (value = '') => {
    if (value.toLowerCase().indexOf('error') !== -1) {
        return {
            status: '⚠',
            rowClassName: 'presenter-grid__row--error',
        };
    }
    if (value.toLowerCase() === 'saved') {
        return {
            status: '✓',
            rowClassName: 'presenter-grid__row--saved',
        };
    }
    if (value.toLowerCase() === 'saving') {
        return {
            status: '\u231B',
            rowClassName: 'presenter-grid__row--busy',
        };
    }
    return {
        status: '',
        rowClassName: 'presenter-grid__row',
    };
};

export const EmptyRowsView = () => (
    <div className="react-grid-Canvas">Nothing to show</div>
);

export const RowRenderer = React.createClass({
    /* eslint-disable react/no-string-refs */
    propTypes: {
        row: React.PropTypes.shape({
            savingStatus: React.PropTypes.string,
        }),
    },
    setScrollLeft(scrollBy) {
        // if you want freeze columns to work, you need to make sure you implement
        // this as a pass through
        this.refs.row.setScrollLeft(scrollBy);
    },
    getRowClassName() {
        return getUnicodeStatus(this.props.row.savingStatus).rowClassName;
    },
    render() {
        return (
            <div className={this.getRowClassName()}>
                <ReactDataGrid.Row ref="row" {...this.props} />
            </div>
        );
    },
});
