/* eslint-disable react/forbid-prop-types */

import React from 'react';
import Select from 'react-select';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import classNames from 'classnames';
import moment from '../util/load-moment-tz';

export { default as renderField } from './redux-form-fields/renderField';

export const renderTextArea = ({ input, label, meta: { touched, error } }) => (
    <div>
        <dt><label htmlFor={input.name}>{label}</label></dt>
        <dd>
            <textarea
                {...input}
                placeholder={label}
                id={input.name}
                value={input.value}
            />
            {touched && error && <span>{error}</span>}
        </dd>
    </div>
);

renderTextArea.propTypes = {
    input: React.PropTypes.object,
    meta: React.PropTypes.object,
    label: React.PropTypes.string.isRequired,
};

export const renderDateTimeLocal = React.createClass({
    propTypes: {
        item: React.PropTypes.object,
        input: React.PropTypes.object,
        meta: React.PropTypes.object,
        label: React.PropTypes.string.isRequired,
        onChangeAction: React.PropTypes.func,
    },
    getInitialState() {
        return {
            warningOn: false,
        };
    },
    componentWillReceiveProps(nextProps) {
        const propname = this.props && this.props.input && this.props.input.name;
        const hasNew = nextProps &&
            nextProps.item &&
            {}.hasOwnProperty.call(nextProps.item, propname);
        if (this.props.item === undefined && hasNew) {
            const parts = moment(nextProps.input.value).tz('America/New_York').format('YYYY-MM-DD hh:mm A');
            this.dateInput.value = moment(parts, 'YYYY-MM-DD hh:mm A').format('YYYY-MM-DDTHH:mm');
        }
    },
    onFpChange(event, valueArg, keepWarning) {
        const value = (event && event.target && event.target.value) ? event.target.value : valueArg;
        const parts = moment(value).format('YYYY-MM-DD hh:mm A');
        const asEST = moment.tz(parts, 'YYYY-MM-DD hh:mm A', 'America/New_York').toDate();
        this.props.input.onChange(asEST);
        this.props.input.onBlur(asEST);
        this.setState({ warningOn: true });
        if (keepWarning !== true) {
            this.toggleWarning(false);
        }
        if (typeof this.props.onChangeAction !== 'undefined') {
            this.props.onChangeAction(asEST, this.props.input);
        }
    },
    updateValue(value) {
        const parts = moment(value).tz('America/New_York').format('YYYY-MM-DD hh:mm A');
        const asLocal = moment(parts, 'YYYY-MM-DD hh:mm A').toDate();
        this.dateInput.value = moment(parts, 'YYYY-MM-DD hh:mm A').format('YYYY-MM-DDTHH:mm');
        this.onFpChange(null, asLocal, true);
    },
    toggleWarning(force) {
        let on = !this.state.warningOn;
        if (typeof force !== 'undefined') {
            on = !!force;
        }
        this.setState({ warningOn: on });
    },
    render() {
        const { input, label, meta: { error }, year } = this.props;
        const defaultYear = year || new Date().getFullYear();
        let defaultDate = `${defaultYear}-04-24T09:00`;
        if (input.value) {
            const parts = moment(input.value).tz('America/New_York').format('YYYY-MM-DD hh:mm A');
            defaultDate = moment(parts, 'YYYY-MM-DD hh:mm A').format('YYYY-MM-DDTHH:mm');
        }
        return (
            <div>
                <dt><label htmlFor={input.name}>{label}</label></dt>
                <dd>
                    <input
                        type="hidden"
                        id={input.name}
                        value={input.value}
                        readOnly
                    />
                    <input
                        ref={(x) => {
                            this.dateInput = x;
                        }}
                        type="datetime-local"
                        defaultValue={defaultDate}
                        onChange={this.onFpChange}
                        onBlur={this.onFpChange}
                        className={classNames({
                            warning: this.state.warningOn,
                            error,
                        })}
                    />

                    {error && <span>{error}</span>}
                </dd>
            </div>
        );
    },
});

export const renderMultiSelect = ({
    input,
    label,
    options,
    isLoading,
    multi = false,
    meta: { touched, error },
    ...props
}) => {
    let value = input.value || '';
    if (isArray(value)) {
        value = value.map((x) => {
            if (isObject(x)) {
                return x._id;
            }
            return x;
        });
    }
    return (
        <div>
            <dt><label htmlFor={input.name}>{label}</label></dt>
            <dd className="item-details-list-item--has-dropdown">
                <Select
                    {...props}
                    multi={multi}
                    simpleValue
                    value={value}
                    placeholder={label}
                    options={options}
                    onBlur={() => input.onBlur(input.value)}
                    onChange={input.onChange}
                    isLoading={isLoading}
                />
                {touched && error && <span>{error}</span>}
            </dd>
        </div>
    );
};

renderMultiSelect.propTypes = {
    input: React.PropTypes.object,
    meta: React.PropTypes.object,
    label: React.PropTypes.string.isRequired,
    isLoading: React.PropTypes.bool,
    multi: React.PropTypes.bool,
    options: React.PropTypes.arrayOf(React.PropTypes.shape({
        // eslint-disable-next-line react/no-unused-prop-types
        value: React.PropTypes.string.isRequired,
        label: React.PropTypes.string.isRequired,
    })).isRequired,
};

export const normalizeToArray = (value, previousValue) => {
    if (!value) {
        return [];
    }
    if (isArray(value)) {
        return value;
    }
    return value.split(',');
};
