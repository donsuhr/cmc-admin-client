import { SubmissionError } from 'redux-form';
import isEmpty from 'lodash/isEmpty';
import { checkStatus, parseJSON } from '../../fetchJsonHelpers';
import { auth } from '../../preconfigured-auth';

export const getSubmissionError = errResponse => new Promise((resolve, reject) => {
    try {
        errResponse.response.json()
            .then((response) => {
                if (Object.hasOwnProperty.call(response, 'error')) {
                    if (!isEmpty(response.error)) {
                        // eslint-disable-next-line no-underscore-dangle
                        response._error = response.error;
                    } else {
                        // eslint-disable-next-line no-underscore-dangle
                        response._error = errResponse.message;
                    }
                }
                resolve(new SubmissionError(response));
            });
    } catch (e) {
        // eslint-disable-next-line no-underscore-dangle
        resolve(new SubmissionError({ _error: errResponse.message }));
    }
});

export const wait = (delay) => {
    console.log('waiting', delay); // eslint-disable-line no-console
    return new Promise((resolve) => {
        setTimeout(() => {
            console.log('calling resolve'); // eslint-disable-line no-console
            resolve();
        }, 2000);
    });
};

export function fetchItems({
    dispatch, startAction, endpoint, endAction, errorAction,
}) {
    dispatch(startAction());
    fetch(endpoint, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            authorization: `Bearer ${auth.getToken()}`,
        },
    })
        .then(checkStatus)
        .then(parseJSON)
        .then(result => dispatch(endAction(result)))
        .catch((err) => {
            dispatch(errorAction(err));
        });
}

export function updateItem({
    item,
    change,
    dispatch,
    startAction,
    endpoint,
    endAction,
    endActionTimeout,
    errorAction,
}) {
    const newItem = {
        ...item,
        ...change,
    };
    dispatch(startAction(item._id));
    return new Promise((resolve, reject) => {
        fetch(endpoint, {
            method: 'PATCH',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                authorization: `Bearer ${auth.getToken()}`,
            },
            body: JSON.stringify(newItem),
        })
            .then(checkStatus)
            .then(parseJSON)
            .then((result) => {
                dispatch(endAction(result));
                setTimeout(() => {
                    dispatch(endActionTimeout(result));
                }, 1000);
                resolve(result);
            })
            .catch((err) => {
                dispatch(errorAction(err, item._id));
                getSubmissionError(err)
                    .then((submitError) => {
                        reject(submitError);
                    });
            });
    });
}

export function addItem({
    item,
    dispatch,
    startAction,
    endpoint,
    endAction,
    endActionTimeout,
    errorAction,
}) {
    dispatch(startAction());
    return new Promise((resolve, reject) => {
        fetch(endpoint, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                authorization: `Bearer ${auth.getToken()}`,
            },
            body: JSON.stringify(item),
        })
            .then(checkStatus)
            .then(parseJSON)
            .then((result) => {
                dispatch(endAction(result));
                setTimeout(() => {
                    dispatch(endActionTimeout(result));
                }, 1000);
                resolve(result);
            })
            .catch((err) => {
                dispatch(errorAction(err));
                getSubmissionError(err)
                    .then((submitError) => {
                        reject(submitError);
                    });
            });
    });
}

export function deleteItem({
    item,
    dispatch,
    startAction,
    endpoint,
    endAction,
    errorAction,
}) {
    dispatch(startAction(item._id));
    return new Promise((resolve, reject) => {
        fetch(endpoint, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                authorization: `Bearer ${auth.getToken()}`,
            },
        })
            .then(checkStatus)
            .then((result) => {
                if (result.status !== 204 && result.status !== 200 && result.status !== 202) {
                    throw new Error('Error deleting item');
                }
            })
            .then((result) => {
                dispatch(endAction(item._id));
                resolve(result);
            })
            .catch((err) => {
                dispatch(errorAction(err, item._id));
                reject(err);
            });
    });
}
