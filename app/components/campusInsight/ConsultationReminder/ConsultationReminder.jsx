import React from 'react';

const Row = React.createClass({
    onDeleteClicked(event) {
        event.preventDefault();
        const { deleteReminderEmail, data } = this.props;
        deleteReminderEmail(data);
    },
    onSendClicked(event) {
        event.preventDefault();
        const { sendReminderEmail, data } = this.props;
        sendReminderEmail(data);
    },
    render() {
        const {
            data: {
                userFirstName,
                userLastName,
                mode,
                analystLoadState,
                profileLoadState,
                reminderLoadState,
                userEmail,
            },
            availabilityId,
        } = this.props;
        return (
            <tr>
                <td>{availabilityId}</td>
                <td>
                    {userFirstName} {userLastName}
                </td>
                <td>{userEmail}</td>
                <td>{mode}</td>
                <td>{analystLoadState}</td>
                <td>{profileLoadState}</td>
                <td>{reminderLoadState}</td>
                <td>
                    <button type="button" onClick={this.onSendClicked}>
                        Send
                    </button>
                    <button type="button" onClick={this.onDeleteClicked}>
                        Delete
                    </button>
                </td>
            </tr>
        );
    },
});

const ConsultationReminder = ({
    data,
    sendReminderEmail,
    deleteReminderEmail,
    ready,
    onSendAllReminderEmailsClicked
}) => (
    <div>
        <table className="consultation-reminder-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>email</th>
                    <th>mode</th>
                    <th>analystLoad</th>
                    <th>profileLoad</th>
                    <th>reminderLoad</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
                {Object.keys(data).map((availabilityId, i) => (
                    <Row
                        key={availabilityId}
                        availabilityId={availabilityId}
                        data={data[availabilityId]}
                        sendReminderEmail={sendReminderEmail}
                        deleteReminderEmail={deleteReminderEmail}
                    />
                ))}
            </tbody>
        </table>
        {ready && <button type="button" onClick={onSendAllReminderEmailsClicked}>Send All</button>}
    </div>
);

ConsultationReminder.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    data: React.PropTypes.object.isRequired,
    deleteReminderEmail: React.PropTypes.func,
    sendReminderEmail: React.PropTypes.func,
};

export default ConsultationReminder;
