/* eslint-disable no-shadow */
module.exports = {
    path: 'consultationReminder',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./ConsultationReminderContainer').default);
        });
    },
};

