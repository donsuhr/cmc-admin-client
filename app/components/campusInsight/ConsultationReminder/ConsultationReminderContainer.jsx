import React from 'react';
import { connect } from 'react-redux';
import isEqual from 'lodash/isEqual';
import {
    listenToAuth,
    listenToUserRoot,
} from '../../firebase-login/auth.actions';
import {
    listenToScheduleSupport,
    listenToUsedAvailability,
} from '../../firebase-login/schedule.support.actions';
import { getSupportConsultationData } from '../redux/reducers';
import FirebaseLogin from '../../firebase-login';
import Loading from '../../client-side-js-error-list/components/loading';
import { fetchUserProfile } from '../../firebase-login/userProfiles.actions';
import { fetchAnalysts } from '../SupportAnalysts/actions';
import ConsultationReminder from './ConsultationReminder';
import {
    fetchData,
    pollData,
    sendReminderEmail,
    deleteReminderEmail,
} from './actions';

const REMINDER_LOAD_STATE_NO_DATA = 'loaded -- no data';

const container = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        listenToAuthConnect: React.PropTypes.func,
        listenToUserRootConnect: React.PropTypes.func,
        listenToUsedAvailabilityConnect: React.PropTypes.func,
        listenToScheduleSupportConnect: React.PropTypes.func,
    },

    getInitialState() {
        return {
            sendingAll: false,
        };
    },

    componentDidMount() {
        const {
            analystsConnect,
            fetchAnalystsConnect,
            reminderEmail,
            fetchReminderStatusConnect,
        } = this.props;
        this.loadListeners();
        if (
            analystsConnect.hasEverLoaded === false &&
            analystsConnect.fetching === false
        ) {
            fetchAnalystsConnect();
        }
        if (
            reminderEmail.hasEverLoaded === false &&
            reminderEmail.fetching === false
        ) {
            fetchReminderStatusConnect();
        }
    },

    componentWillReceiveProps(nextProps) {
        const {
            usedAvailabilityIds,
            user,
            pollReminderStatusConnect,
        } = this.props;

        if (nextProps.user.isAuth && !user.isAuth) {
            this.loadListeners();
        }

        if (
            !isEqual(
                Object.keys(nextProps.usedAvailabilityIds),
                Object.keys(usedAvailabilityIds)
            )
        ) {
            this.usedAvailabilityWillChange({
                nextAvailabilityIds: nextProps.usedAvailabilityIds,
            });
        }
        Object.values(nextProps.data).forEach((x) => {
            if (x.reminderLoadState.toLowerCase().includes('queued')) {
                const {
                    polling,
                    pollCount,
                    data: { _id },
                } = nextProps.reminderEmail.byEmail[x.userEmail];
                if (!polling && pollCount < 4) {
                    pollReminderStatusConnect(_id);
                }
            }
        });
    },

    usedAvailabilityWillChange({ nextAvailabilityIds }) {
        const { fetchUserProfileConnect, userProfiles } = this.props;
        Object.keys(nextAvailabilityIds).forEach((id) => {
            const { userId } = nextAvailabilityIds[id];
            if (
                userId !== 'easybook' &&
                typeof userProfiles.byId[userId] === 'undefined'
            ) {
                fetchUserProfileConnect(nextAvailabilityIds[id].userId);
            }
        });
    },

    loadListeners() {
        const {
            listenToAuthConnect,
            listenToUserRootConnect,
            listenToScheduleSupportConnect,
            listenToUsedAvailabilityConnect,
        } = this.props;
        listenToAuthConnect().then((user) => {
            listenToUserRootConnect(user.uid);
            listenToScheduleSupportConnect();
            listenToUsedAvailabilityConnect();
        });
    },

    render() {
        const {
            profilesLoaded,
            analystsConnect,
            user,
            data,
            deleteReminderEmailConnect,
            sendReminderEmailConnect,
            reminderEmail,
            schedule,
        } = this.props;
        const { sendingAll } = this.state;

        const loadingProfiles = user.isAuth && !profilesLoaded;
        const loadingAnalysts =
            user.isAuth &&
            (!analystsConnect.hasEverLoaded || analystsConnect.fetching);
        const loadingReminders =
            !reminderEmail.hasEverLoaded || reminderEmail.fetching;
        const loadingSchedule = !schedule.hasEverLoaded || schedule.fetching;
        const loadingAvailability = !schedule.hasEverLoadedUsedAvailability;

        const ready =
            !loadingAnalysts &&
            !loadingReminders &&
            !loadingProfiles &&
            !loadingSchedule &&
            !loadingAvailability;

        return (
            <div>
                <FirebaseLogin />{' '}
                {loadingAnalysts && <Loading>Loading Analysts...</Loading>}{' '}
                {!loadingAnalysts &&
                    loadingReminders && (
                    <Loading>Loading Reminders...</Loading>
                )}{' '}
                {!loadingAnalysts &&
                    !loadingReminders &&
                    loadingProfiles && (
                    <Loading>Loading Profiles...</Loading>
                )}{' '}
                {!loadingAnalysts &&
                    !loadingReminders &&
                    !loadingProfiles &&
                    !ready && <Loading>Loading...</Loading>}
                {sendingAll && <Loading>Sending Reminders...</Loading>}
                <ConsultationReminder
                    data={data}
                    ready={ready}
                    sendReminderEmail={sendReminderEmailConnect}
                    deleteReminderEmail={deleteReminderEmailConnect}
                    onSendAllReminderEmailsClicked={
                        this.onSendAllReminderEmailsClicked
                    }
                />
            </div>
        );
    },

    onSendAllReminderEmailsClicked(event) {
        event.preventDefault();
        const { data, sendReminderEmailConnect } = this.props;
        const needsReminder = Object.values(data).filter(
            item => item.reminderLoadState === REMINDER_LOAD_STATE_NO_DATA
        );

        let timeout = 500;
        if (needsReminder.length) {
            this.setState({ sendingAll: true });
            needsReminder.forEach((item, index) => {
                setTimeout(() => {
                    if (index === needsReminder.length - 1) {
                        this.setState({ sendingAll: false });
                    }
                    sendReminderEmailConnect(item);
                }, timeout);
                timeout += 500;
            });
        }
    },
});

function mapStateToProps(state, ownProps) {
    const usedAvailabilityIdKeys = Object.keys(
        state.schedule.usedAvailabilityId
    );

    const profilesLoaded =
        usedAvailabilityIdKeys.length > 0 &&
        usedAvailabilityIdKeys.every((x) => {
            const { userId } = state.schedule.usedAvailabilityId[x];
            const isEasyBook = userId === 'easybook';
            const hasProfileKey = Object.hasOwnProperty.call(
                state.userProfiles.byId,
                userId
            );
            const isLoading =
                hasProfileKey && !state.userProfiles.byId[userId].loading;
            return isEasyBook || isLoading;
        });

    const data = getSupportConsultationData(state);
    const filteredData = Object.keys(data).reduce((acc, availabilityID) => {
        const target = data[availabilityID];
        if (target.mode !== 'easybook') {
            acc[availabilityID] = { ...target };
        }
        return acc;
    }, {});

    const dataWithReminderState = Object.keys(filteredData).reduce(
        (acc, availabilityID) => {
            const target = data[availabilityID];

            let reminderLoadState = '';
            let emailId = null;
            if (!state.reminderEmail.hasEverLoaded) {
                if (state.reminderEmail.fetching) {
                    reminderLoadState = 'loading';
                }
                reminderLoadState = 'not requested';
            } else if (
                Object.hasOwnProperty.call(
                    state.reminderEmail.byEmail,
                    target.userEmail
                )
            ) {
                if (state.reminderEmail.byEmail[target.userEmail].fetching) {
                    reminderLoadState = 'loading';
                } else if (
                    state.reminderEmail.byEmail[target.userEmail].fetchError !==
                    null
                ) {
                    reminderLoadState = 'error';
                } else {
                    const { cxEmail } = state.reminderEmail.byEmail[
                        target.userEmail
                    ].data;
                    const lastEvent = cxEmail[cxEmail.length - 1].event;
                    emailId = lastEvent._id;
                    reminderLoadState = lastEvent;
                }
            } else {
                reminderLoadState = REMINDER_LOAD_STATE_NO_DATA;
            }

            acc[availabilityID] = {
                ...target,
                reminderLoadState,
                emailId,
            };

            return acc;
        },
        {}
    );

    return {
        usedAvailabilityIds: state.schedule.usedAvailabilityId,
        schedule: state.schedule,
        user: state.user,
        year: ownProps.year,
        analystsConnect: state.analysts,
        data: dataWithReminderState,
        profilesLoaded,
        userProfiles: state.userProfiles,
        easyBookConnect: state.easybook,
        reminderEmail: state.reminderEmail,
    };
}

const connected = connect(
    mapStateToProps,
    {
        listenToScheduleSupportConnect: listenToScheduleSupport,
        listenToUsedAvailabilityConnect: listenToUsedAvailability,
        listenToAuthConnect: listenToAuth,
        listenToUserRootConnect: listenToUserRoot,
        fetchUserProfileConnect: fetchUserProfile,
        fetchAnalystsConnect: fetchAnalysts,
        fetchReminderStatusConnect: fetchData,
        pollReminderStatusConnect: pollData,
        sendReminderEmailConnect: sendReminderEmail,
        deleteReminderEmailConnect: deleteReminderEmail,
    }
)(container);

export default connected;
