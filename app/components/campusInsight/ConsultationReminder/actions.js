import config from 'config';
import {
    fetchItems,
    addItem,
    deleteItem,
} from '../util/fetch-util';

const endpoint = `${
    config.domains.api
}/campusInsight/support-consultation-reminder/`;

export const REQUEST_REMINDER_EMAILS = 'REQUEST_REMINDER_EMAILS';

export function requestData() {
    return {
        type: REQUEST_REMINDER_EMAILS,
    };
}

export const POLL_REMINDER_EMAILS = 'POLL_REMINDER_EMAILS';

export function requestPollData(id) {
    return {
        type: POLL_REMINDER_EMAILS,
        id,
    };
}

export const POLL_REMINDER_EMAILS_PAUSE = 'POLL_REMINDER_EMAILS_PAUSE';

export function requestPollDataPause(id) {
    return {
        type: POLL_REMINDER_EMAILS_PAUSE,
        id,
    };
}

export const RECEIVE_REMINDER_EMAILS = 'RECEIVE_REMINDER_EMAILS';

export function receiveData(items) {
    return {
        type: RECEIVE_REMINDER_EMAILS,
        items: items.data,
    };
}

export const RECEIVE_UPDATED_REMINDER_EMAILS =
    'RECEIVE_UPDATED_REMINDER_EMAILS';

export function receiveUpdatedData(items) {
    return {
        type: RECEIVE_UPDATED_REMINDER_EMAILS,
        items: items.data,
    };
}

export const REQUEST_REMINDER_EMAILS_ERROR = 'REQUEST_REMINDER_EMAILS_ERROR';

function requestDataError(error, id) {
    return {
        type: REQUEST_REMINDER_EMAILS_ERROR,
        error,
        id,
    };
}

export const REQUEST_REMINDER_EMAIL_ADD = 'REQUEST_REMINDER_EMAIL_ADD';

function requestItemAdd(email) {
    return {
        type: REQUEST_REMINDER_EMAIL_ADD,
        email,
    };
}

export const RECEIVE_NEW_REMINDER_EMAIL = 'RECEIVE_NEW_REMINDER_EMAIL';

export function receiveNewItem(item) {
    return {
        type: RECEIVE_NEW_REMINDER_EMAIL,
        item,
        email: item.userEmail,
    };
}

export const REQUEST_REMINDER_EMAIL_ADD_TIMEOUT =
    'REQUEST_REMINDER_EMAIL_ADD_TIMEOUT';

export function requestItemAddTimeout(item) {
    return {
        type: REQUEST_REMINDER_EMAIL_ADD_TIMEOUT,
        item,
    };
}

export const ADD_REMINDER_EMAIL_ERROR = 'ADD_REMINDER_EMAIL_ERROR';

function addItemError(email, error) {
    return {
        type: ADD_REMINDER_EMAIL_ERROR,
        error,
        email,
    };
}

export const REQUEST_REMINDER_EMAIL_DELETE = 'REQUEST_REMINDER_EMAIL_DELETE';

function requestItemDelete(id) {
    return {
        type: REQUEST_REMINDER_EMAIL_DELETE,
        id,
    };
}

export const RECEIVE_REMINDER_EMAIL_DELETE = 'RECEIVE_REMINDER_EMAIL_DELETE';

function receiveItemDelete(id) {
    return {
        type: RECEIVE_REMINDER_EMAIL_DELETE,
        id,
    };
}

export const REQUEST_REMINDER_EMAIL_DELETE_ERROR =
    'REQUEST_REMINDER_EMAIL_DELETE_ERROR';

function requestItemDeleteError(error, id) {
    return {
        type: REQUEST_REMINDER_EMAIL_DELETE_ERROR,
        error,
        id,
    };
}

export function fetchData(itemId = '') {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${itemId}?site=ci&group=en-us&include-email=1&year=${year}`,
            startAction: requestData,
            endAction: receiveData,
            errorAction: requestDataError,
        });
    };
}

export function pollData(itemId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        dispatch(requestPollDataPause(itemId));
        setTimeout(() => fetchItems({
            dispatch,
            endpoint: `${endpoint}${itemId}?site=ci&group=en-us&include-email=1&year=${year}`,
            startAction: requestPollData.bind(null, itemId),
            endAction: receiveUpdatedData,
            errorAction: requestDataError,
        }), 3500);
    };
}

export function sendReminderEmail(item) {
    return (dispatch, getState) => {
        const { domAttributes } = getState();
        if (
            domAttributes &&
            Object.hasOwnProperty.call(domAttributes, 'year')
        ) {
            item.year = domAttributes.year;
        }
        return addItem({
            item,
            dispatch,
            endpoint: `${endpoint}?site=ci&group=en-us`,
            startAction: requestItemAdd.bind(null, item.userEmail),
            endAction: receiveNewItem,
            endActionTimeout: requestItemAddTimeout,
            errorAction: addItemError.bind(null, item.userEmail),
        });
    };
}

export function deleteReminderEmail(item) {
    return (dispatch, getState) => {
        item._id = getState().reminderEmail.byEmail[item.userEmail].data._id;
        return deleteItem({
            item,
            dispatch,
            endpoint: `${endpoint}${item._id}`,
            startAction: requestItemDelete,
            endAction: receiveItemDelete,
            errorAction: requestItemDeleteError,
        });
    };
}
