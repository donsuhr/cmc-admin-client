import { combineReducers } from 'redux';
import * as actions from './actions';

export function getEmailByEmailId(items, id) {
    return Object.values(items).find(x =>
        x.data._id === id).data.userEmail;
}

const defaultState = {
    fetching: false,
    fetchError: null,
    polling: false,
    pollCount: 0,
    data: {},
};

const item = (state = defaultState, action) => {
    switch (action.type) {
        case actions.RECEIVE_REMINDER_EMAILS:
        case actions.RECEIVE_NEW_REMINDER_EMAIL:
            return {
                ...defaultState,
                data: {
                    ...action.item,
                },
                fetching: false,
                fetchError: null,
                polling: false,
                pollCount: 0,
            };
        case actions.RECEIVE_UPDATED_REMINDER_EMAILS:
            return {
                ...state,
                data: {
                    ...action.item,
                },
                fetching: false,
                fetchError: null,
                polling: false,
            };
        case actions.REQUEST_REMINDER_EMAIL_ADD:
            return {
                ...defaultState,
                fetching: true,
                fetchError: null,
            };
        case actions.ADD_REMINDER_EMAIL_ERROR:
        case actions.REQUEST_REMINDER_EMAIL_DELETE_ERROR:
            return {
                ...state,
                fetching: false,
                fetchError: action.error,
            };
        case actions.REQUEST_REMINDER_EMAIL_DELETE:
            return {
                ...state,
                fetching: true,
                fetchError: null,
                polling: false,
            };
        case actions.POLL_REMINDER_EMAILS:
            return {
                ...state,
                fetching: true,
                fetchError: null,
                polling: true,
                pollCount: state.pollCount + 1,
            };
        case actions.POLL_REMINDER_EMAILS_PAUSE:
            return {
                ...state,
                polling: true,
            };
        default:
            return state;
    }
};

const byEmail = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_REMINDER_EMAILS:
            return action.items.reduce((acc, currentItem) => {
                acc[currentItem.userEmail] = item(state[currentItem.userEmail], {
                    type: action.type,
                    item: currentItem,
                });
                return acc;
            }, {});
        case actions.RECEIVE_UPDATED_REMINDER_EMAILS:
            const updates = action.items.reduce((acc, currentItem) => {
                acc[currentItem.userEmail] = item(state[currentItem.userEmail], {
                    type: action.type,
                    item: currentItem,
                });
                return acc;
            }, {});
            return {
                ...state,
                ...updates,
            };
        case actions.REQUEST_REMINDER_EMAIL_ADD:
        case actions.RECEIVE_NEW_REMINDER_EMAIL:
        case actions.ADD_REMINDER_EMAIL_ERROR:
            return {
                ...state,
                [action.email]: item(state[action.email], action),
            };
        case actions.REQUEST_REMINDER_EMAIL_DELETE:
        case actions.REQUEST_REMINDER_EMAIL_DELETE_ERROR:
        case actions.POLL_REMINDER_EMAILS:
        case actions.POLL_REMINDER_EMAILS_PAUSE:
            const email1 = getEmailByEmailId(state, action.id);
            return {
                ...state,
                [email1]: item(state[email1], action),
            };
        case actions.RECEIVE_REMINDER_EMAIL_DELETE:
            const email2 = getEmailByEmailId(state, action.id);
            const newState = { ...state };
            delete newState[email2];
            return newState;
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_REMINDER_EMAILS:
            return true;
        case actions.RECEIVE_REMINDER_EMAILS:
        case actions.REQUEST_REMINDER_EMAILS_ERROR:
            return false;
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.RECEIVE_REMINDER_EMAILS:
        case actions.REQUEST_REMINDER_EMAILS_ERROR:
            return true;
        default:
            return state;
    }
};

const reminderEmail = combineReducers({
    hasEverLoaded,
    fetching,
    byEmail,
});

export default reminderEmail;

