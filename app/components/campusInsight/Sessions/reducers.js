import { combineReducers } from 'redux';
import * as actions from './actions';
import { RECEIVE_LOCATION_DELETE } from '../Locations/actions';
import {
    RECEIVE_PRESENTER,
    RECEIVE_PRESENTER_DELETE,
} from '../Presenters/actions';
import { RECEIVE_SESSION_TRACK_DELETE } from '../SessionTracks/actions';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    /*
        ___String are to satisfy proptypes on react-data-grid
        https://github.com/adazzle/react-data-grid/issues/706
     */
    switch (action.type) {
        case actions.REQUEST_SESSION_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.RECEIVE_SESSION:
            return {
                ...state,
                ...action.item,
                presenterCount:
                    action.item.presenter && action.item.presenter.length
                        ? action.item.presenter.length
                        : 0,
                userLevelString:
                    action.item.userLevel && action.item.userLevel.join
                        ? action.item.userLevel.join(', ')
                        : '',
                userTypeString:
                    action.item.userType && action.item.userType.join
                        ? action.item.userType.join(', ')
                        : '',
                trackString:
                    action.item.userType && action.item.track.length
                        ? action.item.track.map(x => x.title).join(',')
                        : '',
                idCopy: action.item._id,
                savingStatus: 'saved',
            };
        case actions.RECEIVE_SESSIONS:
            return {
                ...state,
                ...currentItem,
                presenterCount: currentItem.presenter
                    ? currentItem.presenter.length
                    : 0,
                userLevelString:
                    currentItem.userLevel && currentItem.userLevel.join
                        ? currentItem.userLevel.join(',')
                        : '',
                userTypeString:
                    currentItem.userType && currentItem.userType.join
                        ? currentItem.userType.join(',')
                        : '',
                trackString:
                    currentItem.track && currentItem.track.length
                        ? currentItem.track.map(x => x.title).join(',')
                        : '',
                idCopy: currentItem._id,
                savingStatus: '',
            };

        case actions.RECEIVE_SESSION_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.SESSION_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_SESSIONS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});
        case actions.REQUEST_SESSION_UPDATE:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_SESSION:
        case actions.RECEIVE_SESSION_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.SESSION_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_SESSION_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case RECEIVE_LOCATION_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (
                    state[key].location &&
                    state[key].location._id === action.id
                ) {
                    state[key].location = undefined;
                }
                acc[key] = state[key];
                return acc;
            }, {});
        case RECEIVE_SESSION_TRACK_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key].track) {
                    const index = state[key].track.findIndex(
                        x => x._id === action.id
                    );
                    if (index !== -1) {
                        state[key].track = [
                            ...state[key].track.slice(0, index),
                            ...state[key].track.slice(index + 1),
                        ];
                    }
                }
                acc[key] = state[key];
                return acc;
            }, {});
        case RECEIVE_PRESENTER:
            const updatedPresenterSessionIdList = action.item.sessions.map(
                x => x._id
            );
            return Object.keys(state).reduce((p, c) => {
                const current = state[c];
                const newPresenterIdList = state[c].presenter.slice();
                if (newPresenterIdList.includes(action.item._id)) {
                    if (!updatedPresenterSessionIdList.includes(current._id)) {
                        // this session was removed from the presenter
                        newPresenterIdList.splice(
                            newPresenterIdList.indexOf(action.item._id),
                            1
                        );
                    }
                }
                action.item.sessions.forEach((session) => {
                    if (session._id === current._id) {
                        if (!newPresenterIdList.includes(action.item._id)) {
                            // this session was added to the presenter
                            newPresenterIdList.push(action.item._id);
                        }
                    }
                });
                p[c] = {
                    ...current,
                    presenter: newPresenterIdList,
                };
                return p;
            }, {});
        case RECEIVE_PRESENTER_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key].presenter.includes(action.id)) {
                    const index = state[key].presenter.indexOf(action.id);
                    state[key].presenter = [
                        ...state[key].presenter.slice(0, index),
                        ...state[key].presenter.slice(index + 1),
                    ];
                }
                acc[key] = state[key];
                return acc;
            }, {});
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_SESSIONS:
        case actions.REQUEST_SESSION_ADD:
        case actions.REQUEST_SESSION_UPDATE:
        case actions.REQUEST_SESSION_DELETE:
            return true;
        case actions.RECEIVE_SESSIONS:
        case actions.SESSION_ADD_ERROR:
        case actions.RECEIVE_SESSION:
        case actions.SESSION_UPDATE_ERROR:
        case actions.RECEIVE_SESSION_DELETE:
        case actions.REQUEST_SESSION_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

const presenters = combineReducers({
    fetching,
    byId,
});

export default presenters;

export function getById(state, id) {
    return state.byId[id];
}

export function getPresenterSessions(state, presenterId) {
    if (Object.keys(state.byId).length < 1) {
        return [];
    }
    return Object.keys(state.byId).reduce((acc, sessionId) => {
        if (state.byId[sessionId].presenter.includes(presenterId)) {
            acc.push(sessionId);
        }
        return acc;
    }, []);
}
