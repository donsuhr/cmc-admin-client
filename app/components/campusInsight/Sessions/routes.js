/* eslint-disable no-shadow */
module.exports = {
    path: 'sessions',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('../Sessions/GridContainer').default);
        });
    },

    getChildRoutes(partialNextState, cb) {
        require.ensure([], (require) => {
            cb(null, [
                {
                    path: 'add',
                    getComponent(nextState, cb) {
                        require.ensure([], (require) => {
                            cb(null, require('../Sessions/FormContainer').default);
                        });
                    },
                },
                {
                    path: 'edit/:id',
                    getComponent(nextState, cb) {
                        require.ensure([], (require) => {
                            cb(null, require('../Sessions/FormContainer').default);
                        });
                    },
                },
            ]);
        });
    },
};

