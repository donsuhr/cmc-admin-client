import merge from 'lodash/merge';
import config from 'config';
import { fetchItems, updateItem, addItem, deleteItem } from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/sessions/`;

function normalizeResponse(response) {
    // const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = response.data.map(x => merge({}, {
        title: '',
        description: '',
    }, x));
    return response;
}

export const REQUEST_SESSIONS = 'REQUEST_SESSIONS';

export function requestSessions() {
    return {
        type: REQUEST_SESSIONS,
    };
}

export const RECEIVE_SESSIONS = 'RECEIVE_SESSIONS';

export function receiveSessions(items) {
    return {
        type: RECEIVE_SESSIONS,
        items: normalizeResponse(items),
    };
}

export const RECEIVE_SESSION = 'RECEIVE_SESSION';

export function receiveSession(item) {
    return {
        type: RECEIVE_SESSION,
        item: normalizeResponse({ data: [item] })[0],
    };
}

export const RECEIVE_SESSION_TIMEOUT = 'RECEIVE_SESSION_TIMEOUT';

export function receiveSessionTimeout(item) {
    return {
        type: RECEIVE_SESSION_TIMEOUT,
        item,
    };
}

export const REQUEST_SESSION_UPDATE = 'REQUEST_SESSION_UPDATE';

function requestSessionUpdate(id) {
    return {
        type: REQUEST_SESSION_UPDATE,
        id,
    };
}

export const SESSION_UPDATE_ERROR = 'SESSION_UPDATE_ERROR';

function requestSessionError(error, id) {
    return {
        type: SESSION_UPDATE_ERROR,
        error,
        id,
    };
}

export const REQUEST_SESSION_ADD = 'REQUEST_SESSION_ADD';

function requestSessionAdd() {
    return {
        type: REQUEST_SESSION_ADD,
    };
}

export const SESSION_ADD_ERROR = 'SESSION_ADD_ERROR';

function addSessionError(error, data) {
    return {
        type: SESSION_ADD_ERROR,
        error,
        data,
    };
}

export const REQUEST_SESSION_DELETE = 'REQUEST_SESSION_DELETE';

function requestSessionDelete(id) {
    return {
        type: REQUEST_SESSION_DELETE,
        id,
    };
}

export const RECEIVE_SESSION_DELETE = 'RECEIVE_SESSION_DELETE';

function receiveSessionDelete(id) {
    return {
        type: RECEIVE_SESSION_DELETE,
        id,
    };
}

export const REQUEST_SESSION_DELETE_ERROR = 'REQUEST_SESSION_DELETE_ERROR';

function requestSessionDeleteError(id) {
    return {
        type: REQUEST_SESSION_DELETE_ERROR,
        id,
    };
}

export function addSession(item) {
    return dispatch => addItem({
        item,
        dispatch,
        endpoint,
        startAction: requestSessionAdd,
        endAction: receiveSession,
        endActionTimeout: receiveSessionTimeout,
        errorAction: addSessionError,
    });
}

export function updateSession(item, change) {
    return dispatch => updateItem({
        item,
        change,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestSessionUpdate,
        endAction: receiveSession,
        endActionTimeout: receiveSessionTimeout,
        errorAction: requestSessionError,
    });
}

export function fetchSessions() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `${state.domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestSessions,
            endAction: receiveSessions,
            errorAction: requestSessionError,
        });
    };
}

export function deleteSession(item) {
    return dispatch => deleteItem({
        item,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestSessionDelete,
        endAction: receiveSessionDelete,
        errorAction: requestSessionDeleteError,
    });
}
