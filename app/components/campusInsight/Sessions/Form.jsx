import React from 'react';
import { Field, reduxForm } from 'redux-form';
import moment from 'moment';
import {
    renderField,
    renderTextArea,
    renderMultiSelect,
    normalizeToArray,
    renderDateTimeLocal,
} from '../util/form-util';

const validate = (values) => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Required';
    }
    if (moment(values.start) > moment(values.end)) {
        errors.end = 'End should be after start.';
    }
    return errors;
};

const Form = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        isEdit: React.PropTypes.bool.isRequired,
        error: React.PropTypes.bool,
        pristine: React.PropTypes.bool,
        submitting: React.PropTypes.bool,
        handleSubmit: React.PropTypes.func.isRequired,
        handleDelete: React.PropTypes.func.isRequired,
        presenters: React.PropTypes.object,
        sessionTracks: React.PropTypes.object,
        locations: React.PropTypes.object,
        year: React.PropTypes.string,
    },

    updateSite(val, instance) {
        const startValue = moment(val);
        const endFieldComponent = this.endField.getRenderedComponent();
        let endFieldValue;
        if (typeof endFieldComponent.props.input.value === 'object') {
            endFieldValue = moment(endFieldComponent.props.input.value);
        } else if (typeof endFieldComponent.props.input.value === 'string') {
            endFieldValue = moment(endFieldComponent.props.input.value).tz('America/New_York');
        } else {
            endFieldValue = startValue;
        }
        if (startValue.diff(endFieldValue, 'min') >= 0) {
            const later = startValue.clone().add(45, 'm').toDate();
            endFieldComponent.toggleWarning(true);
            endFieldComponent.updateValue(later);
        }
    },

    render() {
        const {
            error,
            handleSubmit,
            pristine,
            submitting,
            presenters,
            handleDelete,
            locations,
            sessionTracks,
            isEdit,
            year,
            ...props
        } = this.props;
        const presentersOptions = Object.keys(presenters.byId).map(x => ({
            value: presenters.byId[x]._id,
            label: `${presenters.byId[x].lastName}, ${presenters.byId[x].firstName}`,
        }));
        const locationsOptions = Object.keys(locations.byId).map(x => ({
            value: locations.byId[x]._id,
            label: locations.byId[x].title,
            disabled: !locations.byId[x].active,
        }));
        const tracksOptions = Object.keys(sessionTracks.byId).map(x => ({
            value: sessionTracks.byId[x]._id,
            label: sessionTracks.byId[x].title,
        }));
        const userLevelOptions = [
            { value: 'Beginner', label: 'Beginner' },
            { value: 'Intermediate', label: 'Intermediate' },
            { value: 'Expert', label: 'Expert' },
        ];
        const userTypeOptions = [
            { value: 'Functional', label: 'Functional' },
            { value: 'Technical', label: 'Technical' },
            { value: 'Executive', label: 'Executive' },
        ];
        return (
            <div className="cmc-form--sessions">
                <form onSubmit={handleSubmit} className="cmc-form">
                    <dl className="item-details-list">
                        <Field name="title" label="Title" component={renderField} type="text" />
                        <Field name="description" label="Description" component={renderTextArea} />
                        <Field
                            name="start"
                            label="Start Time"
                            component={renderDateTimeLocal}
                            year={year}
                            onChangeAction={this.updateSite}
                            item={props.item}
                        />
                        <Field
                            ref={(x) => {
                                this.endField = x;
                            }}
                            withRef
                            name="end"
                            label="End Time"
                            year={year}
                            component={renderDateTimeLocal}
                            item={props.item}
                        />
                        <Field
                            multi
                            name="presenter"
                            label="Presenters"
                            normalize={normalizeToArray}
                            component={renderMultiSelect}
                            options={presentersOptions}
                            isLoading={presenters.fetching}
                        />
                        <Field
                            multi
                            name="track"
                            label="Track"
                            normalize={normalizeToArray}
                            component={renderMultiSelect}
                            options={tracksOptions}
                            isLoading={presenters.fetching}
                        />
                        <Field
                            name="location._id"
                            label="Location"
                            component={renderMultiSelect}
                            options={locationsOptions}
                            isLoading={locations.fetching}
                        />
                        <Field
                            multi
                            name="userLevel"
                            label="Level of Expertise"
                            normalize={normalizeToArray}
                            component={renderMultiSelect}
                            options={userLevelOptions}
                        />
                        <Field
                            multi
                            name="userType"
                            label="Type of User"
                            normalize={normalizeToArray}
                            component={renderMultiSelect}
                            options={userTypeOptions}
                        />
                        <dt><label htmlFor="status">Status</label></dt>
                        <dd>
                            <Field name="status" label="Status" component="select" id="status">
                                <option value="" />
                                <option value="Pending Approval">Pending Approval</option>
                                <option value="Approved">Approved</option>
                                <option value="Cancelled">Cancelled</option>
                            </Field>
                        </dd>
                        <Field
                            name="active"
                            label="Active"
                            component={renderField}
                            type="checkbox"
                        />
                        <dt><label htmlFor="attendance">Attendance Count</label></dt>
                        <dd>
                            <Field
                                name="attendance"
                                label="Attendance"
                                component="input"
                                type="number"
                            />
                        </dd>
                        {
                            props.item && <div>
                                <dt>Created At</dt>
                                <dd>{moment(props.item.createdAt).format('YYYY-MM-DD hh:mm A')}</dd>
                                <dt>Updated At</dt>
                                <dd>{moment(props.item.updatedAt).format('YYYY-MM-DD hh:mm A')}</dd>
                            </div>
                        }
                    </dl>
                    <button
                        className="item-details__save-button"
                        type="submit"
                        disabled={pristine || submitting}
                    >
                        {isEdit ? 'Save' : 'Create'}
                    </button>
                    <br />
                    {
                        props.item && <button
                            className="item-details__delete-button"
                            onClick={handleDelete}
                            type="button"
                        >
                            Delete
                        </button>
                    }
                    <p>{error && <strong>{error}</strong>}</p>
                </form>
            </div>
        );
    },
});

const ContactForm = reduxForm({
    form: 'sessionForm',
    validate,
})(Form);

export default ContactForm;
