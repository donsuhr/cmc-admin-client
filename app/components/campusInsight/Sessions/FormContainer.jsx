import React from 'react';
import { connect } from 'react-redux';
import FormWrapper from '../components/Form-wrapper';
import { addSession, updateSession, deleteSession } from './actions';
import { fetchPresenters } from '../Presenters/actions';
import { fetchLocations } from '../Locations/actions';
import { fetchSessionTracks } from '../SessionTracks/actions';
import { getSessionById } from '../redux/reducers';
import Form from './Form';

const FormContainer = React.createClass({
    propTypes: {
        /* eslint-disable react/forbid-prop-types */
        params: React.PropTypes.object,
        presenters: React.PropTypes.object,
        locations: React.PropTypes.object,
        sessionTracks: React.PropTypes.object,
        fetchPresenters: React.PropTypes.func.isRequired,
        fetchLocations: React.PropTypes.func.isRequired,
        fetchSessionTracks: React.PropTypes.func.isRequired,
    },

    componentDidMount() {
        if (!this.props.presenters.fetching &&
            Object.keys(this.props.presenters.byId).length === 0) {
            this.props.fetchPresenters();
        }
        if (!this.props.locations.fetching &&
            Object.keys(this.props.locations.byId).length === 0) {
            this.props.fetchLocations();
        }
        if (!this.props.sessionTracks.fetching &&
            Object.keys(this.props.sessionTracks.byId).length === 0) {
            this.props.fetchSessionTracks();
        }
    },

    preSubmitProcess(data) {
        const ret = { ...data };
        if (ret.attendance && (ret.attendance === '' || ret.attendance === undefined)) {
            ret.attendance = null;
        }

        return ret;
    },

    render() {
        return (
            <FormWrapper
                returnTo="/sessions"
                addAction={addSession}
                updateAction={updateSession}
                deleteAction={deleteSession}
                getById={getSessionById}
                stateKey="sessions"
                formName="sessionForm"
                presenters={this.props.presenters}
                year={this.props.year}
                sessionTracks={this.props.sessionTracks}
                locations={this.props.locations}
                params={this.props.params}
                formComponent={Form}
                preSubmitProcess={this.preSubmitProcess}
            />
        );
    },
});

function mapStateToProps(state, ownProps) {
    return {
        presenters: state.presenters,
        locations: state.locations,
        sessionTracks: state.sessionTracks,
        year: state.domAttributes.year,
    };
}

const FormContainerConnected = connect(mapStateToProps, {
    fetchPresenters,
    fetchSessionTracks,
    fetchLocations,
})(FormContainer);

export default FormContainerConnected;
