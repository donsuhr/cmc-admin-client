import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import moment from '../util/load-moment-tz';
import { fetchSessions, updateSession } from './actions';
import CheckBoxEditor from '../react-data-grid-editors/CheckBoxEditor';
import asyncDropDownEditorHOC from '../react-data-grid-editors/AsyncDropDownEditor';
import SelectLocations from '../react-data-grid-editors/Select--locations';
import Grid from '../components/Grid';
import { getUnicodeStatus } from '../util/grid-util';
import { getById } from './reducers';

const LocationSelectDdEditor = asyncDropDownEditorHOC(SelectLocations);

/* eslint-disable react/prop-types */
const gridColumns = gridContainerProps => ([
    {
        key: '_id',
        name: '',
        width: 35,
        formatter: props => (
            <div>
                <Link to={`${props.dependentValues.gridProps.location.pathname}/edit/${props.value}`}>
                    Edit
                </Link>
            </div>
        ),
        cellClass: 'presenter-grid__cell--actions',
        getRowMetaData() {
            return { gridProps: gridContainerProps };
        },
    },
    {
        key: 'title',
        name: 'Title',
        editable: true,
        sortable: true,
        resizable: true,
        width: 100,
    },
    {
        key: 'location',
        name: 'Location',
        editable: true,
        sortable: true,
        resizable: true,
        formatter: ({ value }) => (<div>{value && value.title}</div>),
        editor: LocationSelectDdEditor,
        editorPristineValue: '_id',
        width: 100,
    },
    {
        key: 'description',
        name: 'Description',
        sortable: true,
        resizable: true,
        width: 100,
    },
    {
        key: 'userTypeString',
        name: 'Type',
        sortable: true,
        resizable: true,
        width: 100,
    },
    {
        key: 'userLevelString',
        name: 'Level',
        sortable: true,
        resizable: true,
        width: 100,
    },
    {
        key: 'trackString',
        name: 'Track',
        sortable: true,
        resizable: true,
        width: 100,
    },
    {
        key: 'attendance',
        name: 'Atnd',
        editable: true,
        sortable: true,
        resizable: true,
        width: 45,
    },
    {
        key: 'start',
        name: 'Start Time',
        sortable: true,
        resizable: true,
        width: 135,
        formatter: ({ value }) => (<div>{value ? moment(value).tz('America/New_York').format('YYYY-MM-DD hh:mm A') : ''}</div>),
    },
    {
        key: 'end',
        name: 'End Time',
        sortable: true,
        resizable: true,
        width: 135,
        formatter: ({ value }) => (<div>{value ? moment(value).tz('America/New_York').format('YYYY-MM-DD hh:mm A') : ''}</div>),
    },
    {
        key: 'status',
        name: 'Status',
        sortable: true,
        resizable: true,
        width: 85,
    },
    {
        key: 'active',
        name: 'Act',
        editable: true,
        sortable: true,
        width: 35,
        formatter: ({ value }) => (<div>{value ? '✓' : '✖'}</div>),
        cellClass: 'presenter-grid__cell--active',
        editor: CheckBoxEditor,
    },
    {
        key: 'presenterCount',
        name: 'Pres',
        sortable: true,
        width: 35,
        cellClass: 'presenter-grid__cell--sessions',
    },
    {
        key: 'savingStatus',
        name: '↹',
        width: 35,
        formatter: ({ value }) => (<div>{getUnicodeStatus(value).savingStatus}</div>),
        cellClass: 'presenter-grid__cell--saving',
    },
    {
        key: 'idCopy',
        name: '',
        width: 45,
        formatter: props => (
            <div>
                <Link to={`${props.dependentValues.gridProps.location.pathname}/edit/${props.value}`}>
                    Edit
                </Link>
            </div>
        ),
        cellClass: 'presenter-grid__cell--actions',
        getRowMetaData() {
            return { gridProps: gridContainerProps };
        },
    },
]);

const GridContainer = props => (
    <Grid
        columns={gridColumns(props)}
        {...props}
    />
);

function mapStateToProps(state, ownProps) {
    return {
        dataById: state.sessions.byId,
        fetching: state.sessions.fetching,
        getItemById: getById.bind(null, state.sessions),
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        update: bindActionCreators(updateSession, dispatch),
        fetch: bindActionCreators(fetchSessions, dispatch),
    };
}

const GridConnected = connect(mapStateToProps, mapDispatchToProps)(GridContainer);

export default GridConnected;
