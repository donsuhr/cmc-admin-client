/* eslint global-require:0 */
let App;

if (process.env.NODE_ENV === 'production') {
    App = require('./App/App.prod').default;
} else {
    App = require('./App/App.dev').default;
}

const createRoutes = () => ({
    path: '/',
    component: App,
    childRoutes: [
        require('./Presenters/routes'),
        require('./Sessions/routes'),
        require('./PreConfTraining/routes'),
        require('./Locations/routes'),
        require('./Products/routes'),
        require('./SessionTracks/routes'),
        require('./PctTracks/routes'),
        require('./SupportAnalysts/routes'),
        require('./ConsultationReminder/routes'),
    ],
});

export default createRoutes;
