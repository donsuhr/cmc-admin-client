import React from 'react';
import $ from 'jquery';
import 'jquery-ui/ui/widgets/dialog';

const Confirm = React.createClass({
    propTypes: {
        confirmLabel: React.PropTypes.string,
        abortLabel: React.PropTypes.string,
        description: React.PropTypes.string,
    },

    getDefaultProps() {
        return {
            confirmLabel: 'OK',
            abortLabel: 'Cancel',
        };
    },

    componentDidMount() {
        this.promise = new Promise((resolve, reject) => {
            this.rejectPromise = reject;
            this.resolvePromise = resolve;
        });
        this.dialog = $(this.modalContent).dialog({
            appendTo: this.$el,
            resizable: false,
            height: 'auto',
            width: 400,
            modal: true,
            buttons: {
                [this.props.confirmLabel]: () => {
                    this.confirm();
                },
                [this.props.abortLabel]: () => {
                    this.abort();
                },
            },
        });
    },

    componentWillUnmount() {
        this.dialog.dialog('destroy');
    },

    abort() {
        return this.rejectPromise();
    },

    confirm() {
        return this.resolvePromise();
    },

    render() {
        const spanStyle = {
            float: 'left',
            margin: '12px 12px 20px 0',
        };

        return (
            <div className="dialogWrapper">
                <div
                    id="dialog-confirm"
                    title="Delete Item"
                    ref={(x) => {
                        this.modalContent = x;
                    }}
                >
                    <p>
                        <span className="ui-icon ui-icon-alert" style={spanStyle} />
                        {
                            this.props.description ||
                            'The item will be permanently deleted and cannot be recovered. Are you sure?'
                        }
                    </p>
                </div>
            </div>
        );
    },
});

export default Confirm;
