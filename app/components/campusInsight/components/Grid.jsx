import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import debounce from 'lodash/debounce';
import ReactDataGrid from 'react-data-grid';
import isEqual from 'lodash/isEqual';

import Loading from '../../client-side-js-error-list/components/loading';
import { EmptyRowsView, RowRenderer } from '../util/grid-util';
import ExcelColumnShape from '../react-data-grid-editors/ExcelColumn.Shape';

const Grid = React.createClass({
    propTypes: {
        children: React.PropTypes.oneOfType([
            React.PropTypes.arrayOf(React.PropTypes.node),
            React.PropTypes.node,
        ]),
        // eslint-disable-next-line react/forbid-prop-types
        dataById: React.PropTypes.object.isRequired,
        update: React.PropTypes.func.isRequired,
        fetch: React.PropTypes.func.isRequired,
        getItemById: React.PropTypes.func.isRequired,
        columns: React.PropTypes.arrayOf(React.PropTypes.shape(ExcelColumnShape)).isRequired,
        fetching: React.PropTypes.bool.isRequired,
        location: React.PropTypes.shape({
            pathname: React.PropTypes.string,
        }).isRequired,

    },

    getInitialState() {
        return {
            rows: Object.keys(this.props.dataById).map(x => this.props.dataById[x]) || [],
            currentSort: '_id',
            currentSortDirection: 'NONE',
            scroll: {
                left: 0,
                top: 0,
            },
        };
    },

    componentDidMount() {
        if (Object.keys(this.props.dataById).length === 0) {
            this.props.fetch();
        }
    },

    componentWillReceiveProps(nextProps) {
        let rows = Object.keys(nextProps.dataById).map(x => nextProps.dataById[x]);
        rows = this.sortRows(this.state.currentSort, this.state.currentSortDirection, rows);
        this.setState({
            rows: rows || [],
        });
    },

    onGridScroll(e) {
        this.setState({ scroll: { top: e.target.scrollTop, left: e.target.scrollLeft } });
    },

    handleRowUpdated(e) {
        const item = this.props.getItemById(this.rowGetter(e.rowIdx)._id);
        const newItem = {
            ...item,
            ...e.updated,
        };
        if (!isEqual(item, newItem)) {
            this.props.update(item, e.updated);
        }
    },

    handleGridSort(sortColumn, sortDirection) {
        const rows = this.sortRows(sortColumn, sortDirection, this.state.rows);
        this.setState({
            currentSort: sortColumn,
            currentSortDirection: sortDirection,
            rows,
        });
    },

    sortRows(sortColumn, sortDirection, rows) {
        const comparer = (a, b) => {
            switch (sortColumn) {
                case 'line':
                    a = parseInt(a[sortColumn], 10);
                    b = parseInt(b[sortColumn], 10);
                    if (sortDirection === 'ASC') {
                        return (a > b) ? 1 : -1;
                    }
                    return (a < b) ? 1 : -1;
                default:
                    if (sortDirection === 'ASC') {
                        return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
                    }
                    return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
            }
        };
        return sortDirection === 'NONE' ? rows.slice(0) : [...rows].sort(comparer);
    },

    rowGetter(i) {
        return this.state.rows[i];
    },

    newGridRender(grid) {
        if (!this.debouncedScrollHandler) {
            this.debouncedScrollHandler = debounce(this.onGridScroll, 250);
        }
        this.grid = grid;
        let gridChange = false;
        // eslint-disable-next-line react/no-find-dom-node
        const newGridDom = grid && ReactDOM.findDOMNode(grid);
        const newCanvas = newGridDom && newGridDom.querySelector('.react-grid-Canvas');
        if (this.gridCanvas && newCanvas && newCanvas !== this.gridCanvas) {
            this.gridCanvas.removeEventListener('scroll', this.debouncedScrollHandler);
            gridChange = true;
        }
        if (newCanvas) {
            this.gridCanvas = newCanvas;
            this.gridCanvas.addEventListener('scroll', this.debouncedScrollHandler);
            if (gridChange) {
                this.gridCanvas.scrollLeft = this.state.scroll.left;
                this.gridCanvas.scrollTop = this.state.scroll.top;
            }
        }
    },

    render() {
        return (
            <div>
                {
                    this.props.children || <div>
                        <ReactDataGrid
                            ref={(c) => {
                                this.newGridRender(c);
                            }}
                            onGridSort={this.handleGridSort}
                            columns={this.props.columns}
                            rowGetter={this.rowGetter}
                            rowsCount={this.state.rows.length}
                            rowRenderer={RowRenderer}
                            enableCellSelect
                            minHeight={500}
                            onRowUpdated={this.handleRowUpdated}
                            emptyRowsView={EmptyRowsView}
                            rowHeight={30}
                        />
                        <Link
                            className="item-details__back-button"
                            to={`${this.props.location.pathname}/add`}
                        >
                            Add
                        </Link>
                        {this.props.fetching && (<Loading>Loading...</Loading>)}
                    </div>
                }
            </div>
        );
    },
});

export default Grid;
