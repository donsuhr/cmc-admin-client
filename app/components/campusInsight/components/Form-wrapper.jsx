import React from 'react';
import ReactDom from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import { routerShape } from 'react-router/lib/PropTypes';
import { reset } from 'redux-form';
import isEqual from 'lodash/isEqual';

import Confirm from './Confirm';
import Loading from '../../client-side-js-error-list/components/loading';

const FormWrapper = React.createClass({
    propTypes: {
        reset: React.PropTypes.func.isRequired,
        formName: React.PropTypes.string.isRequired,
        returnTo: React.PropTypes.string.isRequired,
        addAction: React.PropTypes.func.isRequired,
        updateAction: React.PropTypes.func.isRequired,
        deleteAction: React.PropTypes.func.isRequired,
        // eslint-disable-next-line react/forbid-prop-types
        item: React.PropTypes.object,
        isEdit: React.PropTypes.bool.isRequired,
        fetching: React.PropTypes.bool.isRequired,
        formComponent: React.PropTypes.func.isRequired,
        preSubmitProcess: React.PropTypes.func,
    },

    contextTypes: {
        router: routerShape,
    },

    handleSubmitSuccess(result, dispatch) {
        this.props.reset(this.props.formName);
        this.context.router.push(this.props.returnTo);
    },

    handleSubmit(data) {
        if (this.props.isEdit) {
            let changes = Object.keys(data).reduce((accum, key) => {
                if (!isEqual(data[key], this.props.item[key])) {
                    accum[key] = data[key];
                }
                return accum;
            }, {});
            if (this.props.preSubmitProcess) {
                changes = this.props.preSubmitProcess(changes);
            }
            return this.props.updateAction(this.props.item, changes);
        }
        return this.props.addAction(data);
    },

    handleDelete(event) {
        const wrapper = document.body.appendChild(document.createElement('div'));
        const confirmComponent = ReactDom.render(
            <Confirm confirmLabel="Delete" item={this.props.item} />,
            wrapper
        );
        this.cleanupConfirm = () => {
            ReactDom.unmountComponentAtNode(wrapper);
            return setTimeout(() => {
                wrapper.remove();
            });
        };
        confirmComponent.promise
            .then(
                () => {
                    this.props.deleteAction(this.props.item)
                        .then((res) => {
                            this.cleanupConfirm();
                            this.context.router.push(this.props.returnTo);
                        });
                },
                () => {
                    this.cleanupConfirm();
                });
    },

    render() {
        const showLoading = this.props.fetching ||
            (this.props.item && this.props.item.savingStatus === 'saving');

        const props = {
            onSubmit: this.handleSubmit,
            onSubmitSuccess: this.handleSubmitSuccess,
            initialValues: this.props.item,
            handleDelete: this.handleDelete,
            ...this.props,
        };
        const formComponent = React.createElement(this.props.formComponent, props);

        return (
            <div>
                {formComponent}
                <Link to={this.props.returnTo} className="item-details__back-button">Back</Link>
                {showLoading && <Loading>Loading...</Loading>}
            </div>
        );
    },
});

function mapStateToProps(state, ownProps) {
    const ret = {
        isEdit: false,
        fetching: state[ownProps.stateKey].fetching,
    };
    if (ownProps.params && ownProps.params.id) {
        const item = ownProps.getById(state, ownProps.params.id);
        if (item) {
            ret.item = item;
            ret.isEdit = true;
        }
    }
    return ret;
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        updateAction: bindActionCreators(ownProps.updateAction, dispatch),
        addAction: bindActionCreators(ownProps.addAction, dispatch),
        deleteAction: bindActionCreators(ownProps.deleteAction, dispatch),
        reset: bindActionCreators(reset, dispatch),
    };
}

const FormContainer = connect(mapStateToProps, mapDispatchToProps)(FormWrapper);

export default FormContainer;
