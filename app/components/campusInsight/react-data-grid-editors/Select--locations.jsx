import React from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { fetchLocations } from '../Locations/actions';

class WrappedSelect extends React.Component {
    componentDidMount() {
        if (!this.props.locations.fetching &&
            Object.keys(this.props.locations.byId).length === 0) {
            this.props.fetchLocations();
        }
    }

    render() {
        const { locations, value, placeholder, onChange } = this.props;
        const locationsOptions = Object.keys(locations.byId).map(x => ({
            value: locations.byId[x]._id,
            label: locations.byId[x].title,
            disabled: !locations.byId[x].active,
        }));
        return (
            <Select
                simpleValue
                value={value}
                placeholder={placeholder}
                onChange={onChange}
                options={locationsOptions}
                isLoading={locations.fetching}
            />
        );
    }
}

WrappedSelect.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    locations: React.PropTypes.object,
    fetchLocations: React.PropTypes.func.isRequired,
    value: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    onChange: React.PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
    return {
        locations: state.locations,
    };
}

const SelectLocations = connect(mapStateToProps, { fetchLocations })(WrappedSelect);

export default SelectLocations;
