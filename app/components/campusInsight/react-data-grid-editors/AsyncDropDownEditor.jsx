import React from 'react';
import ReactDOM from 'react-dom';

function asyncDropDownEditorHOC(WrappedComponent) {
    class AsyncDropDownEditor extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                value: this.props.value._id,
            };
            this.handleSelectChange = this.handleSelectChange.bind(this);
        }

        getInputNode() {
            // eslint-disable-next-line react/no-find-dom-node
            return ReactDOM.findDOMNode(this);
        }

        getValue() {
            const updated = {};
            if (this.hasResults()) {
                updated[this.props.column.key] = this.state.value;
            }
            return updated;
        }

        hasResults() {
            let pristine = this.props.rowData[this.props.column.key];
            if (pristine && this.props.column.editorPristineValue) {
                pristine = pristine[this.props.column.editorPristineValue];
            }
            return this.state.value !== pristine;
        }

        handleSelectChange(value) {
            this.setState({ value });
        }

        render() {
            return (
                <WrappedComponent
                    value={this.state.value}
                    placeholder={this.props.column.name}
                    onChange={this.handleSelectChange}
                />
            );
        }
    }

    AsyncDropDownEditor.propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        column: React.PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        rowData: React.PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        value: React.PropTypes.shape({
            _id: React.PropTypes.String,
        }),
    };

    return AsyncDropDownEditor;
}

export default asyncDropDownEditorHOC;
