import React from 'react';
import moment from '../util/load-moment-tz';

class DateEditor extends React.Component {
    static getStyle() {
        return {
            width: '100%',
        };
    }

    static inheritContainerStyles() {
        return true;
    }

    getValue() {
        const updated = {};
        const value = this.getInputNode().value;
        const parts = moment(value).format('YYYY-MM-DD hh:mm A');
        const asEST = moment.tz(parts, 'YYYY-MM-DD hh:mm A', 'America/New_York').toDate();
        updated[this.props.column.key] = asEST;
        return updated;
    }

    getInputNode() {
        return this.input;
    }

    render() {
        return (
            // eslint-disable-next-line jsx-a11y/no-static-element-interactions
            <div
                onKeyDown={(event) => {
                    event.stopPropagation();
                }}
            >
                <input
                    ref={(input) => {
                        this.input = input;
                    }}
                    type="datetime-local"
                    className="form-control"
                    defaultValue={this.props.value ?
                        moment(this.props.value).tz('America/New_York').format('YYYY-MM-DDThh:mm') :
                        moment().format('YYYY-MM-DDThh:mm')}
                />
            </div>
        );
    }
}

DateEditor.propTypes = {
    column: React.PropTypes.shape({
        key: React.PropTypes.string,
        onCellChange: React.PropTypes.func,
    }),
    value: React.PropTypes.oneOfType([
        React.PropTypes.instanceOf(Date),
        React.PropTypes.string,
    ]),
};

export default DateEditor;
