import React from 'react';
import { Editors } from 'react-data-grid-addons';

const { SimpleTextEditor } = Editors;

class CheckBoxEditor extends SimpleTextEditor {
    getInputNode() {
        return this.input;
    }

    getValue() {
        const updated = {};
        updated[this.props.column.key] = this.getInputNode().checked;
        return updated;
    }

    render() {
        return (
            <input
                ref={(input) => {
                    this.input = input;
                }}
                type="checkbox"
                className="form-control"
                defaultChecked={!!this.props.value}
            />
        );
    }
}

export default CheckBoxEditor;
