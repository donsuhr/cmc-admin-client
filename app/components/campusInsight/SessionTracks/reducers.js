import { combineReducers } from 'redux';
import * as actions from './actions';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.REQUEST_SESSION_TRACK_UPDATE:
            if (state._id === action.id) {
                return {
                    ...state,
                    savingStatus: 'saving',
                };
            }
            return state;
        case actions.RECEIVE_SESSION_TRACK:
            return {
                ...state,
                savingStatus: 'saved',
                ...action.item,
            };
        case actions.RECEIVE_SESSION_TRACK_TIMEOUT:
            return {
                ...state,
                savingStatus: '',
            };
        case actions.SESSION_TRACK_UPDATE_ERROR:
            return {
                ...state,
                savingStatus: `Error:${action.error}`,
            };
        case actions.RECEIVE_SESSION_TRACKS: {
            return {
                ...defaultState,
                ...state,
                ...currentItem,
                savingStatus: '',
            };
        }
        default:
            return state;
    }
};

const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_SESSION_TRACKS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});
        case actions.REQUEST_SESSION_TRACK_UPDATE:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        case actions.RECEIVE_SESSION_TRACK:
        case actions.RECEIVE_SESSION_TRACK_TIMEOUT:
            return {
                ...state,
                [action.item._id]: item(state[action.item._id], action),
            };
        case actions.RECEIVE_SESSION_TRACK_DELETE:
            return Object.keys(state).reduce((acc, key) => {
                if (state[key]._id !== action.id) {
                    acc[key] = state[key];
                }
                return acc;
            }, {});
        case actions.SESSION_TRACK_UPDATE_ERROR:
            return {
                ...state,
                [action.id]: item(state[action.id], action),
            };
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_SESSION_TRACKS:
        case actions.REQUEST_SESSION_TRACK_ADD:
        case actions.REQUEST_SESSION_TRACK_UPDATE:
        case actions.REQUEST_SESSION_TRACK_DELETE:
            return true;
        case actions.RECEIVE_SESSION_TRACKS:
        case actions.SESSION_TRACK_ADD_ERROR:
        case actions.SESSION_TRACK_UPDATE_ERROR:
        case actions.RECEIVE_SESSION_TRACK:
        case actions.RECEIVE_SESSION_TRACK_DELETE:
        case actions.REQUEST_SESSION_TRACK_DELETE_ERROR:
            return false;
        default:
            return state;
    }
};

const sessionTracks = combineReducers({
    fetching,
    byId,
});

export default sessionTracks;

export function getById(state, id) {
    return state.byId[id];
}
