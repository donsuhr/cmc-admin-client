import merge from 'lodash/merge';
import omitBy from 'lodash/omitBy';
import isNil from 'lodash/isNil';
import config from 'config';
import { fetchItems, updateItem, addItem, deleteItem } from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/sessionTracks/`;

function normalizeResponse(response) {
    const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = nullRemoved.map(x => merge({}, {
        title: '',
    }, x));
    return response;
}

export const REQUEST_SESSION_TRACKS = 'REQUEST_SESSION_TRACKS';

export function requestSessionTracks() {
    return {
        type: REQUEST_SESSION_TRACKS,
    };
}

export const RECEIVE_SESSION_TRACKS = 'RECEIVE_SESSION_TRACKS';

export function receiveSessionTracks(items) {
    return {
        type: RECEIVE_SESSION_TRACKS,
        items: normalizeResponse(items),
    };
}

export const RECEIVE_SESSION_TRACK = 'RECEIVE_SESSION_TRACK';

export function receiveSessionTrack(item) {
    return {
        type: RECEIVE_SESSION_TRACK,
        item: normalizeResponse({ data: [item] })[0],
    };
}

export const RECEIVE_SESSION_TRACK_TIMEOUT = 'RECEIVE_SESSION_TRACK_TIMEOUT';

export function receiveSessionTrackTimeout(item) {
    return {
        type: RECEIVE_SESSION_TRACK_TIMEOUT,
        item,
    };
}

export const REQUEST_SESSION_TRACK_UPDATE = 'REQUEST_SESSION_TRACK_UPDATE';

function requestSessionTrackUpdate(id) {
    return {
        type: REQUEST_SESSION_TRACK_UPDATE,
        id,
    };
}

export const SESSION_TRACK_UPDATE_ERROR = 'SESSION_TRACK_UPDATE_ERROR';

function requestSessionTrackError(error, id) {
    return {
        type: SESSION_TRACK_UPDATE_ERROR,
        error,
        id,
    };
}

export const REQUEST_SESSION_TRACK_ADD = 'REQUEST_SESSION_TRACK_ADD';

function requestSessionTrackAdd() {
    return {
        type: REQUEST_SESSION_TRACK_ADD,
    };
}

export const SESSION_TRACK_ADD_ERROR = 'SESSION_TRACK_ADD_ERROR';

function addSessionTrackError(error, data) {
    return {
        type: SESSION_TRACK_ADD_ERROR,
        error,
        data,
    };
}

export const REQUEST_SESSION_TRACK_DELETE = 'REQUEST_SESSION_TRACK_DELETE';

function requestSessionTrackDelete(id) {
    return {
        type: REQUEST_SESSION_TRACK_DELETE,
        id,
    };
}

export const RECEIVE_SESSION_TRACK_DELETE = 'RECEIVE_SESSION_TRACK_DELETE';

function receiveSessionTrackDelete(id) {
    return {
        type: RECEIVE_SESSION_TRACK_DELETE,
        id,
    };
}

export const REQUEST_SESSION_TRACK_DELETE_ERROR = 'REQUEST_SESSION_TRACK_DELETE_ERROR';

function requestSessionTrackDeleteError(id) {
    return {
        type: REQUEST_SESSION_TRACK_DELETE_ERROR,
        id,
    };
}

export function addSessionTrack(item) {
    return (dispatch, getState) => {
        const { domAttributes } = getState();
        if (domAttributes && Object.hasOwnProperty.call(domAttributes, 'year')) {
            item.year = domAttributes.year;
        }
        return addItem({
            item,
            dispatch,
            endpoint,
            startAction: requestSessionTrackAdd,
            endAction: receiveSessionTrack,
            endActionTimeout: receiveSessionTrackTimeout,
            errorAction: addSessionTrackError,
        });
    };
}

export function updateSessionTrack(item, change) {
    const newItem = {
        ...item,
        ...change,
    };

    return dispatch => updateItem({
        item: newItem,
        change,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestSessionTrackUpdate,
        endAction: receiveSessionTrack,
        endActionTimeout: receiveSessionTrackTimeout,
        errorAction: requestSessionTrackError,
    });
}

export function fetchSessionTracks() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `${state.domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestSessionTracks,
            endAction: receiveSessionTracks,
            errorAction: requestSessionTrackError,
        });
    };
}

export function deleteSessionTrack(item) {
    return dispatch => deleteItem({
        item,
        dispatch,
        endpoint: `${endpoint}${item._id}`,
        startAction: requestSessionTrackDelete,
        endAction: receiveSessionTrackDelete,
        errorAction: requestSessionTrackDeleteError,
    });
}
