import React from 'react';
import { connect } from 'react-redux';
import FormWrapper from '../components/Form-wrapper';
import { addSessionTrack, updateSessionTrack, deleteSessionTrack } from './actions';
import Form from './Form';
import { getSessionTrackById } from '../redux/reducers';

const FormContainer = ({ params, year }) => (
    <FormWrapper
        returnTo="/sessionTracks"
        addAction={addSessionTrack}
        updateAction={updateSessionTrack}
        deleteAction={deleteSessionTrack}
        getById={getSessionTrackById}
        stateKey="sessionTracks"
        year={year}
        formName="trackForm"
        params={params}
        formComponent={Form}
    />
);

function mapStateToProps(state, ownProps) {
    return {
        sessions: state.sessions,
        pcts: state.pcts,
        year: state.domAttributes.year,
    };
}

FormContainer.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    params: React.PropTypes.object,
    year: React.PropTypes.string,
};

const FormContainerConnected = connect(
    mapStateToProps
)(FormContainer);

export default FormContainerConnected;
