/* eslint-disable no-shadow */
module.exports = {
    path: 'sessionTracks',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./GridContainer').default);
        });
    },

    getChildRoutes(partialNextState, cb) {
        require.ensure([], (require) => {
            cb(null, [
                {
                    path: 'add',
                    getComponent(nextState, cb) {
                        require.ensure([], (require) => {
                            cb(null, require('./FormContainer').default);
                        });
                    },
                },
                {
                    path: 'edit/:id',
                    getComponent(nextState, cb) {
                        require.ensure([], (require) => {
                            cb(null, require('./FormContainer').default);
                        });
                    },
                },
            ]);
        });
    },
};

