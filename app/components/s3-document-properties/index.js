import Backbone from 'backbone';
import AppView from './views/appView';
import Router from '../generic-list-detail/router';

export default {
    init(options) {
        const router = new Router();

        new AppView({ // eslint-disable-line no-new
            el: options.el,
            router,
            site: options.site,
            group: options.group,
            ACL: options.ACL,
            label: options.label,
            apiFolder: options.apiFolder,
        });
        Backbone.history.start();
    },
};
