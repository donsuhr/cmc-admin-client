import $ from 'jquery';
import Backbone from 'backbone';
import assign from 'lodash/assign';
import isArray from 'lodash/isArray';
import 'parsleyjs';
import 'jquery-ui/ui/widgets/dialog';
import formSerialize from 'form-serialize';
import template from '../templates/itemDetailView.hbs';
import editTemplate from '../templates/itemDetailEdit.hbs';
import { auth } from '../../preconfigured-auth';
import confirmDeleteTemplate from '../../generic-list-detail/templates/confirm-delete-dialog.hbs';

export default Backbone.View.extend({
    initialize(options) {
        this.label = options.label;
        this.viewTemplate = options.viewTemplate || template;
        this.activeTemplate = this.viewTemplate;
        this.editTemplate = options.editTemplate || editTemplate;
        this.group = options.group;
        this.site = options.site;
    },

    tagName: 'div',

    events: {
        'click .item-details__back-button': 'onBackClicked',
        'click .item-details__edit-button': 'onEditClicked',
        'click .item-details__save-button': 'onSaveClicked',
        'click .item-details__cancel-button': 'onCancelClicked',
        'click .list-details__delete-button': 'onDeleteClicked',
    },

    updateModel(model) {
        this.pageYOffset = 0;
        if (this.model) {
            this.model.off(null, null, this);
        }
        this.activeTemplate = this.viewTemplate;
        this.model = model;
    },

    render() {
        const vm = assign(
            {},
            this.model.toJSON(),
            { label: this.label, site: this.site, group: this.group }
        );
        this.$el.html(this.activeTemplate(vm));
        if (typeof this.pageYOffset !== 'undefined') {
            window.scrollTo(0, this.pageYOffset);
        }
        this.delegateEvents();
        return this;
    },

    renderView() {
        this.activeTemplate = this.viewTemplate;
        this.render();
    },

    renderEdit() {
        this.activeTemplate = this.editTemplate;
        this.render();
    },

    onBackClicked(event) {
        event.preventDefault();
        this.trigger('backClicked', { action: 'show-home' });
    },

    onEditClicked(event) {
        event.preventDefault();
        this.renderEdit();
    },

    onCancelClicked(event) {
        event.preventDefault();
        this.renderView();
    },

    onSaveClicked(event) {
        event.preventDefault();
        const $form = this.$el.find('form');
        const parsleyApi = $form.parsley();
        const valid = parsleyApi.validate();
        if (valid) {
            this.trigger('saveStart');
            const obj = formSerialize($form.get(0), { hash: true, empty: true, disabled: true });
            $form.find('[type="checkbox"][value="true"]').each((index, el) => {
                obj[el.name] = $(el).is(':checked');
            });
            Object.keys(obj).forEach((key) => {
                if (isArray(obj[key])) {
                    obj[key] = obj[key].filter(x => !!x);
                }
            });
            this.model.set(obj);
            this.$el.find('.item-details__status').text('Saving...');
            this.model
                .patch(auth)
                .then((result) => {
                    this.renderView();
                })
                .catch((error) => {
                    // eslint-disable-next-line no-console
                    console.log('request failed', error);
                    this.$el.find('.item-details__status').text('Error Saving Item');
                })
                .then(() => {
                    this.trigger('saveStop');
                });
        }
    },

    onDeleteClicked(event) {
        const dialogContent = confirmDeleteTemplate();
        this.confirmDeleteDialog = $(dialogContent).dialog({
            appendTo: this.$el,
            resizable: false,
            height: 'auto',
            width: 400,
            modal: true,
            buttons: {
                Delete: () => {
                    this.onConfirmDelete();
                    this.confirmDeleteDialog.dialog('close');
                },
                Cancel: () => {
                    this.confirmDeleteDialog.dialog('close');
                },
            },
        });
    },

    onConfirmDelete() {
        this.model.destroy({
            success: (model, response) => {
                this.trigger('saveStop');
                this.trigger('backClicked', { action: 'show-home' });
            },
            error: () => {
                this.trigger('saveStop');
                this.$el.find('.item-details__status').text('Error Deleting Item');
            },
        });
    },

});
