import Backbone from 'backbone';
import ItemView from '../../generic-list-detail/views/listItemView';
import itemViewTemplate from '../templates/listItemView.hbs';

export default Backbone.View.extend({
    initialize(options) {
        this.options = options;
        this.propertiesCollection = options.propertiesCollection;
        this.s3Collection = options.s3Collection;
        this.awsEver = false;
        this.propsEver = false;

        this.listenTo(this.propertiesCollection, 'sync reset', this.onPropsLoaded);
        this.listenTo(this.s3Collection, 'sync reset', this.onAwsLoaded);
    },

    onPropsLoaded() {
        this.propsEver = true;
        this.checkBoth();
    },

    onAwsLoaded() {
        this.awsEver = true;
        this.checkBoth();
    },

    checkBoth() {
        if (this.propsEver && this.awsEver) {
            this.render();
        }
    },

    munge() {
        this.s3Collection.each((item) => {
            let matching = this.propertiesCollection.findWhere({ s3url: item.get('Key') });
            if (!matching) {
                // eslint-disable-next-line new-cap
                matching = new this.propertiesCollection.model({
                    s3url: item.get('Key'),
                    matching: true,
                    fileDate: item.get('LastModified'),
                });
                this.propertiesCollection.add(matching);
            } else {
                matching.set('matching', true);
                matching.set('fileDate', item.get('LastModified'));
            }
        });
    },

    render(event) {
        this.$el.empty();
        this.munge();
        this.propertiesCollection.forEach((item) => {
            const template = this.options.itemViewTemplate ?
                this.options.itemViewTemplate : itemViewTemplate;
            const view = new ItemView({
                model: item,
                template,
            });
            this.$el.append(view.render().el);
        });
        return this;
    },

});
