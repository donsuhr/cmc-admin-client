import Backbone from 'backbone';
import PendingUploadListItem from './pendingUploadListItem';

export default Backbone.View.extend({
    initialize(options) {
        this.listenTo(this.collection, 'sync reset', this.render);
        this.listenTo(this.collection, 'add remove', this.render);
        this.listenTo(this.collection, 'cancelItem', this.onCancelItem);
    },

    onCancelItem(model) {
        this.collection.remove(model);
    },

    render(event) {
        this.$el.empty();
        this.collection.each((item) => {
            const view = new PendingUploadListItem({
                model: item,
            });
            this.$el.append(view.render().el);
        });
        this.$el.off().on('cancelItem', this.onCancelItem);
        return this;
    },
});
