export default {
    renderDetails(model) {
        this.detailsView.updateModel(model);
        this.$el.html(this.detailsView.render().el);
    },

    onEdit(event, model) {
        const idField = this.listCollection.model.prototype.idAttribute;
        this.onRoute({
            action: 'display',
            id: model.get(idField),
        });
        this.router.updateDisplay(model.get(idField));
    },

    onBackClicked() {
        this.router.updateDisplay();
        this.onRoute({ action: 'show-home' });
    },

    onPendingDisplay() {
        this.onRoute({
            action: 'display',
            id: this.pendingId,
        });
    },

    onRoute(event) {
        switch (event.action) {
            case 'show-home':
            default:
                this.render();
                if (this.pending) {
                    this.loading.show();
                }
                break;
            case 'display':
                this.pageYOffset = window.pageYOffset;
                if (this.pending) {
                    this.pendingId = event.id;
                    this.stopListening(this.listCollection, 'sync reset', this.onPendingDisplay);
                    this.listenToOnce(this.listCollection, 'sync reset', this.onPendingDisplay);
                } else {
                    this.stopListening(this.listCollection, 'sync reset', this.onPendingDisplay);
                    const idField = this.listCollection.model.prototype.idAttribute;
                    const model = this.listCollection.findWhere({ [idField]: event.id });
                    this.renderDetails(model);
                }
                break;
        }
    },
};
