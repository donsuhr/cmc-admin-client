import ldExtend from 'lodash/extend';
import Backbone from 'backbone';
import 'blueimp-file-upload';
import config from 'config';
import LoadingAnimation from '../../loading-view';
import template from '../templates/appView.hbs';
import PropertiesCollection from '../collections/propertiesCollection';
import S3Collection from '../collections/s3Collection';
import PendingUploadsCollection from '../collections/pendingUploadsCollection';
import PendingUploadsView from './pendingUploadList';
import ListView from './listView';
import routerMethods from './appView-EditRouter';
import DetailsView from './itemDetailView';

const appView = {
    initialize(options) {
        this.options = options;
        this.label = options.label;
        this.router = options.router;
        this.listenTo(this.router, 'route', this.onRoute);
        this.template = options.appViewTemplate || template;
        this.loading = new LoadingAnimation({ $container: this.$el });

        this.s3Collection = new S3Collection(null, {
            site: options.site,
            group: options.group,
            apiFolder: options.apiFolder,
        });

        this.listCollection = new PropertiesCollection(null, {
            site: options.site,
            group: options.group,
            apiFolder: options.apiFolder,
        });

        this.collectionView = new ListView({
            s3Collection: this.s3Collection,
            propertiesCollection: this.listCollection,
            itemViewTemplate: options.itemViewTemplate,
        });

        this.pendingUploadsCollection = new PendingUploadsCollection(null, {
            site: options.site,
            group: options.group,
            ACL: options.ACL,
            apiFolder: options.apiFolder,
            AWS_S3_BUCKET_PREFIX: config.AWS_S3_BUCKET_PREFIX,
        });
        this.pendingUploadsView = new PendingUploadsView({
            collection: this.pendingUploadsCollection,
        });

        this.detailsView = new DetailsView({
            viewTemplate: options.detailsViewTemplate,
            editTemplate: options.detailsEditTemplate,
            label: options.label,
            site: options.site,
            group: options.group,
        });
        this.listenTo(this.detailsView, 'backClicked', this.onBackClicked);
        this.listenTo(this.detailsView, 'saveStart', this.onSyncStart);
        this.listenTo(this.detailsView, 'saveStop', this.onSyncEnd);
        this.listenTo(Backbone, 'itemUploaded', this.onItemUploaded);

        this.pending = true;
        this.onSyncStart();
        Promise.all([this.s3Collection.fetch(), this.listCollection.fetch()])
            .then((result) => {
                this.pending = false;
                this.listCollection.trigger('reset');
                this.onSyncEnd();
            });
    },

    events: {
        edit: 'onEdit',
    },

    attachFileUploader() {
        const that = this;

        this.$el.find('.upload__file-input').fileupload({
            type: 'PUT',
            dataType: 'XML',
            multipart: false,
            paramName: 'file',
            singleFileUploads: true,
            autoUpload: false,
            add(e, data) {
                that.pendingUploadsCollection.add(data);
            },
            beforeSend(xhr, data) {
                xhr.setRequestHeader('Content-Disposition', '');
            },
            progress(e, data) {
                data.bbModel.onProgress(e, data);
            },

        });
    },

    render() {
        this.$el.html(this.template({
            label: this.label,
            options: this.options,
            AWS_REGION: config.AWS_REGION,
            AWS_S3_BUCKET_PREFIX: config.AWS_S3_BUCKET_PREFIX,
        }));
        this.attachFileUploader();
        this.pendingUploadsView.setElement(this.$el.find('.upload-list'));
        this.collectionView.setElement(this.$el.find('.list-detail-app__item-list'));
        if (!this.pending) {
            this.collectionView.render();
        }
        if (typeof this.pageYOffset !== 'undefined') {
            window.scrollTo(0, this.pageYOffset);
        }
        return this;
    },

    onSyncStart() {
        this.loading.show();
    },

    onSyncEnd() {
        this.loading.hide();
    },

    onItemUploaded() {
        this.s3Collection.fetch();
    },
};

export default Backbone.View.extend(ldExtend({}, appView, routerMethods));
