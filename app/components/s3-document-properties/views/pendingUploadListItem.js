import $ from 'jquery';
import isObject from 'lodash/isObject';
import Backbone from 'backbone';
import template from '../templates/pendingUploadListItem.hbs';

export default Backbone.View.extend({
    tagName: 'li',

    className: 'upload-list-item',

    initialize(options) {
        this.listenTo(this.model, 'sync reset', this.render);
        this.listenTo(this.model, 'urlRenewed', this.render);
        this.listenTo(this.model, 'change:currentProgress', this.onProgress);
    },

    events: {
        'click .upload-item__cancel-button': 'onCancelClicked',
        'click .upload-item__start-button': 'onStartClicked',
        'click .upload-item__renew-button': 'onRenewClicked',
    },

    remove(...args) {
        clearInterval(this.expiresTimer);
        Backbone.View.prototype.remove.apply(this, args);
    },

    onStartClicked(event) {
        event.preventDefault();
        this.model.attributes
            .submit()
            .then(() => {
                this.onUploadSuccess();
            })
            .fail((e) => {
                const message = e.responseXML
                    ? $(e.responseXML).find('Error Message').text()
                    : 'error';
                const code = e.responseXML
                    ? $(e.responseXML).find('Error Code').text()
                    : '0';
                this.onUploadError({
                    message,
                    code,
                });
            });
        this.$el.find('button').prop('disabled', true);
        clearInterval(this.expiresTimer);
        this.$el.find('.upload-item__expires').text('');
    },

    onCancelClicked(event) {
        event.preventDefault();
        this.removeSelf();
    },

    onRenewClicked(event) {
        event.preventDefault();
        this.model.renewUrl();
    },

    onProgress(progressOrModel) {
        const progress = isObject(progressOrModel)
            ? progressOrModel.get('currentProgress')
            : progressOrModel;
        this.$el
            .find('.upload-item__progress')
            .attr('aria-valuenow', progress)
            .children()
            .first()
            .css('width', `${progress}%`);
    },

    onUploadSuccess() {
        this.$el
            .find('.upload-item__status')
            .removeClass('upload-item__status--error')
            .addClass('upload-item__status--success')
            .text('Upload Complete');
        Backbone.trigger('itemUploaded');
        setTimeout(() => {
            this.removeSelf();
        }, 2000);
    },

    onUploadError(err) {
        this.$el
            .find('.upload-item__status')
            .addClass('upload-item__status--error')
            .removeClass('upload-item__status--success')
            .text(`Error: ${err.message}`);
        this.$el
            .find('.upload-item__cancel-button')
            .prop('disabled', '')
            .text('Close');
    },

    removeSelf() {
        this.$el.find('button').prop('disabled', true);
        this.$el.addClass('upload-list-item--removing');
        setTimeout(() => {
            this.model.trigger('cancelItem', this.model);
        }, 1000);
    },

    onExpireInterval(event) {
        const expiresMs = this.model.get('expires') * 1000 - Date.now();
        let expiresS = Math.floor(expiresMs / 1000);
        if (expiresS <= 0) {
            this.onExpired();
            expiresS = 0;
            clearInterval(this.expiresTimer);
        }
        this.$el
            .find('.upload-item__expires')
            .text(`Url expires in ${expiresS} sec.`);
    },

    onExpired() {
        this.onUploadError({ message: 'URL Expired' });
        this.$el.find('.upload-item__start-button').prop('disabled', true);
        this.$el.find('.upload-item__renew-button').css('display', 'block');
    },

    render(event) {
        this.$el.html(
            template(this.model.toJSON(), {
                allowProtoPropertiesByDefault: true,
            }),
        );
        const bound = this.onExpireInterval.bind(this);
        if (this.expiresTimer) {
            clearInterval(this.expiresTimer);
        }
        this.expiresTimer = setInterval(bound, 500);
        const progress = this.model.get('currentProgress');
        if (progress === 100) {
            this.onProgress(progress);
            this.onUploadSuccess();
        } else {
            this.onProgress(progress);
        }
        return this;
    },
});
