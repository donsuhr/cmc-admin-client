import Backbone from 'backbone';
import config from 'config';
import { oneLineTrim } from 'common-tags';
import { checkStatus, parseJSON } from '../../fetchJsonHelpers';

export default Backbone.Model.extend({
    idAttribute: 's3url',

    defaults: {
        matching: false,
        priority: 0.5,
        display: false,
        createdAt: new Date(),
    },

    url() {
        const group = this.collection.group ? `${this.collection.group}/` : '';
        const site = this.collection.site ? `${this.collection.site}/` : '';
        const file = encodeURIComponent(this.get(this.idAttribute));
        return oneLineTrim`${config.domains.api}/${this.collection.apiFolder}/
            ${site}${group}${file}`;
    },

    parse(response) {
        response.createdAt = new Date(response.createdAt);
        response.updatedAt = new Date(response.updatedAt);
        return response;
    },

    patch(auth) {
        return fetch(this.url(), {
            method: 'PATCH',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                authorization: `Bearer ${auth.getToken()}`,
            },
            body: JSON.stringify(this.toJSON()),
        })
            .then(checkStatus)
            .then(parseJSON)
            .then((response) => {
                this.clear({ silent: true }).set(response, { silent: true });
                this.dirty = [];
                return response;
            });
    },
});
