import Backbone from 'backbone';
import config from 'config';
import queryString from 'query-string';
import { oneLineTrim } from 'common-tags';
import { auth } from '../../preconfigured-auth';
import { checkStatus, parseJSON } from '../../fetchJsonHelpers';

export default Backbone.Model.extend({
    defaults: {
        currentProgress: 0,
    },

    initialize() {
        this.set('url', '', { silent: true });
        this.set('bbModel', this);
        this.renewUrl();
    },

    onProgress(e, data) {
        const currentProgress = Math.floor((data.loaded / data.total) * 100);
        this.set('currentProgress', currentProgress);
    },

    renewUrl() {
        this.getSignedRequest(this.get('files')[0])
            .then((response) => {
                const qs = queryString.parse(response.signedRequest);
                this.set('url', response.signedRequest);
                this.set('signedRequest', response.signedRequest);
                this.set('link', response.url);
                this.set('expires', qs.Expires);
            })
            .then(() => {
                this.trigger('urlRenewed');
            });
    },
    getSignedRequest(file) {
        const group = this.collection.group ? `--${this.collection.group}` : '';
        const site = this.collection.site ? `--${this.collection.site}` : '';
        const url = oneLineTrim`${config.domains.api}/aws/sign-s3/
            ${this.collection.AWS_S3_BUCKET_PREFIX}
            ${this.collection.apiFolder}${site}${group}
            ?file-name=${file.name}&file-type=${file.type}
            &ACL=${this.collection.ACL}&action=putObject`;
        return fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                authorization: `Bearer ${auth.getToken()}`,
            },
        })
            .then(checkStatus)
            .then(parseJSON)
            .catch((err) => {
                // eslint-disable-next-line no-console
                console.log('unable to get signed url', err);
                throw err;
            });
    },

});
