import Backbone from 'backbone';

export default Backbone.Model.extend({
    defaults: {
        Key: undefined,
        LastModified: new Date(),
    },
    parse(response) {
        response.LastModified = new Date(response.LastModified);
        return response;
    },
});
