import Backbone from 'backbone';
import uploadItem from '../models/itemUploadModel';

export default Backbone.Collection.extend({
    model: uploadItem,
    initialize(models, options) {
        this.group = options.group;
        this.site = options.site;
        this.ACL = options.ACL || 'public-read';
        this.apiFolder = options.apiFolder;
        this.AWS_S3_BUCKET_PREFIX = options.AWS_S3_BUCKET_PREFIX;
    },
});
