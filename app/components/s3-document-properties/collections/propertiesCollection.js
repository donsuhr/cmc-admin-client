import Backbone from 'backbone';
import config from 'config';
import propertiesModel from '../models/propertiesModel';

export default Backbone.Collection.extend({
    model: propertiesModel,
    initialize(models, options) {
        this.group = options.group;
        this.site = options.site;
        this.apiFolder = options.apiFolder;
    },
    comparator(model) {
        const date = model.get('fileDate') || model.get('createdAt');
        return -date.getTime();
    },
    url() {
        const group = this.group ? `${this.group}/` : '';
        const site = this.site ? `${this.site}/` : '';
        return `${config.domains.api}/${this.apiFolder}/${site}${group}`;
    },
    parse(response) {
        return response.data;
    },
});
