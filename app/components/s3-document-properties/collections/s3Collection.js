import Backbone from 'backbone';
import config from 'config';
import { oneLineTrim } from 'common-tags';
import s3ItemModel from '../models/s3ItemModel';

export default Backbone.Collection.extend({
    model: s3ItemModel,
    initialize(models, options) {
        this.group = options.group;
        this.site = options.site;
        this.apiFolder = options.apiFolder;
        this.AWS_S3_BUCKET_PREFIX = config.AWS_S3_BUCKET_PREFIX;
    },
    comparator(model) {
        return -model.get('LastModified').getTime();
    },
    url() {
        const group = this.group ? `--${this.group}` : '';
        let site = this.site ? `--${this.site}` : '';
        if (this.apiFolder === 'general' && this.site === 'cmc') {
            site = '';
        }
        return oneLineTrim`${config.domains.api}/aws/buckets/
            ${this.AWS_S3_BUCKET_PREFIX}${this.apiFolder}${site}${group}`;
    },
    parse(response) {
        return response.Contents;
    },
});
