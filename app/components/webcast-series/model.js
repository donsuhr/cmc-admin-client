import ItemModel from '../generic-list-detail/itemModel';

export default ItemModel.extend({
    initialize(attributes, options) {
        this.set('site', options.site);
        this.set('group', options.group);
        this.set('query', options.query);
        // eslint-disable-next-line no-underscore-dangle, prefer-rest-params
        this.constructor.__super__.initialize.apply(this, arguments);
    },
    patch(auth) {
        try {
            this.set('date', new Date(this.get('date')).toISOString());
        } catch (e) {
            this.set('date', '');
        }
        // eslint-disable-next-line no-underscore-dangle, prefer-rest-params
        return this.constructor.__super__.patch.apply(this, arguments);
    },
});
