import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import config from 'config';

const appCache = {};

function createFirebaseApp(year = '2017') {
    if (Object.hasOwnProperty.call(appCache, year)) {
        return appCache[year];
    }
    const app = firebase.initializeApp(
        {
            apiKey: config.firebase[year].apiKey,
            databaseURL: config.firebase[year].databaseURL,
            authDomain: config.firebase[year].authDomain,
        },
        `ci-app-${year}`
    );
    const auth = app.auth();

    appCache[year] = {
        app,
        db: app.database(),
        auth,
        ns: firebase,
    };
    return appCache[year];
}

export { createFirebaseApp as default };
