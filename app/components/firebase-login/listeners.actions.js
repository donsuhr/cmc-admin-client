export const LISTENER_ADDED = 'LISTENER_ADDED';

export function addListener(ref) {
    return {
        type: LISTENER_ADDED,
        ref,
    };
}
