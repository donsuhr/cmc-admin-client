import createFirebaseApp from './firebase';

export const AUTH_INIT = 'AUTH_INIT';

export function onAuthInit(user) {
    return {
        type: AUTH_INIT,
        user,
    };
}

export const AUTH_INIT_REQUESTED = 'AUTH_INIT_REQUESTED';

export function onAuthInitRequested() {
    return {
        type: AUTH_INIT_REQUESTED,
    };
}

export const USER_ROOT_LOADED = 'USER_ROOT_LOADED';

function onUserRootLoaded(data) {
    return {
        type: USER_ROOT_LOADED,
        data,
    };
}

export const USER_ROOT_REQUESTED = 'USER_ROOT_REQUESTED';

function userRootRequested(userId) {
    return {
        type: USER_ROOT_REQUESTED,
        userId,
    };
}

export const REQUEST_USER_ROOT_ERROR = 'REQUEST_USER_ROOT_ERROR';

function onUserRootLoadError(error) {
    return {
        type: REQUEST_USER_ROOT_ERROR,
        error,
    };
}

export function listenToUserRoot(userId) {
    return (dispatch, getState) => {
        if (!listenToUserRoot.cached) {
            const { year } = getState().domAttributes;
            const firebaseDb = createFirebaseApp(year).db;
            const userRef = firebaseDb.ref(`users/${userId}`);
            dispatch(userRootRequested(userId));
            listenToUserRoot.cached = userRef.on('value',
                (data) => {
                    dispatch(onUserRootLoaded(data));
                },
                (error) => {
                    dispatch(onUserRootLoadError(error));
                });
        }
    };
}

function removeRootListener() {
    if (listenToUserRoot.cached) {
        listenToUserRoot.cached();
        listenToUserRoot.cached = null;
    }
}

export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS';

export function signOutSuccess() {
    removeRootListener();
    return {
        type: SIGN_OUT_SUCCESS,
    };
}

export const PASSIVE_SIGN_OUT = 'PASSIVE_SIGN_OUT';

export function passiveSignOut() {
    removeRootListener();
    return {
        type: PASSIVE_SIGN_OUT,
    };
}

export function listenToAuth() {
    return (dispatch, getState) => {
        if (listenToAuth.cached) {
            return listenToAuth.cached;
        }
        listenToAuth.cached = new Promise((resolve, reject) => {
            dispatch(onAuthInitRequested());
            const { year } = getState().domAttributes;
            const firebaseAuth = createFirebaseApp(year).auth;
            firebaseAuth.onAuthStateChanged((user) => {
                if (user) {
                    dispatch(onAuthInit(user));
                    resolve(user);
                } else {
                    dispatch(passiveSignOut());
                    delete listenToAuth.cached;
                }
            });
        });
        return listenToAuth.cached;
    };
}

export function signOut() {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseAuth = createFirebaseApp(year).auth;
        firebaseAuth.signOut()
            .then(() => dispatch(signOutSuccess()));
    };
}

