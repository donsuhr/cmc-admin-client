import React from 'react';
import { connect } from 'react-redux';
import { listenToAuth, listenToUserRoot, signOut } from './auth.actions';
import { listenToScheduleSupport, listenToUsedAvailability } from './schedule.support.actions';

import LoginForm from './LoginForm';

const container = React.createClass({
    propTypes: {
        listenToAuth: React.PropTypes.func,
        listenToUserRoot: React.PropTypes.func,
        signOut: React.PropTypes.func,
        user: React.PropTypes.shape({
            isAuth: React.PropTypes.bool,
            isAdmin: React.PropTypes.bool,
        }),
    },
    componentDidMount() {
        this.props.listenToAuth()
            .then((user) => {
                this.props.listenToUserRoot(user.uid);
            });
    },
    onSignOutClicked(event) {
        event.preventDefault();
        this.props.signOut();
    },
    render() {
        if (!this.props.user.isAuth) {
            return (
                <div>
                    <p>Login to Firebase [{this.props.year}] to delete availability rows.</p>
                    <LoginForm year={this.props.year} />
                </div>
            );
        }
        if (!this.props.user.isAdmin) {
            return (
                <div>
                    <p>Admin rights required.</p>
                    <button onClick={this.onSignOutClicked}>Log out of Firebase</button>
                </div>
            );
        }
        return (<button onClick={this.onSignOutClicked}>Log out of Firebase</button>);
    },
});

function mapStateToProps(state, ownProps) {
    return {
        schedule: state.schedule,
        user: state.user,
        year: state.domAttributes.year,
    };
}

const connected = connect(
    mapStateToProps,
    {
        listenToScheduleSupport,
        listenToUsedAvailability,
        listenToAuth,
        listenToUserRoot,
        signOut,
    }
)(container);

export default connected;
