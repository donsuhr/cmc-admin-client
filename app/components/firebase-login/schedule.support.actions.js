import createFirebaseApp from './firebase';

export const SCHEDULE_SUPPORT_LOADED = 'SCHEDULE_SUPPORT_LOADED';

function onScheduleSupportLoaded(data) {
    return {
        type: SCHEDULE_SUPPORT_LOADED,
        data,
    };
}

export const SCHEDULE_SUPPORT_REQUESTED = 'SCHEDULE_SUPPORT_REQUESTED';

function onScheduleSupportRequested() {
    return {
        type: SCHEDULE_SUPPORT_REQUESTED,
    };
}

export const SCHEDULE_SUPPORT_CHILD_REMOVED = 'SCHEDULE_SUPPORT_CHILD_REMOVED';

function onScheduleSupportChildRemoved(data) {
    return {
        type: SCHEDULE_SUPPORT_CHILD_REMOVED,
        data,
    };
}

export const SCHEDULE_SUPPORT_CHILD_ADDED = 'SCHEDULE_SUPPORT_CHILD_ADDED';

function onScheduleSupportChildAdded(data) {
    return {
        type: SCHEDULE_SUPPORT_CHILD_ADDED,
        data,
    };
}

export const SCHEDULE_USED_AVAILABILITY_LOADED = 'SCHEDULE_USED_AVAILABILITY_LOADED';

function onScheduleUsedAvailabilityLoaded(data) {
    return {
        type: SCHEDULE_USED_AVAILABILITY_LOADED,
        data,
    };
}

export const SCHEDULE_USED_AVAILABILITY_CHANGED = 'SCHEDULE_USED_AVAILABILITY_CHANGED';

function onScheduleUsedAvailabilityChanged(data) {
    return {
        type: SCHEDULE_USED_AVAILABILITY_CHANGED,
        data,
    };
}

export const SCHEDULE_USED_AVAILABILITY_REQUESTED = 'SCHEDULE_USED_AVAILABILITY_REQUESTED';

function onScheduleUsedAvailabilityRequested() {
    return {
        type: SCHEDULE_USED_AVAILABILITY_REQUESTED,
    };
}

function removeSupportFromSchedule(scheduleItem) {
    return (dispatch, getState) => {
        const userId = getState().user.info.uid;
        const { availabilityId } = scheduleItem.val;
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        firebaseDb.ref(`usedAvail/${userId}/${availabilityId}`).set(null);
        return scheduleItem.ref.remove();
    };
}

function listenToScheduleSupport(userId) {
    return (dispatch, getState) => {
        if (!listenToScheduleSupport.enabled) {
            const { year } = getState().domAttributes;
            const firebaseDb = createFirebaseApp(year).db;
            const scheduleRef = firebaseDb.ref(`users/${userId}/schedule/supportCouncil`);

            dispatch(onScheduleSupportRequested());
            scheduleRef.once('value', (data) => {
                dispatch(onScheduleSupportLoaded(data));
            });
            scheduleRef.on('child_added', (data) => {
                dispatch(onScheduleSupportChildAdded(data));
            });
            scheduleRef.on('child_removed', (data) => {
                dispatch(onScheduleSupportChildRemoved(data));
            });

            listenToScheduleSupport.enabled = true;
        }
    };
}

function listenToUsedAvailability() {
    return (dispatch, getState) => {
        if (!listenToUsedAvailability.enabled) {
            const { year } = getState().domAttributes;
            const firebaseDb = createFirebaseApp(year).db;
            const scheduleRef = firebaseDb.ref('usedAvail');
            dispatch(onScheduleUsedAvailabilityRequested());
            scheduleRef.once('value', (data) => {
                dispatch(onScheduleUsedAvailabilityLoaded(data));
            });
            scheduleRef.on('child_changed', (data) => {
                scheduleRef.once('value', (allData) => {
                    dispatch(onScheduleUsedAvailabilityChanged(allData));
                });
            });
            scheduleRef.on('child_added', (data) => {
                scheduleRef.once('value', (allData) => {
                    dispatch(onScheduleUsedAvailabilityChanged(allData));
                });
            });
            scheduleRef.on('child_removed', (data) => {
                scheduleRef.once('value', (allData) => {
                    dispatch(onScheduleUsedAvailabilityChanged(allData));
                });
            });
            listenToUsedAvailability.enabled = true;
        }
    };
}

export {
    listenToScheduleSupport,
    removeSupportFromSchedule,
    listenToUsedAvailability,
};
