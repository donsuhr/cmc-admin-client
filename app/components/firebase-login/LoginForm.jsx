import $ from 'jquery';
import React from 'react';
import { Form } from 'formsy-react';
import MyInput from './Input';
import createFirebaseApp from './firebase';

const LoginForm = React.createClass({
    getInitialState() {
        return {
            canSubmit: false,
            signinError: '',
            loading: false,
        };
    },
    componentDidMount() {
        $('.account-modal .modal-title').text('Login');
        $('.account-modal [name="email"]').focus();
    },
    submit(data) {
        this.setState({
            loading: true,
        });
        const firebaseAuth = createFirebaseApp(this.props.year).auth;
        firebaseAuth.signInWithEmailAndPassword(data.email, data.password)
            .then(() => {
                this.setState({
                    loading: false,
                });
            })
            .catch((error) => {
                const translate = {
                    'auth/user-not-found': 'Incorrect username or password',
                    'auth/user-disabled': 'Account disabled',
                    'auth/wrong-password': 'Incorrect username or password',
                    'auth/invalid-email': 'Invalid Email address',
                };
                this.setState({
                    signinError: translate[error.code] || 'Unknown Error',
                    loading: false,
                });
            });
    },
    enableButton() {
        this.setState({ canSubmit: true });
    },
    disableButton() {
        this.setState({ canSubmit: false });
    },
    render() {
        return (<Form
            className="cmc-form item-details-list firebase-login"
            onSubmit={this.submit}
            onValid={this.enableButton}
            onInvalid={this.disableButton}
        >
            <p className="signin-error">{this.state.signinError}</p>
            {this.state.loading && <p>Loading...</p>}
            <fieldset>
                <dl>
                    <MyInput
                        name="email"
                        title="Email"
                        validations="isEmail"
                        validationError="This is not a valid email"
                        required
                    />
                    <MyInput
                        name="password"
                        title="Password"
                        type="password"
                        required
                    />
                    <dt className="cmc-form__submit-row cmc-form__submit-row--login">&nbsp;</dt>
                    <dd>
                        <button
                            className="firebase-login__submit-button"
                            type="submit"
                            disabled={!this.state.canSubmit}
                        >
                            Login
                        </button>
                    </dd>
                </dl>
            </fieldset>
        </Form>);
    },
});

export default LoginForm;
