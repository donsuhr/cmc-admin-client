import { combineReducers } from 'redux';
import * as actions from './auth.actions';

const info = (state = {}, action) => {
    switch (action.type) {
        case actions.AUTH_INIT:
            return {
                displayName: action.user.displayName,
                email: action.user.email || action.user.providerData[0].email,
                uid: action.user.uid,
            };
        case actions.SIGN_OUT_SUCCESS:
        case actions.PASSIVE_SIGN_OUT:
            return {};
        default:
            return state;
    }
};

const isAuth = (state = false, action) => {
    switch (action.type) {
        case actions.AUTH_INIT:
            return true;
        case actions.SIGN_OUT_SUCCESS:
        case actions.PASSIVE_SIGN_OUT:
            return false;
        default:
            return state;
    }
};

const isAdmin = (state = false, action) => {
    switch (action.type) {
        case actions.USER_ROOT_LOADED:
            try {
                return !!action.data.val().admin;
            } catch (e) {
                return false;
            }
        case actions.REQUEST_USER_ROOT_ERROR:
        case actions.SIGN_OUT_SUCCESS:
        case actions.PASSIVE_SIGN_OUT:
            return false;
        default:
            return state;
    }
};


const loadingRoot = (state = false, action) => {
    switch (action.type) {
        case actions.USER_ROOT_REQUESTED:
            return true;
        case actions.USER_ROOT_LOADED:
        case actions.REQUEST_USER_ROOT_ERROR:
            return false;
        default:
            return state;
    }
};

const initting = (state = false, action) => {
    switch (action.type) {
        case actions.AUTH_INIT_REQUESTED:
            return true;
        case actions.AUTH_INIT:
        case actions.SIGN_OUT_SUCCESS:
        case actions.PASSIVE_SIGN_OUT:
            return false;
        default:
            return state;
    }
};


const user = combineReducers({
    info,
    isAuth,
    isAdmin,
    initting,
    loadingRoot,
});

export default user;
