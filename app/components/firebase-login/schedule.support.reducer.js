import { combineReducers } from 'redux';
import * as actions from './schedule.support.actions';

const userItems = (state = [], action) => {
    switch (action.type) {
        case actions.SCHEDULE_SUPPORT_CHILD_ADDED:
            return [
                ...state,
                {
                    ref: action.data.ref,
                    key: action.data.key,
                    val: action.data.val(),
                },
            ];
        case actions.SCHEDULE_SUPPORT_CHILD_REMOVED:
            const index = state.findIndex(x => x.key === action.data.key);
            if (index >= 0) {
                return [
                    ...state.slice(0, index),
                    ...state.slice(index + 1),
                ];
            }
            return state;
        default:
            return state;
    }
};

const usedAvailabilityId = (state = {}, action) => {
    switch (action.type) {
        case actions.SCHEDULE_USED_AVAILABILITY_CHANGED:
            const val = action.data.val();
            return Object.keys(val).reduce((acc, userId) => {
                const availabilityIds = val[userId];
                Object.keys(availabilityIds).forEach((id) => {
                    acc[id] = {
                        userId,
                        scheduleId: availabilityIds[id],
                    };
                });
                return acc;
            }, {});
        default:
            return state;
    }
};

const hasEverLoadedUsedAvailability = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_USED_AVAILABILITY_LOADED:
            return true;
        default:
            return state;
    }
};

const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_SUPPORT_LOADED:
            return true;
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_SUPPORT_REQUESTED:
            return true;
        case actions.SCHEDULE_SUPPORT_LOADED:
            return false;
        default:
            return state;
    }
};

function getScheduleItemById(state, analystId, availabilityId) {
    if (!state || !state.userItems) {
        return null;
    }
    const filtered = state.userItems.filter(x =>
        x.val.analystId === analystId && x.val.availabilityId === availabilityId);
    if (filtered.length < 1) {
        return null;
    }
    return filtered[0];
}

function getScheduleItemIsAvailable(state, availabilityId) {
    return !Object.keys(state.usedAvailabilityId).includes(availabilityId);
}

const supportConsultations = combineReducers({
    hasEverLoaded,
    hasEverLoadedUsedAvailability,
    fetching,
    userItems,
    usedAvailabilityId,
});

function getRegularBookedByAvailabilityId(state, availabilityId) {
    const item = state.usedAvailabilityId[availabilityId];
    if (item === true) {
        return null;
    }
    return item;
}

export {
    supportConsultations as default,
    getRegularBookedByAvailabilityId,
    getScheduleItemIsAvailable,
    getScheduleItemById,
};
