import { auth, profile } from 'cmc-auth';
import config from 'config';

auth.setup(config);
profile.setup(config);

export { auth, profile };
