import $ from 'jquery';
import _ from 'lodash';
import { Router } from 'director/build/director';
import formSerialize from 'form-serialize';
import { profile } from 'cmc-auth';
import 'parsleyjs';
import ProfileView from './view';

function clearGarlicLocalStorage() {
    _.each(_.keys(localStorage), (key) => {
        if (key.match(/^garlic/)) {
            localStorage.removeItem(key);
        }
    });
}

function createProfile(state) {
    const { auth } = state;
    if (!auth.isAuthenticated()) {
        window.location.href = '/';
        return;
    }

    profile.setup(state.config);
    const viewOptions = _.assign({}, { profile }, state);
    const view = ProfileView.create(viewOptions);
    // eslint-disable-next-line new-cap
    const router = new Router({
        '/': () => view.render('display'),
        '/edit': () => view.render('edit'),
    }).init('/');

    function addEventListeners($el) {
        $el
            .on('click', '.profile-form__submit-btn', (event) => {
                event.preventDefault();
                const form = $(event.target).closest('form').get(0);
                const obj = formSerialize(form, { hash: true, empty: true, disabled: true });
                const data = {
                    user_metadata: obj,
                };
                view.showLoading();

                const parsleyApi = $el.find('form').parsley();
                const valid = parsleyApi.validate();
                if (valid) {
                    profile.updateProfile(auth.getToken(), data)
                        .then((result) => {
                            clearGarlicLocalStorage();
                            router.setRoute('/');
                        })
                        .catch((error) => {
                            console.log('error', error); // eslint-disable-line no-console
                            view.showError(error);
                        });
                } else {
                    view.hideLoading();
                }
            })
            .on('click', '.profile-form__cancel-btn', (event) => {
                event.preventDefault();
                clearGarlicLocalStorage();
                router.setRoute('/');
            });
    }

    addEventListeners(state.$el);

    // eslint-disable-next-line consistent-return
    return {};
}

export default {
    create: function create(options) {
        const state = _.assign({}, options);
        return _.assign({}, createProfile(state));
    },
};
