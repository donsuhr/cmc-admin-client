import _ from 'lodash';
import $ from 'jquery';
import 'parsleyjs';
import 'garlicjs';
import templateDisplay from './display.hbs';
import templateForm from './form-wrapper.hbs';

function profileView(state) {
    const { $el, profile: model, auth: authApi } = state;
    let $loadingEl;

    return {
        render(mode) {
            if (mode === 'display') {
                $('.cmc-article-header__title').text('Profile');
                $('.cmc-article-header__bg-image')
                    .attr('src', '/images/banners/Banner_Profile.png');
            } else {
                $('.cmc-article-header__title').text('Edit Profile');
                $('.cmc-article-header__bg-image').attr('src', '/images/banners/Banner_Edit.png');
            }
            this.showLoading();
            const currentForm = $el.find('form');
            if (currentForm.length) {
                $(currentForm).parsley().destroy();
            }
            const template = mode === 'display' ? templateDisplay : templateForm;
            model.getProfile(authApi)
                .then((profileData) => {
                    $el.html(template({ profileData }));
                    const $newForm = $el.find('form');
                    if ($newForm.length) {
                        $newForm.parsley();
                        $newForm.garlic();
                        $newForm
                            .find('[name="country"] option')
                            .filter(
                                function countryFilter() {
                                    return $(this).text() === profileData.user_metadata.country;
                                }
                            )
                            .prop('selected', true);
                    }
                })
                .catch((error) => {
                    $el.html(template({ error }));
                });
        },
        showError(error) {
            $el.find('.profile-form__error')
                .text(`Error updating profile. Please try again later. (${error.message})`);
        },
        showLoading() {
            this.hideLoading();
            $loadingEl = $('<div class="loading">Loading...</div>');
            const x = ($el.innerWidth() / 2) - ($loadingEl.innerWidth() / 2);
            const y = Math.max(0, (($el.innerHeight() / 2) - ($loadingEl.innerHeight() / 2)));
            $loadingEl.css({ left: x, top: y });
            $el.append($loadingEl);
        },
        hideLoading() {
            if ($loadingEl && $loadingEl.length) {
                $loadingEl.remove();
            }
        },
    };
}

export default {
    create(options) {
        return _.assign({}, profileView(options));
    },
};
