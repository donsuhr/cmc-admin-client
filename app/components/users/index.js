import Backbone from 'backbone';
import AppView from '../generic-list-detail/views/appView';
import Router from '../generic-list-detail/router';

import detailsViewTemplate from './templates/detailsView.hbs';
import DetailsView from './views/detailsView';
import appViewTemplate from './templates/appView.hbs';
import emailLogListContainerTemplate from './templates/email-log-list-container.hbs';
import itemViewTemplate from './templates/itemView.hbs';
import ItemModel from './models/user';
import ListCollection from './collections/listCollection';

export default {
    init(options) {
        const router = new Router();

        new AppView({ // eslint-disable-line no-new
            el: options.el,
            router,
            listViewClassId: '.list-detail-app__item-list',
            apiFolder: 'users',
            appViewTemplate,
            itemViewTemplate,
            emailLogListContainerTemplate,
            DetailsView,
            detailsViewTemplate,
            ItemModel,
            ListCollection,
        });

        Backbone.history.start();
    },
};
