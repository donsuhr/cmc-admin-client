import $ from 'jquery';
import ldExtend from 'lodash/extend';
import cloneDeep from 'lodash/cloneDeep';
import config from 'config';
import { auth } from '../../preconfigured-auth';
import genericItemDetailView from '../../generic-list-detail/views/itemDetailView';

export default genericItemDetailView.extend({

    events() {
        return ldExtend({}, genericItemDetailView.prototype.events, {
            'click .user-detail__add-permission-button': 'onAddPermissionClicked',
            'click .user-detail__remove-permission-button': 'onRemovePermissionClicked',
            'click .user-detail__block-button': 'onToggleBlockClicked',
        });
    },

    onToggleBlockClicked(event) {
        event.preventDefault();
        const $target = $(event.target);
        this.pageYOffset = window.pageYOffset;
        this.model.set('blocked', !this.model.get('blocked'));
        const $loadingEl = $('<span class="item-details__status">Loading...</span>');
        $target.closest('dd').find('[class*="item-details__status"]').remove();
        this.$el.find('.user-detail__block-button')
            .css('display', 'none')
            .after($loadingEl);
        this.model
            .patch(auth)
            .then(result => this.render())
            .catch((error) => {
                // eslint-disable-next-line no-console
                console.log('request failed', error);
                $loadingEl.replaceWith(
                    '<span class="item-details__status--error">Error Updating User</span>'
                );
                this.$el.find('.user-detail__block-button')
                    .css('display', 'inline-block');
            });
    },

    onAddPermissionClicked(event) {
        event.preventDefault();
        this.pageYOffset = window.pageYOffset;
        const $target = $(event.target);
        const userID = this.model.get('user_id');
        const permission = $target.closest('[data-permission]').data('permission');
        const appMetadata = cloneDeep(this.model.get('app_metadata'));
        if (appMetadata.roles.indexOf(permission) === -1) {
            appMetadata.roles.push(permission);
            this.model.set({ app_metadata: appMetadata });
        }
        $target.closest('dd').find('[class*="item-details__status"]').remove();
        const $loadingEl = $('<span class="item-details__status">Loading...</span>');
        $target
            .css('display', 'none')
            .after($loadingEl);

        this.model
            .patch(auth)
            .then(result => this.render())
            .catch((error) => {
                // eslint-disable-next-line no-console
                console.log('request failed', error);
                $loadingEl.replaceWith(
                    '<span class="item-details__status--error">Error Updating User</span>'
                );
                $target.css('display', 'inline-block');
            })
            .then(() => {
                this.render();
                if (permission === 'product-center') {
                    return fetch(
                        `${config.domains.api}/users/product-center-access-granted`, {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                                authorization: `Bearer ${auth.getToken()}`,
                            },
                            body: JSON.stringify({
                                email: this.model.get('email'),
                                user_id: userID,
                                firstName: this.model.get('user_metadata').firstName,
                                lastName: this.model.get('user_metadata').lastName,
                                source: this.model.get('user_metadata').source,
                            }),
                        }
                    );
                }
                return Promise.resolve();
            });
    },

    onRemovePermissionClicked(event) {
        event.preventDefault();
        this.pageYOffset = window.pageYOffset;
        const $target = $(event.target);
        const permission = $target.closest('[data-permission]').data('permission');
        const appMetadata = cloneDeep(this.model.get('app_metadata'));
        if (appMetadata.roles.indexOf(permission) !== -1) {
            appMetadata.roles.splice(appMetadata.roles.indexOf(permission), 1);
            this.model.set({ app_metadata: appMetadata });
        }
        $target.closest('dd').find('[class*="item-details__status"]').remove();
        const $loadingEl = $('<span class="item-details__status">Loading...</span>');
        $target
            .css('display', 'none')
            .after($loadingEl);

        this.model
            .patch(auth)
            .then(result => this.render())
            .catch((error) => {
                // eslint-disable-next-line no-console
                console.log('request failed', error);
                $loadingEl.replaceWith(
                    '<span class="item-details__status--error">Error Updating User</span>'
                );
                $target.css('display', 'inline-block');
            });
    },

});
