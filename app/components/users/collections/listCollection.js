import GenericListCollection from '../../generic-list-detail/listCollection';

export default GenericListCollection.extend({
    parse(response) {
        return response.users;
    },
});

/**
 {
  "start": 0,
  "limit": 50,
  "length": 1,
  "users": [
    {
      "email": "test1@donsuhr.com",
      "email_verified": true,
      "user_id": "auth0|5661179e7dcac045264ac7da",
      "picture": "https://s.gravatar.com/avatar/7629a3ab3c2ec09de9bbdc1384696858?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fte.png",
      "nickname": "test1",
      "identities": [
        {
          "user_id": "5661179e7dcac045264ac7da",
          "provider": "auth0",
          "connection": "Username-Password-Authentication",
          "isSocial": false
        }
      ],
      "updated_at": "2016-06-10T01:01:43.490Z",
      "created_at": "2015-12-04T04:33:34.521Z",
      "name": "test1@donsuhr.com",
      "user_metadata": {
        "foo": "test10",
        "firstName": "Haviva",
        "lastName": "Wyatt123",
        "phone": "+462-90-6585045",
        "jobTitle": "job title",
        "institution": "institution",
        "country": "Angola"
      },
      "last_password_reset": "2016-06-08T06:11:09.282Z",
      "app_metadata": {
        "roles": [
          "user",
          "product-center",
          "admin"
        ]
      },
      "last_ip": "50.188.187.60",
      "last_login": "2016-06-10T01:01:43.490Z",
      "logins_count": 205
    }
  ],
  "total": 1
 }
 */
