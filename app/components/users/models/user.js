import ItemModel from '../../generic-list-detail/itemModel';

export default ItemModel.extend({
    idAttribute: 'user_id',
    onlyPatchChanged: true,
    defaults: {
        app_metadata: {
            roles: [],
        },
    },
});
