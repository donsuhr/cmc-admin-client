import React from 'react';
import { Link } from 'react-router';
import parser from 'ua-parser-js';
import Loading from './loading';
import dateFormat from '../../../html/helpers/dateFormat';
import SourceCode from './SourceCode.container';

const ErrorDetail = ({ fetching, label, error }) => {
    if (fetching || !error) {
        return (
            <Loading>loading...</Loading>
        );
    }
    const userAgent = error.navigator && error.navigator.userAgent ? error.navigator.userAgent : '';
    const ua = parser(userAgent);
    return (
        <div className="item-details">
            <h2 className="list-detail-app__title">{label} -- Item Details</h2>

            <h3>Item Info</h3>

            <dl className="item-details-list">
                <dt>Referrer</dt>
                <dd>{error.referrer}</dd>
                <dt>IP</dt>
                <dd>{error.ip}</dd>
                <dt>Location</dt>
                <dd><a href={error.locationHref}>{error.locationHref}</a></dd>
                <dt>UA String</dt>
                <dd>{userAgent}</dd>
                <dt>UA Browser</dt>
                <dd>{ua.browser.name} {ua.browser.version}</dd>
                <dt>UA Engine</dt>
                <dd>{ua.engine.name} {ua.engine.version}</dd>
                <dt>UA OS</dt>
                <dd>{ua.os.name} {ua.os.version}</dd>
                <dt>UA Device</dt>
                <dd>{ua.device.model} {ua.device.type} {ua.device.vendor}</dd>
                <dt>UA CPU</dt>
                <dd>{ua.cpu.architecture}</dd>
                <dt>Error Location</dt>
                <dd>
                    <SourceCode
                        label={`${error.source}:${error.line}:${error.col}`}
                        line={parseInt(error.line, 10)}
                        column={parseInt(error.col, 10)}
                        url={`${error.source}.map`}
                    />
                </dd>
                <dt>Error Name</dt>
                <dd>{error.error.name}</dd>
                <dt>Error Message</dt>
                <dd>{error.error.message}</dd>
                <dt>Error Stack</dt>
                <dd>
                    { error.error.stack.split('\n').map((x, i) => {
                        const results = /([^(]*)(?=\(?http)\(?([^)]*)/g.exec(x);
                        if (results) {
                            const atStr = results[1];
                            const urlLineCol = results[2];
                            const parts = urlLineCol.split(':');
                            const col = parseInt(parts.splice(-1), 10);
                            const line = parseInt(parts.splice(-1), 10);
                            const url = parts.join(':');
                            return (
                                // eslint-disable-next-line react/no-array-index-key
                                <div key={i} className="error-stack-row">
                                    <SourceCode
                                        label={`\u2022 ${atStr} (${url}:${line}:${col})`}
                                        line={line}
                                        column={col}
                                        url={`${url}.map`}
                                    />
                                </div>
                            );
                        }
                        return (
                            // eslint-disable-next-line react/no-array-index-key
                            <div key={i}>{x}</div>
                        );
                    })}
                </dd>

                <dt>Date Created</dt>
                <dd>{dateFormat(error.createdAt, 'MMM D, YYYY hh:mm A')}</dd>
            </dl>
            <br />
            <Link to="/" className="item-details__back-button">Back</Link>
        </div>
    );
};

ErrorDetail.propTypes = {
    label: React.PropTypes.string,
    error: React.PropTypes.shape({
        error: React.PropTypes.object,
    }),
    fetching: React.PropTypes.bool.isRequired,
};

export default ErrorDetail;
