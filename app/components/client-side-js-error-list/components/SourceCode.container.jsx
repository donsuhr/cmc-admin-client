import React from 'react';
import config from 'config';
import { SourceMapConsumer } from 'source-map';
import arrayBuffer from 'source-map/lib/mappings.wasm';
import SourceCode from './SourceCode';
import { auth } from '../../preconfigured-auth';
import { checkStatus } from '../../fetchJsonHelpers';

SourceMapConsumer.initialize({
    'lib/mappings.wasm': arrayBuffer,
});

const SourceCodeContainer = React.createClass({
    propTypes: {
        line: React.PropTypes.number,
        column: React.PropTypes.number,
        label: React.PropTypes.string,
        url: React.PropTypes.string,
    },

    getInitialState() {
        return {
            loading: false,
            loaded: false,
            location: {},
            src: '',
        };
    },

    onLoadClicked(event) {
        event.preventDefault();
        this.doLoad();
    },

    doLoad() {
        this.setState({ loading: true });
        return Promise.resolve()
            .then(() => fetch(
                `${
                    config.domains.api
                }/error-proxy/?file=${encodeURIComponent(this.props.url)}`,
                {
                    headers: {
                        authorization: `Bearer ${auth.getToken()}`,
                    },
                },
            ))
            .then(checkStatus)
            .then((response) => response.text())
            .then((text) => new SourceMapConsumer(text))
            .then((smc) => {
                // const smc = new SourceMapConsumer(text);
                const location = smc.originalPositionFor({
                    line: this.props.line,
                    column: this.props.column,
                });
                this.setState({
                    location,
                    src: smc.sourceContentFor(location.source),
                    loaded: true,
                    loading: false,
                });
            })
            .catch((error) => {
                // eslint-disable-next-line no-console
                console.log('error', error);
                this.setState({
                    loaded: error,
                    loading: false,
                });
            });
    },

    render() {
        return (
            <SourceCode
                {...this.state}
                label={this.props.label}
                onLoadClicked={this.onLoadClicked}
            />
        );
    },
});

export default SourceCodeContainer;
