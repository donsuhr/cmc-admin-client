import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import Loading from './loading';

const errorRowMapper = (item, index) => (
    <tr key={item._id}>
        <td>{item.error.message}</td>
        <td><a href={item.locationHref}>{item.locationHref}</a></td>
        <td>
            {item.source}:{item.line}:{item.col}
        </td>
        <td>
            {item.ip}
        </td>
        <td>
            {moment(item.createdAt).format('YYYY-MMM-DD')}
            <br />
            {moment(item.createdAt).format('hh:mm:ss a')}
        </td>
        <td>
            <Link
                className="list-detail-app__edit-button"
                to={`/error/${item._id}`}
            >
                Edit
            </Link>
        </td>
    </tr>
);

const ErrorTable = ({ data, fetching, label }) => {
    if (fetching) {
        return (
            <Loading>loading...</Loading>
        );
    }
    return (
        <div>
            <h2 className="list-detail-app__title">{label}</h2>

            <table className="list-detail-app__item-table list-detail-app__item-table--js-errors">
                <colgroup>
                    <col className="item-table-col--error" />
                    <col className="item-table-col--url" />
                    <col className="item-table-col--file" />
                    <col className="item-table-col--ip" />
                    <col className="item-table-col--date" />
                    <col className="item-table-col--actions" />
                </colgroup>
                <thead>
                    <tr>
                        <th>Error</th>
                        <th>URL</th>
                        <th>JS File</th>
                        <th>IP</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map(errorRowMapper)}
                </tbody>
            </table>
        </div>
    );
};

ErrorTable.propTypes = {
    label: React.PropTypes.string,
    data: React.PropTypes.arrayOf(
        React.PropTypes.object
    ),
    fetching: React.PropTypes.bool.isRequired,
};

export default ErrorTable;
