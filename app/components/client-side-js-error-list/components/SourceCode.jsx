import React from 'react';

const SourceCode = React.createClass({
    propTypes: {
        loaded: React.PropTypes.oneOfType([
            React.PropTypes.bool,
            React.PropTypes.object,
        ]),
        loading: React.PropTypes.bool,
        location: React.PropTypes.shape({
            line: React.PropTypes.number,
            column: React.PropTypes.number,
        }),
        src: React.PropTypes.string,
        onLoadClicked: React.PropTypes.func,
        label: React.PropTypes.string,
    },

    getInitialState() {
        return { visible: true };
    },

    componentDidUpdate() {
        window.SyntaxHighlighter.highlight();
    },

    toggle(event) {
        event.preventDefault();
        this.setState({ visible: !this.state.visible });
    },

    render() {
        const { loaded, loading, location, src, onLoadClicked, label } = this.props;
        if (loading) {
            return (<div>
                <span className="source-code-label-button">{label}</span>
                <p>Loading...</p>
            </div>);
        }
        if (!loaded) {
            return (<div>
                <button
                    className="source-code-label-button"
                    onClick={onLoadClicked}
                >
                    {label}
                </button>
            </div>);
        }
        if (Object.hasOwnProperty.call(loaded, 'message')) {
            return (<div>
                <button
                    className="source-code-label-button"
                    onClick={onLoadClicked}
                >
                    {label}
                </button>
                <p>error loading: {loaded.message}</p>
            </div>);
        }
        const visible = this.state.visible ? 'source-code-wrapper--visible' : 'source-code-wrapper';
        return (<div>
            <button className="source-code-label-button" onClick={this.toggle}>{label}</button>
            <div
                className={visible}
                ref={(node) => {
                    this.node = node;
                }}

            >
                <pre className={`brush: js; highlight: [${location.line}]`}>{src}</pre>
            </div>
        </div>);
    },
});

export default SourceCode;
