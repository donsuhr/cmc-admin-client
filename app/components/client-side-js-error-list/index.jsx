import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, hashHistory, applyRouterMiddleware } from 'react-router';
import useScroll from 'react-router-scroll/lib/useScroll';
import ErrorTableContainer from './containers/ErrorTableContainer';
import ErrorDetailContainer from './containers/ErrorDetailContainer';

import App from './containers/app-container';
import rootReducer from './reducers';

const store = createStore(rootReducer,
    applyMiddleware(thunk)
);

const router = props => (
    <Provider store={store}>
        <Router
            history={hashHistory}
            render={applyRouterMiddleware(useScroll())}
        >
            <Route path="/" component={App} label={props.label}>
                <IndexRoute component={ErrorTableContainer} />
                <Route path="/error/:errorId" component={ErrorDetailContainer} />
            </Route>
        </Router>
    </Provider>
);

router.propTypes = {
    label: React.PropTypes.node.isRequired,
};

export default router;
