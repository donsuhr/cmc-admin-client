import { combineReducers } from 'redux';
import { REQUEST_ERRORS, RECEIVE_ERRORS } from '../actions/error-list';

const errors = (state = [], action) => {
    switch (action.type) {
        case RECEIVE_ERRORS:
            return action.errors;
        default:
            return state;
    }
};

const fetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_ERRORS:
            return true;
        case RECEIVE_ERRORS:
            return false;
        default:
            return state;
    }
};

const errorList = combineReducers({
    errors,
    fetching,
});

export default errorList;

export function getErrorById(state, id) {
    return state['error-list'].errors.filter(x => x._id === id)[0];
}
