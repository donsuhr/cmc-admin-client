import { combineReducers } from 'redux';
import errorList from './error-list';

export default combineReducers({
    'error-list': errorList,
});
