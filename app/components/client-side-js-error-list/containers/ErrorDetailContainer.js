import { connect } from 'react-redux';
import ErrorDetail from '../components/ErrorDetail';
import { getErrorById } from '../reducers/error-list';

function mapStateToProps(state, ownProps) {
    return {
        error: getErrorById(state, ownProps.params.errorId),
        fetching: state['error-list'].fetching,
        label: ownProps.label,
    };
}
const ErrorTableContainer = connect(mapStateToProps)(ErrorDetail);

export default ErrorTableContainer;
