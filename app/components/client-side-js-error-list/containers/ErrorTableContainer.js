import { connect } from 'react-redux';
import ErrorTable from '../components/error-table';

function mapStateToProps(state, ownProps) {
    return {
        data: state['error-list'].errors,
        fetching: state['error-list'].fetching,
        label: ownProps.label,
    };
}

const ErrorTableContainer = connect(mapStateToProps)(ErrorTable);

export default ErrorTableContainer;
