import React from 'react';
import { connect } from 'react-redux';
import { fetchErrors } from '../actions/error-list';

const AppContainer = React.createClass({
    propTypes: {
        fetchErrors: React.PropTypes.func.isRequired,
        children: React.PropTypes.oneOfType([
            React.PropTypes.arrayOf(React.PropTypes.node),
            React.PropTypes.node,
        ]),
    },
    componentDidMount() {
        this.props.fetchErrors();
    },
    render() {
        return (
            <div>
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    },
});

function mapStateToProps(state, ownProps) {
    return {
        label: ownProps.route.label,
    };
}

export default connect(mapStateToProps, { fetchErrors })(AppContainer);
