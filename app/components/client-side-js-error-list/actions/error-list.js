import merge from 'lodash/merge';
import omitBy from 'lodash/omitBy';
import isNil from 'lodash/isNil';
import config from 'config';
import { auth } from '../../preconfigured-auth';
import { checkStatus, parseJSON } from '../../fetchJsonHelpers';

function normalizeResponse(response) {
    const nullRemoved = response.data.map(x => omitBy(x, isNil));
    response = nullRemoved.map(x => merge({}, {
        referrer: '',
        source: '',
        line: '',
        col: '',
        error: {
            message: '',
            stack: '',
        },
    }, x));
    return response;
}

export const REQUEST_ERRORS = 'REQUEST_ERRORS';
export function requestErrors() {
    return {
        type: REQUEST_ERRORS,
    };
}

export const RECEIVE_ERRORS = 'RECEIVE_ERRORS';
export function receiveErrors(errors) {
    return {
        type: RECEIVE_ERRORS,
        errors: normalizeResponse(errors),
    };
}

export function fetchErrors() {
    return (dispatch) => {
        dispatch(requestErrors());
        const endpoint = `${config.domains.api}/log-error/js/`;
        fetch(endpoint, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                authorization: `Bearer ${auth.getToken()}`,
            },
        })
            .then(checkStatus)
            .then(parseJSON)
            .then(result => dispatch(receiveErrors(result)));
    };
}
