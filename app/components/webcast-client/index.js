import Backbone from 'backbone';

import AppView from '../generic-list-detail/views/appView';
import Router from '../generic-list-detail/router';

import detailsViewTemplate from './templates/detailsView.hbs';
import appViewTemplate from './templates/appView.hbs';
import detailsEditTemplate from './templates/itemDetailEdit.hbs';
import itemViewTemplate from './templates/itemView.hbs';
import Model from '../webcast-series/model';

export default {
    init(options) {
        const router = new Router();

        new AppView({ // eslint-disable-line no-new
            el: options.el,
            router,
            listViewClassId: '.list-detail-app__item-list',
            apiFolder: options.apiFolder,
            site: options.site,
            group: options.group,
            label: options.label,
            appViewTemplate,
            itemViewTemplate,
            detailsViewTemplate,
            detailsEditTemplate,
            ItemModel: Model,
            query: options.query,
        });
        Backbone.history.start();
    },
};
