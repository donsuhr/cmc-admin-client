import Backbone from 'backbone';
import AppView from '../s3-document-properties/views/appView';
import Router from '../generic-list-detail/router';

import detailsViewTemplate from './templates/itemDetailView.hbs';
import detailsEditTemplate from './templates/itemDetailEdit.hbs';
import appViewTemplate from './templates/appView.hbs';
import itemViewTemplate from './templates/listItemView.hbs';


export default {
    init(options) {
        const router = new Router();

        new AppView({ // eslint-disable-line no-new
            el: options.el,
            router,
            site: options.site,
            group: options.group,
            label: options.label,
            ACL: options.ACL,
            apiFolder: options.apiFolder,
            appViewTemplate,
            itemViewTemplate,
            detailsViewTemplate,
            detailsEditTemplate,
        });
        Backbone.history.start();
    },
};

