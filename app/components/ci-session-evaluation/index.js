import Backbone from 'backbone';

import AppView from '../generic-list-detail/views/appView';
import Router from '../generic-list-detail/router';

import detailsViewTemplate from './templates/detailsView.hbs';
import appViewTemplate from './templates/appView.hbs';
import itemViewTemplate from './templates/itemView.hbs';

export default {
    init(options) {
        const router = new Router();

        new AppView({ // eslint-disable-line no-new
            el: options.el,
            router,
            apiFolder: options.apiFolder,
            site: options.site,
            group: options.group,
            query: options.query,
            label: options.label,
            listViewClassId: '.list-detail-app__item-list',
            appViewTemplate,
            itemViewTemplate,
            detailsViewTemplate,
        });
        Backbone.history.start();
    },
};
