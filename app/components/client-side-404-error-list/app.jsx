import React from 'react';
import config from 'config';
import Loading from './loading';
import ErrorTable from './error-table';
import { checkStatus, parseJSON } from '../fetchJsonHelpers';
import { auth } from '../preconfigured-auth';

function getData() {
    const endpoint = `${config.domains.api}/log-error/404/`;
    return fetch(endpoint, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            authorization: `Bearer ${auth.getToken()}`,
        },
    })
        .then(checkStatus)
        .then(parseJSON);
}

const App = React.createClass({
    propTypes: {
        label: React.PropTypes.string.isRequired,
    },
    getInitialState() {
        return {
            fetching: true,
            data: [],
        };
    },
    componentDidMount() {
        getData().then(result => this.setState({ fetching: false, data: result.data }));
    },
    render() {
        const child = this.state.fetching ?
            (<Loading>loading...</Loading>) :
            (<ErrorTable data={this.state.data} />);
        return (
            <section className="main-app app-container--s3-list">
                <h2
                    className="list-detail-app__title list-detail-app__title--s3-list"
                >
                    {this.props.label}
                </h2>
                {child}
            </section>
        );
    },
});

export default App;
