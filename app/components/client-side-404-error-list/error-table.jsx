import React from 'react';
import moment from 'moment';

const ErrorTable = ({ data }) => (
    <table className="list-detail-app__item-table list-detail-app__item-table--404-errors">
        <colgroup>
            <col className="item-table-col--url" />
            <col className="item-table-col--referrer" />
            <col className="item-table-col--ip" />
            <col className="item-table-col--date" />
        </colgroup>
        <thead>
            <tr>
                <th>Url</th>
                <th>Referrer</th>
                <th>IP</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            {data.map((item, index) => (
                <tr key={item._id}>
                    <td><a href={item.locationHref}>{item.locationHref}</a></td>
                    <td><a href={item.referrer}>{item.referrer}</a></td>
                    <td>{item.ip}</td>
                    <td>
                        {moment(item.createdAt).format('YYYY-MMM-DD')}
                        <br />
                        {moment(item.createdAt).format('hh:mm:ss a')}
                    </td>
                </tr>
            ))}
        </tbody>
    </table>
);

ErrorTable.propTypes = {
    data: React.PropTypes.arrayOf(React.PropTypes.object),
};

export default ErrorTable;
