import ItemModel from '../generic-list-detail/itemModel';

export default ItemModel.extend({
    idAttribute: 'id',
});
