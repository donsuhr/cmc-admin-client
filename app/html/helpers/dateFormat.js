const moment = require('moment');

module.exports = function dateFormat(date, format, options) {
    if (!date) {
        return '';
    }
    return moment(date).format(format);
};
