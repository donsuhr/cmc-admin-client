module.exports = function json(value, safeVal, options) {
    return JSON.stringify(value, null, 2);
};
