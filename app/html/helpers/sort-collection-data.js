/* eslint
  "comma-dangle": [
    "error", {
        "arrays": "always-multiline",
        "objects": "always-multiline",
        "imports": "always-multiline",
        "exports": "always-multiline",
        "functions": "never"
    }]
*/

'use strict'; // eslint-disable-line strict, lines-around-directive

const set = require('lodash/set');
const get = require('lodash/get');
const head = require('lodash/head');
const assign = require('lodash/assign');
const ldTemplate = require('lodash/template');
const orderBy = require('lodash/orderBy');
const each = require('lodash/each');
const Handlebars = require('handlebars');

const sortCollectionDataCache = {};

function mergeAndSortFiles(obj, maxDepth, currentDepth) {
    // merge children into files and sort
    currentDepth = currentDepth || 1;
    const result = orderBy(obj, ['menu-priority', 'label'], ['desc', 'asc']);
    result.forEach((node) => {
        if (node.children) {
            // move it to files
            if (currentDepth < maxDepth) {
                each(node.children, (child) => {
                    node.files.push(child);
                });
                node.children = mergeAndSortFiles(
                    node.children,
                    maxDepth,
                    currentDepth + 1
                );
            }
        }
        node.files = orderBy(
            node.files,
            ['menu-priority', 'label'],
            ['desc', 'asc']
        );
        node.files.forEach((x) => {
            x.currentDepth = currentDepth;
        });
        if (currentDepth >= maxDepth) {
            node.files = null;
        }
    });
    return result;
}

// eslint-disable-next-line max-len
module.exports = function sortCollectionData(currentCollection,
    startFrom,
    maxLevels,
    partial,
    options) {
    const cacheKey = `${currentCollection}-${startFrom}-${maxLevels}-${partial}`;
    if (Object.hasOwnProperty.call(sortCollectionDataCache, cacheKey)) {
        return sortCollectionDataCache[cacheKey];
    }
    maxLevels = maxLevels || 10;
    const context = this;
    const { data: { root: { collections } } } = options;
    let hierarchy = {};
    let rootLink;
    let rootKey;
    const origStartFrom = startFrom;

    const fileMap = function fileMap(file) {
        const href = (function getHref() {
            const metaDataLinkTo = file['link-to'];
            if (metaDataLinkTo) {
                const template = ldTemplate(metaDataLinkTo);
                return template(options.data.root);
            }
            const prefix = options.data.root.leftNavUrlPrefix
                ? options.data.root.leftNavUrlPrefix
                : '';
            return `${prefix}/${file.path}/`;
        }());
        const label = file.label || file.title;
        const shortName = label.toLowerCase()
            .replace(/[^\w]/g, '');
        return {
            label,
            href,
            'menu-priority': file['menu-priority'],
            originalFilename: file.originalFilename,
            collectionName: file.collection[0],
            isActiveCollection:
            context.collection &&
            file.collection[0] === context.collection[0],
            isActiveFile: file.originalFilename === context.originalFilename,
            children: file.children,
            'show-in-quick-launch': file['show-in-quick-launch'],
            'show-in-left-nav': file['show-in-left-nav'],
            shortName,
        };
    };

    // en-us root files have no collection (search.html, 404.html)
    if (currentCollection && startFrom === '--parent--') {
        const tokens = currentCollection.split('==>');
        tokens.splice(-1, 1);
        startFrom = tokens.join('==>');
    }

    if (currentCollection && startFrom && startFrom.indexOf('==>*') !== -1) {
        const tokens = startFrom.split('==>');
        tokens.pop();
        startFrom = tokens.join('==>');
    }

    Object.keys(collections).forEach((collectionName) => {
        // two passes. cant sort the children until we know what they are
        const currentCollectionFiles = collections[collectionName];
        const indexFile = head(
            currentCollectionFiles.filter(file =>
                file.originalFilename.endsWith('index.html'))
        );

        if (currentCollectionFiles.length > 0) {
            if (typeof indexFile === 'undefined') {
                throw new Error(
                    `index.html file missing for collection ${collectionName}`
                );
            }

            const files = currentCollectionFiles
                .filter(file => !file.originalFilename.endsWith('index.html'))
                .map(fileMap);

            const filesAndLinks = files.concat(
                get(currentCollectionFiles, 'metadata.links', [])
            );

            const folder = assign(
                {},
                {
                    index: indexFile,
                    files: filesAndLinks,
                },
                fileMap(indexFile)
            );

            const key = collectionName.split('==>').join('.children.');
            set(hierarchy, key, folder);

            if (collectionName === startFrom) {
                rootLink = get(hierarchy, key);
                rootKey = collectionName;
            }
        }
    });

    if (rootLink) {
        hierarchy = { [rootKey]: rootLink };
        if (origStartFrom.indexOf('==>*') !== -1) {
            maxLevels += 1;
        }
    }

    const orderedHierarchy = mergeAndSortFiles(hierarchy, maxLevels);
    const hbPartial = Handlebars.partials[partial];
    const template =
        typeof hbPartial === 'string'
            ? Handlebars.compile(hbPartial)
            : hbPartial;

    const result = new Handlebars.SafeString(
        template({
            files:
                origStartFrom && origStartFrom.indexOf('==>*') !== -1
                    ? orderedHierarchy[0].files
                    : orderedHierarchy,
            class: 'root static',
            config: options.data.root.config,
            leftNavUrlPrefix: options.data.root.leftNavUrlPrefix,
        })
    );

    if (process.env.NODE_ENV !== 'production') {
        sortCollectionDataCache[cacheKey] = result;
    }

    return result;
};
