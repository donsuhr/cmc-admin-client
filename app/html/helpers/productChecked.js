module.exports = function productChecked(productsString, toMatch, options) {
    const products = typeof productsString === 'string' ?
        productsString.split(',') : productsString || [];
    const trimmed = products.map(x => x.trim());
    const isChecked = trimmed.includes(toMatch);
    return isChecked ? options.fn(this) : options.inverse(this);
};
