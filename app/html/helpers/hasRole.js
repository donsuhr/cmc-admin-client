module.exports = function hasRole(role, options) {
    const fnTrue = options.fn;
    const fnFalse = options.inverse;
    const doesHaveRole = (
        this.app_metadata &&
        this.app_metadata.roles &&
        this.app_metadata.roles.indexOf(role) !== -1
    );
    return doesHaveRole ? fnTrue(this) : fnFalse(this);
};
