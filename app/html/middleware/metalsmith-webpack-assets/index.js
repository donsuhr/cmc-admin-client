'use strict';

const fs = require('fs');
const path = require('path');

const webpackAssets = function webpackAssets(config) {
    return (files, metalsmith, done) => {
        const hrstart = process.hrtime();
        const data = fs.readFileSync(
            path.resolve(process.cwd(), 'webpack-manifest.json'),
            'utf8',
        );
        const assets = JSON.parse(data);

        Object.keys(files).forEach((filename) => {
            const file = files[filename];
            const additonalScripts = file['additional-scripts'] || [];
            const pageScripts = [...assets['cmc-admin'].js];

            additonalScripts.forEach((script) => {
                const assetsKey = path.basename(script.src, '.js');
                if (assets[assetsKey]) {
                    pageScripts.push(...assets[assetsKey].js);
                }
            });
            file.scripts = [...new Set(pageScripts)];
        });
        const hrend = process.hrtime(hrstart);
        // eslint-disable-next-line no-console
        console.info(
            'Metalsmith - webpackAssets \t\tExecution time: %ds %dms',
            hrend[0],
            hrend[1] / 1000000,
        );
        setImmediate(done);
    };
};

module.exports = webpackAssets;
