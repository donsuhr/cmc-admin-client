'use strict';

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const AssetsPlugin = require('assets-webpack-plugin');
const glob = require('glob');

const production = process.env.NODE_ENV === 'production';
const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

const entry = () => {
    const cmcSite = [path.join(__dirname, 'app/scripts/index.js')];
    if (!production) {
        cmcSite.unshift(
            'webpack/hot/dev-server',
            'webpack-hot-middleware/client?reload=true',
        );
    }

    return glob
        .sync('./app/scripts/pages/!(*.e2e).js', { absolute: true })
        .reduce(
            (acc, x) => {
                const ext = path.extname(x);
                acc[path.basename(x, ext)] = x;
                return acc;
            },
            {
                'cmc-admin': cmcSite,
            },
        );
};

const plugins = [
    new AssetsPlugin({
        entrypoints: false,
        prettyPrint: true,
        filename: 'webpack-assets.json',
    }),
    new AssetsPlugin({
        entrypoints: true,
        prettyPrint: true,
        filename: 'webpack-manifest.json',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery', // garlic, headroom, bootstrap
        jQuery: 'jquery', // bootstrap parsley superfish
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        },
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
];

if (!production) {
    plugins.push(new webpack.HotModuleReplacementPlugin());

    const manifestFile = path.resolve(process.cwd(), 'dll-manifest.json');
    let manifestJson;
    try {
        manifestJson = JSON.parse(fs.readFileSync(manifestFile));
    } catch (e) {
        manifestJson = false;
    }
    if (manifestJson) {
        plugins.push(
            new webpack.DllReferencePlugin({
                context: '.',
                manifest: manifestJson,
            }),
        );
    }
}

module.exports = {
    stats: 'errors-only',
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    plugins,
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
        },
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config$: path.join(__dirname, 'config.js'),
            'jquery.ui.widget':
                'blueimp-file-upload/js/vendor/jquery.ui.widget',
            'jquery-ui/widget':
                'blueimp-file-upload/js/vendor/jquery.ui.widget',
            // underscore: 'lodash/core',
            underscore: path.join(modulesDir, 'underscore/underscore'),
            handlebars: 'handlebars/runtime',
        },
        extensions: ['.js', '.jsx'],
    },
    output: {
        path: path.join(__dirname, 'dist/'),
        filename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        chunkFilename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        publicPath: '/',
    },

    module: {
        rules: [
            {
                test: /\.wasm$/,
                type: 'javascript/auto',
                loader: 'arraybuffer-loader', // installed via npm
            },
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/config.js'),
                    fs.realpathSync(`${modulesDir}/cmc-auth`),
                ],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        query: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            helperDirs: [`${__dirname}/app/html/helpers`],
                            partialDirs: [`${__dirname}/app/html/partials`],
                        },
                    },
                ],
                include: [
                    path.join(__dirname, '/app/'),
                    fs.realpathSync(`${modulesDir}/cmc-auth`),
                ],
            },
            {
                test: /\.html$/,
                loader: 'underscore-template-loader',
                query: {
                    variable: 'data',
                },
            },
            {
                test: /\.scss/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
            {
                test: /\.png$/,
                use: [{ loader: 'url-loader', query: { limit: 100000 } }],
            },
            { test: /\.jpg$/, loader: 'file-loader' },
        ],
    },
};
