'use strict';

const path = require('path');
const timeGrunt = require('time-grunt');
const loadGruntConfig = require('load-grunt-config');

const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

const config = {
    app: 'app',
    dist: 'dist',
    sassOutput: '.tmp/sass-out/',
    modulesDir,
    livereloadPort: 35731,
};

module.exports = function gruntfile(grunt) {
    if (process.env.NODE_ENV !== 'production') {
        timeGrunt(grunt);
    }
    loadGruntConfig(grunt, {
        configPath: path.join(process.cwd(), '/build/grunt'),
        data: config,
        jitGrunt: {
            staticMappings: {
                scsslint: 'grunt-scss-lint',
                replace: 'grunt-text-replace',
            },
        },
    });

    grunt.registerTask('serve', (target) => {
        if (target === 'dist') {
            grunt.task.run(['build', 'connect:dist:keepalive']);
        } else {
            grunt.task.run([
                'watchPrep',
                'connect:livereload',
                'concurrent:serve',
            ]);
        }
    });

    grunt.registerTask('ssoIframe', [
        'copy:ssoIframe',
        'replace:ssoIframe',
        'exec:build-js--sso-iframe',
    ]);

    grunt.registerTask('build', [
        'clean',
        'exec:write-config',
        'ssoIframe',
        'concurrent:dist',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'exec:postcss-min',
        'filerev',
        'usemin',
        'htmlmin',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);

    grunt.registerTask('watchPrep', [
        'clean',
        'exec:write-config',
        'ssoIframe',
        'exec:build-js',
        'styles',
        'exec:metalsmith-dist',
    ]);

    grunt.registerTask('styles', [
        'exec:sass',
        'exec:postcss',
        'copy:dist',
        'filerev',
    ]);

    grunt.registerTask('netlify', ['build']);

    grunt.registerTask('dist', ['build']);
};
