'use strict';

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

const production = process.env.NODE_ENV === 'production';

const entry = {
    'page--sso-iframe': [path.resolve('.', 'app/scripts/sso-iframe.js')],
};

const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

const plugins = [new webpack.NoEmitOnErrorsPlugin()];

module.exports = {
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config$: path.resolve('.', 'config.js'),
        },
        extensions: ['.js', '.jsx'],
    },
    output: {
        path: path.resolve('.', 'dist'),
        filename: production ? 'scripts/[name].js' : 'scripts/[name].js',
    },
    plugins,
    module: {
        rules: [
            {
                test: /\.js?$/,
                use: ['babel-loader'],
                include: [
                    path.resolve('.', 'app'),
                    path.resolve('.', 'config.js'),
                    fs.realpathSync(`${modulesDir}/cmc-auth`),
                ],
            },
        ],
    },
};
