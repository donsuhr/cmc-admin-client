FROM node:6.11.1-alpine
ENV DOCKERIZED=true
RUN \
    npm install pm2 -g --silent && \
    npm cache clean
EXPOSE 80 443 43554
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
RUN \
	mkdir -p /aws && \
	apk -Uuv add git openssh-client && \
	apk --purge -v del py-pip && \
	rm /var/cache/apk/*
RUN \
  mkdir -p /root/.ssh && chmod 0700 /root/.ssh && \
  ssh-keyscan bitbucket.com > /root/.ssh/known_hosts && \
  echo -e "Host bitbucket.org\n"\
            " IdentityFile /root/.ssh/deploy_key\n"\
            " IdentitiesOnly yes\n"\
            " UserKnownHostsFile=/dev/null\n"\
            " StrictHostKeyChecking no"\
            > /root/.ssh/config
COPY deploy_key /root/.ssh/
RUN \
  chmod 0600 /root/.ssh/deploy_key
COPY build/postinstall.js /usr/src/app/build/
COPY package.json yarn.lock /usr/src/app/
RUN \
    yarn install && \
    yarn cache clean && \
    rm -f /root/.ssh/deploy_key
COPY . /usr/src/app/
RUN \
    yarn dist
CMD [ "pm2-docker", "start", "--auto-exit", "pm2.yml" ]
