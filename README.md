# cmc-admin-client

metalsmith and js processors for the admin subdomain

```
# when running under cmc-democenter-server proxy
# make sure this package is symlinked into cmc-democenter-server, not installed
npm run bs
```


## update the ssoIframe
```
grunt ssoIframe
```

## dist
```
npm run dist
```

## update the cmc packages
```
./scripts/update-npm-cmc-peer.sh
```

