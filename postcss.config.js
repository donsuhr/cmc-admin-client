'use strict';

const autoprefixer = require('autoprefixer');
const inlineSvg = require('postcss-inline-svg');
const replace = require('postcss-replace');
const cssnano = require('cssnano');
const { argv } = require('yargs');

const plugins = [
    inlineSvg({ paths: ['./app/sprite-src-svg', './app'] }),
    replace({
        pattern: /(images\/ui-icons_)/,
        data: { 'images/ui-icons_': '/images/jquery-ui/ui-icons_' },
    }),
    autoprefixer({ grid: 'autoplace' }),
];

if (argv.minify) {
    plugins.push(cssnano());
}

module.exports = {
    plugins,
};
