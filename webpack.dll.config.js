'use strict';

const webpack = require('webpack');
const path = require('path');

module.exports = {
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    entry: {
        dll: [
            'garlicjs',
            'auth0-js',
            'backbone',
            'blueimp-file-upload',
            'classnames',
            'common-tags',
            'director',
            'firebase',
            'form-serialize',
            'formsy-react',
            'handlebars/runtime',
            'jquery',
            'jquery-ui',
            'js-cookie',
            'lodash/assignIn',
            'lodash/clone',
            'lodash/debounce',
            'lodash/each',
            'lodash/filter',
            'lodash/first',
            'lodash/forOwn',
            'lodash/get',
            'lodash/head',
            'lodash/intersection',
            'lodash/isArray',
            'lodash/isDate',
            'lodash/isEmpty',
            'lodash/isNil',
            'lodash/kebabCase',
            'lodash/keys',
            'lodash/map',
            'lodash/omitBy',
            'lodash/set',
            'lodash/sortBy',
            'lodash/template',
            'lodash/uniq',
            'moment',
            'moment-timezone',
            'parsleyjs',
            'pubsub-js',
            'query-string',
            'react',
            'react-data-grid',
            'react-data-grid-addons',
            'react-dom',
            'react-redux',
            'react-router',
            'react-router-redux',
            'react-router-scroll',
            'react-select',
            'redux',
            'redux-form',
            'redux-thunk',
            'source-map',
            'ua-parser-js',
            'underscore',
        ],
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname,
        // The name of the global variable which the library's
        // require() function will be assigned to
        library: '[name]_lib',
    },
    resolve: {
        modules: ['node_modules'],
        alias: {
            'jquery.ui.widget':
                'blueimp-file-upload/js/vendor/jquery.ui.widget',
            'jquery-ui/widget':
                'blueimp-file-upload/js/vendor/jquery.ui.widget',
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore',
            ),
            handlebars: 'handlebars/runtime',
        },
    },

    plugins: [
        new webpack.ProvidePlugin({
            'window.jQuery': 'jquery', // garlic, headroom, bootstrap
            jQuery: 'jquery', // bootstrap parsley superfish
        }),
        new webpack.DllPlugin({
            // The path to the manifest file which maps between
            // modules included in a bundle and the internal IDs
            // within that bundle
            path: '[name]-manifest.json',
            // The name of the global variable which the library's
            // require function has been assigned to. This must match the
            // output.library option above
            name: '[name]_lib',
        }),
    ],
};
