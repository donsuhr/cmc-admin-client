/* eslint-disable no-console */
// eslint-disable-next-line import/no-extraneous-dependencies

'use strict';

const chokidar = require('chokidar');
const path = require('path');
const { spawn } = require('child_process');

let eslintChild;
let gruntChild;
const eslint = path.resolve(__dirname, '../node_modules/.bin/eslint');
const grunt = path.resolve(__dirname, '../node_modules/.bin/grunt');

chokidar
    .watch([
        './**/*.js',
        './**/*.jsx',
        '!./app/scripts/vendor/**',
        '!./node_modules/**',
        '!./dist/**',
        '!./.tmp/**',
    ])
    .on('change', (filePath) => {
        console.log('chokidar file change: ', filePath);

        const onClose = (code) => {
            console.log(
                `eslint pid ${eslintChild.pid} complete. Exit Code: ${code}`,
            );
            eslintChild.removeAllListeners();
            eslintChild = null;
        };

        if (eslintChild) {
            console.log(`kill eslint pid ${eslintChild.pid}`);
            eslintChild.removeAllListeners();
            eslintChild.kill();
        }

        eslintChild = spawn(eslint, ['.'], { stdio: 'inherit' });
        eslintChild.on('close', onClose);
        console.log('start eslint pid', eslintChild.pid);
    });

chokidar.watch(['./app/**/*.scss']).on('change', (filePath) => {
    console.log('chokidar file change: ', filePath);

    const onClose = (code) => {
        console.log(`grunt pid ${gruntChild.pid} complete. Exit Code: ${code}`);
        gruntChild.removeAllListeners();
        gruntChild = null;
    };

    if (gruntChild) {
        console.log(`kill grunt pid ${gruntChild.pid}`);
        gruntChild.removeAllListeners();
        gruntChild.kill();
    }

    gruntChild = spawn(grunt, ['styles'], { stdio: 'inherit' });
    gruntChild.on('close', onClose);
    console.log('start grunt pid', gruntChild.pid);
});
