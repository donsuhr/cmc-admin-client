#!/usr/local/bin/node

'use strict';

const { spawn } = require('child_process');

function promiseFromChildProcess(child) {
    return new Promise((resolve, reject) => {
        child.addListener('error', reject);
        child.addListener('exit', resolve);
    });
}

function writeConfig() {
    return promiseFromChildProcess(
        spawn('yarn', ['run', 'build:write-config'], { stdio: 'inherit' }),
    );
}

function watchPrep() {
    return promiseFromChildProcess(
        spawn('yarn', ['run', 'grunt', 'watchPrep'], { stdio: 'inherit' }),
    );
}

Promise.resolve()
    .then(writeConfig)
    .then(watchPrep)
    .then(() => {
        const watch = spawn('node', ['./build/justWatch.js'], {
            stdio: 'inherit',
        });
        // TODO: removed browser sync
        const bs = spawn(
            './node_modules/.bin/browser-sync',
            ['start', '--config', 'bs-config.js'],
            { stdio: 'inherit' },
        );

        function done() {
            process.kill(watch.pid);
            process.kill(bs.pid);
            setImmediate(() => {
                console.log('exiting...'); // eslint-disable-line no-console
                process.exit(0);
            });
        }

        process.on('SIGINT', () => {
            done();
        });
        process.on('SIGTERM', () => {
            done();
        });

        spawn('top', ['-n', 1], { stdio: 'inherit' });
    });
