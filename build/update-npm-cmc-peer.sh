#!/usr/bin/env bash
# git+ssh://git@bitbucket.org/zorg128/cmc-auth.git

#yarn remove cmc-auth

#yarn add \
#git+ssh://git@bitbucket.org/zorg128/cmc-auth.git

yarn remove cmc-write-config metalsmith-add-orig-file-name cmc-auth

yarn add \
git+ssh://git@bitbucket.org/zorg128/cmc-write-config.git \
git+ssh://git@bitbucket.org/zorg128/metalsmith-add-orig-file-name.git \
git+ssh://git@bitbucket.org/zorg128/cmc-auth.git

#yarn upgrade \
# git+ssh://git@bitbucket.org/zorg128/cmc-auth.git
# npm run grunt ssoIframe
