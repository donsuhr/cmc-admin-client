'use strict';

const path = require('path');
const fs = require('fs');
const { argv } = require('yargs');
// eslint-disable-next-line import/no-extraneous-dependencies
const chokidar = argv.watch ? require('chokidar') : null;

const Metalsmith = require('metalsmith');
const layouts = require('metalsmith-layouts');
const inPlace = require('metalsmith-in-place');
const helpers = require('metalsmith-register-helpers');
const permalinks = require('metalsmith-permalinks');
const metalsmithEnv = require('metalsmith-env');
const defaultValues = require('metalsmith-default-values');
const collections = require('metalsmith-collections');
const partials = require('metalsmith-register-partials');
const addOriginalFilename = require('metalsmith-add-orig-file-name');
const handlebars = require('handlebars');
const handlebarsLayouts = require('handlebars-layouts');
const webpackAssets = require('../app/html/middleware/metalsmith-webpack-assets');

// assemble helpers have the handlebars-layouts in it. assemble first
require('handlebars-helpers')({
    handlebars,
}); // http://assemble.io/helpers/
// overwrite second
handlebars.registerHelper(handlebarsLayouts(handlebars));

require('@babel/register')({
    only: [/config.js/],
    presets: [
        [
            '@babel/preset-env',
            {
                targets: {
                    node: 'current',
                },
            },
        ],
    ],
});

const config = require('../config').default;

function getdirSync(dirPath, parentName, result) {
    const stat = fs.lstatSync(dirPath);
    result = result || {};
    if (stat.isDirectory()) {
        const name = parentName
            ? `${parentName}==>${path.basename(dirPath)}`
            : path.basename(dirPath);
        const localName = name.replace(/^pages==>/, '');
        if (parentName) {
            // ignore root dir
            result[localName] = {
                path: dirPath,
                name: localName,
                refer: false,
                pattern: `${localName.replace(/==>/g, '/')}/*.html`,
            };
        }

        fs.readdirSync(dirPath).map((child) => getdirSync(`${dirPath}/${child}`, name, result));
    }
    return result;
}

const autoCollections = getdirSync(
    path.resolve(`${__dirname}/../app/html/pages`),
);

function build(clean) {
    const ms = new Metalsmith(path.resolve(`${__dirname}/../`));
    ms.clean(!!clean)
        .source('app/html/pages')
        .destination('dist')
        .metadata({
            config,
        })
        .use(addOriginalFilename({}))
        .use(metalsmithEnv())
        .use(
            defaultValues([
                {
                    pattern: '**/*.html',
                    defaults: {
                        priority: 0.5,
                        'show-in-quick-launch': true,
                        'show-in-left-nav': true,
                    },
                },
            ]),
        )
        .use(webpackAssets({}))
        .use(collections(autoCollections))
        .use(
            permalinks({
                relative: false,
            }),
        )
        .use(
            helpers({
                engine: 'handlebars',
                directory: 'app/html/helpers',
            }),
        )
        .use(
            partials({
                directory: 'app/html/partials',
                handlebars,
            }),
        )
        .use(
            partials({
                directory: 'app/html/layouts',
                handlebars,
            }),
        )
        .use(
            inPlace({
                engine: 'handlebars',
            }),
        )
        .use(
            layouts({
                // files using local layouts folder
                engine: 'handlebars',
                directory: 'app/html/layouts',
                pattern: ['**/*.html'],
            }),
        )
        .build((err, files) => {
            if (err) {
                console.log(err); // eslint-disable-line no-console
                console.log('Error rendering files. Stack:'); // eslint-disable-line no-console
                console.log(err.stack); // eslint-disable-line no-console
            } else {
                // eslint-disable-next-line no-console
                console.log(
                    'Metalsmith Done',
                    Object.keys(files).length,
                    'files written',
                );
            }
        });
}

if (argv.watch) {
    chokidar
        .watch([
            'app/html/**/*.html',
            'app/html/layouts/**/*.hbs',
            'app/html/partials/**/*.hbs',
            'app/html/helpers/**/*.js',
        ])
        .on('change', (filePath) => {
            // eslint-disable-next-line no-console
            console.log('metalsmith chokidar watch: ', filePath);
            build(false);
        });
}

if (!argv.justWatch) {
    build(!!argv.clean);
}
