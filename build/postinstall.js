/* eslint-disable no-console */
// https://gist.github.com/DavidWells/e886bff1fdb543a707b39451535b1369

'use strict';

const path = require('path');
const { spawn, spawnSync } = require('child_process');

const cwd = process.cwd();
const isNodeChild = cwd.indexOf('node_modules') !== -1;

function distIfChild() {
    if (process.env.DOCKERIZED !== true && isNodeChild) {
        console.log('is in child context and not docker, run dist.');
        spawn('npm', ['run', 'dist'], { stdio: 'inherit' });
    }
}

if (!isNodeChild && process.env.NODE_ENV !== 'production') {
    console.log('in local dev context. Build DLL');
    const webpackPath = path.join(cwd, 'node_modules', '.bin', 'webpack');
    spawnSync(
        webpackPath,
        [
            '--display',
            'normal',
            '--progress',
            '--display-chunks',
            '--color',
            '--config',
            'webpack.dll.config.js',
        ],
        { stdio: 'inherit' },
    );
    console.log('Built dll for local DEV');
    distIfChild();
} else {
    console.log(
        'in node_modules context or prod, stop DLL build on postinstall',
    );
    distIfChild();
}
