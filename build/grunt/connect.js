'use strict';

const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const serveStatic = require('serve-static');
const compression = require('compression');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const configPath = path.resolve('.', 'webpack.config.js');
const webpackConfig = require(configPath); // eslint-disable-line import/no-dynamic-require

const bundler = webpack(webpackConfig);

module.exports = function connectConfig(grunt, options) {
    const defaultOptions = {
        port: 9004,
        open: !!grunt.option('open'),
        livereload: options.livereloadPort,
        hostname: '*',
        useAvailablePort: false,
    };
    if (grunt.option('secure')) {
        defaultOptions.key = grunt.file
            .read('/etc/letsencrypt/live/suhrthing.com/privkey.pem')
            .toString();
        defaultOptions.cert = grunt.file
            .read('/etc/letsencrypt/live/suhrthing.com/cert.pem')
            .toString();
        defaultOptions.protocol = 'https';
    }
    return {
        options: defaultOptions,
        livereload: {
            options: {
                // eslint-disable-next-line arrow-body-style
                middleware: (connect, middlewareOptions) => {
                    return [
                        webpackDevMiddleware(bundler, {
                            // IMPORTANT: dev middleware can't access config, so we should
                            // provide publicPath by ourselves
                            publicPath: webpackConfig.output.publicPath,
                            stats: 'errors-only',
                        }),
                        webpackHotMiddleware(bundler),
                        (req, res, next) => {
                            if (req.method === 'POST') {
                                req.method = 'GET';
                            }
                            return next();
                        },
                        serveStatic('.tmp'),
                        connect().use('/dist', serveStatic('./dist')),
                        connect().use(
                            '/dll.bundle.js',
                            serveStatic('./dll.bundle.js'),
                        ),
                        serveStatic(options.app),
                        serveStatic('./dist'),
                        (req, res) => {
                            const filePath = path.resolve('.', 'dist/404.html');
                            const stat = fs.statSync(filePath);
                            res.writeHead(404, {
                                'Content-Type': 'text/html',
                                'Content-Length': stat.size,
                            });
                            const readStream = fs.createReadStream(filePath);
                            readStream.pipe(res);
                        },
                    ];
                },
            },
        },

        dist: {
            options: {
                // port: 9001,
                base: options.dist,
                livereload: false,
                open: true,
                keepalive: true,
                hostname: '*',
                // protocol: 'https',
                // eslint-disable-next-line arrow-body-style
                middleware: (connect) => {
                    return [
                        connect().use(
                            compression({
                                // eslint-disable-next-line arrow-body-style
                                filter: (req, res) => {
                                    return /json|text|xml|css|javascript/.test(
                                        res.getHeader('Content-Type'),
                                    );
                                },
                            }),
                        ),
                        connect().use('/', serveStatic('./dist')),
                    ];
                },
            },
        },
    };
};
