'use strict';

module.exports = function watch(grunt, options) {
    const config = {
        js: {
            files: [
                './**/*.js',
                './**/*.jsx',
                '!./app/scripts/vendor/**',
                '!./node_modules/**',
                '!./dist/**',
                '!./.tmp/**',
            ],
            tasks: ['exec:lint'],
        },
        sass: {
            files: ['./app/**/*.scss'],
            tasks: ['scsslint'],
        },
        livereload: {
            options: {
                livereload: {
                    port: options.livereloadPort,
                },
            },
            files: [
                '.tmp/styles/**/*.css',
                `${options.app}/images/{,*/}*`,
                '/test/{,*/}*.js',
                `${options.dist}/**/*.html`,
            ],
        },
    };
    if (grunt.option('secure')) {
        config.livereload.options.livereload.key = grunt.file.read(
            '/etc/letsencrypt/live/suhrthing.com/privkey.pem',
        );
        config.livereload.options.livereload.cert = grunt.file.read(
            '/etc/letsencrypt/live/suhrthing.com/cert.pem',
        );
    }
    return config;
};
