'use strict';

module.exports = function replace(grunt, options) {
    return {
        ssoIframe: {
            src: ['.tmp/webpack.config-ssoIframe.js'],
            overwrite: true,
            replacements: [
                {
                    // eslint-disable-next-line no-useless-escape
                    from: 'app/scripts/sso-iframe.js',
                    to: '.tmp/scripts/sso-iframe.js',
                },
                {
                    // eslint-disable-next-line no-useless-escape
                    from: "'app'",
                    to: "'.tmp'",
                },
            ],
        },
    };
};
