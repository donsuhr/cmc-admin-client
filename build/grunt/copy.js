'use strict';

const path = require('path');

module.exports = function copy(grunt, options) {
    return {
        dist: {
            files: [
                {
                    // non imagemin / svgmin
                    expand: true,
                    dot: true,
                    cwd: options.app,
                    dest: options.dist,
                    src: ['*.{ico,png,txt}', 'images/**/**', 'fonts/{,*/}*.*'],
                },
                {
                    expand: true,
                    dot: true,
                    cwd: path.resolve(
                        options.modulesDir,
                        'jquery-ui/themes/base/images',
                    ),
                    src: ['**/*'],
                    dest: `${options.dist}/images/jquery-ui`,
                },
            ],
        },
        ssoIframe: {
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: '.',
                    dest: '.tmp',
                    src: ['webpack.config-ssoIframe.js'],
                },
                {
                    expand: true,
                    dot: true,
                    cwd: './app/scripts',
                    dest: '.tmp/scripts',
                    src: ['sso-iframe.js'],
                },
            ],
        },
    };
};
