'use strict';

module.exports = function useminCof(grunt, options) {
    return {
        options: {
            assetsDirs: [options.dist],
        },
        html: {
            src: `${options.dist}/**/*.html`,
        },
        css: {
            src: `${options.dist}/**/*.css`,
        },
        headers: {
            src: `${options.dist}/_headers`,
            options: {
                patterns: {
                    headers: [
                        [/Link: <([^>]+)>/gm, 'Replacing reference to css'],
                    ],
                },
            },
        },
    };
};
