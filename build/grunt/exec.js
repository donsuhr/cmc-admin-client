'use strict';

module.exports = function run(grunt, options) {
    return {
        'build-js': {
            cmd: 'npm run build:js',
            stdio: 'inherit',
        },
        'build-js--quiet': {
            cmd: 'npm run build:js:quiet',
            stdio: 'inherit',
        },
        'build-js--sso-iframe': {
            cmd: 'npm run build:js:ssoIframe',
            stdio: 'inherit',
        },
        lint: {
            cmd: 'npm run lint',
            stdio: 'inherit',
        },
        'metalsmith-dist': {
            cmd: 'npm run build:html',
            stdio: 'inherit',
        },
        'metalsmith-watch': {
            cmd: 'npm run watch:html',
            stdio: 'inherit',
        },
        postcss: {
            cmd: 'npm run postcssprefix -- --dir .tmp/styles',
            stdio: 'inherit',
        },
        'postcss-watch': {
            cmd: 'npm run postcssprefix -- --dir .tmp/styles --watch',
            stdio: 'inherit',
        },
        'postcss-min': {
            cmd: 'npm run postcssprefix -- --dir dist/styles --minify',
            stdio: 'inherit',
        },
        sass: {
            cmd: 'npm run sass',
            stdio: 'inherit',
        },
        'sass-watch': {
            cmd: 'npm run sass -- -w --skip-initial',
            stdio: 'inherit',
        },
        'write-config': {
            cmd: 'npm run build:write-config',
            stdio: 'inherit',
        },
        'write-sitemap': {
            cmd: 'npm run build:write-sitemap',
            stdio: 'inherit',
        },
    };
};
