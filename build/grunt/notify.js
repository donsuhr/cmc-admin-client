'use strict';

module.exports = function notify(grunt, options) {
    return {
        dist: {
            options: {
                title: 'Grunt Task',
                message: 'Dist Done',
            },
        },
    };
};
