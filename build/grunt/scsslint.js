'use strict';

module.exports = function scsslint(grunt, options) {
    return {
        allFiles: [`${options.app}/**/*.scss`],
        options: {
            bundleExec: false,
            config: '.scss-lint.yml',
            reporterOutput: null, // '.tmp/scss-lint-report.xml',
            colorizeOutput: true,
            exclude: [`${options.app}/styles/vendor/**`],
        },
    };
};
