'use strict';

module.exports = function concurrent(grunt, options) {
    return {
        options: {
            logConcurrentOutput: true,
            limit: 8,
        },
        dist: ['exec:sass', 'exec:build-js--quiet', 'copy:dist'],
        serve: [
            // 'watch:js',
            // 'watch:sass',
            'watch:livereload',
            'exec:metalsmith-watch',
            'exec:sass-watch',
            'exec:postcss-watch',
        ],
    };
};
